# Change Log

All notable changes to the "devextreme-extension" extension will be documented in this file.

Check [Keep a Changelog](http://keepachangelog.com/) for recommendations on how to structure this file.

## [Added] [Changed] (2022-05-03)
Add property and function in:
- `DxCalendarOptions`
- `DxColumn`
- `DxGroupItem`
- `DxTotalItem`

Add snippet:
- `TreeList`

## [Added] [Changed] (2022-04-28)
Add property and function in:
- `DxValidator`

## [Added] [Changed] (2022-04-27)
Add property and function in:
- `DxValidationGroup`

Changed:
- `Auto import children component in script`

## [Added] [Fixed] (2022-04-26)
Add autocomplete accepted values in:
- `DxAutocomplete`

Fixed:
- `Import component`

## [Added] [Fixed] (2022-04-25)
Add property and function in:
- `DxCalendar`
- `DxLoadIndicator`
- `DxLoadPanel`
- `DxLookup`
- `DxMultiView`
- `DxPopover`
- `DxRangeSelector`
- `DxToast`

Fixed:
- `Position import component`

## [Added] [Changed] (2022-04-23)
Add snippet:
- `DxBox`

Add property and function in:
- `DxButtonGroup`
- `DxAccordion`
- `DxActionSheet`
- `DxAutocomplete`
- `DxBox`
- `DxColorBox`
- `DxDrawer`

Changed:
- `Auto import component in script`

## [Added] (2022-04-22)
- `Auto import component in script`

## [Added] (2022-04-19)
Add property and function in:
- `DxTagBox`
- `DxTextAria`
- `DxToolbar`
- `DxTooltip`

## [Added] [Fixed] (2022-04-18)
Add property and function in:
- `DxPopup`
- `DxRadioGroup`
- `DxSlider`
- `DxTabPanel`
- `DxSwitch`
- `DxTabs`

Fixed:
- `Language`

## [Added] (2022-04-15)
Add property and function in:
- `DxCheckBox`
- `DxDateBox`
- `DxButtonBox`
- `DxDataGridBox`
- `DxDropDownBox`
- `DxDropDownButton`
- `DxSelectBox`
- `DxGallery`
- `DxList`
- `DxMenu`
- `DxNumberBox`

## [Added] (2022-04-14)
Add property and function in:
- `DxTextBox`

