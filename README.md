# devextreme-snippet-html README
This extension adds snippets Devextreme for VS Code

## Version
    Devextreme: v21.2
## Snippets (Updating...Please check CHANGELOG)

* `dx-TextBox`: creates an devextreme textbox
* `dx-TextArea`: creates an devextreme dx-TextArea
* `dx-NumberBox`: creates an devextreme dx-NumberBox
* `dx-DateBox`: creates an devextreme dx-DateBox
* `dx-CheckBox`: creates an devextreme dx-CheckBox
* `dx-DropDownBox-List`: creates an devextreme dx-DropDownBox-List
* `dx-DropDownBox`: creates an devextreme dx-DropDownBox
* `dx-Button`: creates an devextreme dx-Button
* `dx-List`: creates an devextreme dx-List
* `dx-DataGrid`: creates an devextreme dx-DataGrid
* `dx-Validation-Group`: creates an devextreme dx-Validation-Group
* `dx-Reponsive-Box`: creates an devextreme dx-Reponsive-Box
* `... `: And more

## Usages
Completion Tag
![Getting Started](./images/gif-1.gif)

Completion Property and Function
![Getting Started](./images/gif-2.gif)

Completion Template
![Getting Started](./images/gif-3.gif)

-----------------------------------------------------------------------------------------------------------


### For more information

* [Extension Snippet Support](http://sonphat.net)
* [Extension on Marketplace](https://marketplace.visualstudio.com/items?itemName=SPSnippet.devextreme-snippet-html&ssr=false#version-history)

**Thankyou! :)**
