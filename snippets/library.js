
const DxTextBox = require('../librarys/DxTextBox');
const DxSelectBox = require('../librarys/DxSelectBox');
const DxButton = require('../librarys/DxButton');
const DxCheckBox = require('../librarys/DxCheckBox');
const DxDataGrid = require('../librarys/DxDataGrid');
const DxDateBox = require('../librarys/DxDateBox');
const DxDropDownBox = require('../librarys/DxDropDownBox');
const DxDropDownButton = require('../librarys/DxDropDownButton');
const DxGallery = require('../librarys/DxGallery');
const DxList = require('../librarys/DxList');
const DxMenu = require('../librarys/DxMenu');
const DxNumberBox = require('../librarys/DxNumberBox');
const DxPopup = require('../librarys/DxPopup');
const DxRadioGroup = require('../librarys/DxRadioGroup');
const DxSlider = require('../librarys/DxSlider');
const DxTabPanel = require('../librarys/DxTabPanel');
const DxSwitch = require('../librarys/DxSwitch');
const DxTabs = require('../librarys/DxTabs');
const DxTagBox = require('../librarys/DxTagBox');
const DxTextArea = require('../librarys/DxTextArea');
const DxToolbar = require('../librarys/DxToolbar');
const DxTooltip = require('../librarys/DxTooltip');
const DxButtonGroup = require('../librarys/DxButtonGroup');
const DxAccordion = require('../librarys/DxAccordion');
const DxActionSheet = require('../librarys/DxActionSheet');
const DxAutocomplete = require('../librarys/DxAutocomplete');
const DxBox = require('../librarys/DxBox');
const DxColorBox = require('../librarys/DxColorBox');
const DxDrawer = require('../librarys/DxDrawer');
const DxCalendar = require('../librarys/DxCalendar');
const DxCalendarOptions = require('../librarys/DxCalendar');
const DxLoadIndicator = require('../librarys/DxLoadIndicator');
const DxLoadPanel = require('../librarys/DxLoadPanel');
const DxLookup = require('../librarys/DxLookup');
const DxMultiView = require('../librarys/DxMultiView');
const DxPopover = require('../librarys/DxPopover');
const DxRangeSelector = require('../librarys/DxRangeSelector');
const DxToast = require('../librarys/DxToast');
const DxValidationGroup = require('../librarys/DxValidationGroup');
const DxValidator = require('../librarys/DxValidator');
const DxColumn = require('../librarys/DxColumn');
const DxGroupItem = require('../librarys/DxGroupItem');
const DxTotalItem = require('../librarys/DxTotalItem');
const DxTreeList = require('../librarys/DxTreeList');
module.exports = {
    DxTextBox,
    DxSelectBox,
    DxButton,
    DxCheckBox,
    DxDataGrid,
    DxDateBox,
    DxDropDownBox,
    DxDropDownButton,
    DxGallery,
    DxList,
    DxMenu,
    DxNumberBox,
    DxPopup,
    DxRadioGroup,
    DxSlider,
    DxTabPanel,
    DxSwitch,
    DxTabs,
    DxTagBox,
    DxTextArea,
    DxToolbar,
    DxTooltip,
    DxButtonGroup,
    DxAccordion,
    DxActionSheet,
    DxAutocomplete,
    DxBox,
    DxColorBox,
    DxDrawer,
    DxCalendar,
    DxCalendarOptions,
    DxLoadIndicator,
    DxLoadPanel,
    DxLookup,
    DxMultiView,
    DxPopover,
    DxRangeSelector,
    DxToast,
    DxValidationGroup,
    DxValidator,
    DxColumn,
    DxGroupItem,
    DxTotalItem,
    DxTreeList,
}