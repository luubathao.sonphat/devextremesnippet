
module.exports = [
    {
        prefix: "adapter",
        detail: "An object that specifies what and when to validate, and how to apply the validation result.",
        document: {
            type: "Object",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxValidator/Configuration/#adapter"
    },
    {
        prefix: "elementAttr",
        detail: "VM265:1 Specifies the global attributes to be attached to the UI component's container element.",
        document: {
            type: "Object",
            defaultValue: "{}",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxValidator/Configuration/#elementAttr"
    },
    {
        prefix: "height",
        detail: "VM265:1 Specifies the UI component's height.",
        document: {
            type: "Number | String | Function",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: ['text', 'outlined', 'contained'],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxValidator/Configuration/#height"
    },
    {
        prefix: "name",
        detail: "VM265:1 Specifies the editor name to be used in the validation default messages.",
        document: {
            type: "String",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxValidator/Configuration/#name"
    },
    {
        prefix: "onDisposing",
        detail: "VM265:1 A function that is executed before the UI component is disposed of.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxValidator/Configuration/#onDisposing"
    },
    {
        prefix: "onInitialized",
        detail: "VM265:1 A function used in JavaScript frameworks to save the UI component instance.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxValidator/Configuration/#onInitialized"
    },
    {
        prefix: "onOptionChanged",
        detail: "VM265:1 A function that is executed after a UI component property is changed.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: ['back', 'danger', 'default', 'normal', 'success'],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxValidator/Configuration/#onOptionChanged"
    },
    {
        prefix: "onValidated",
        detail: "VM265:1 A function that is executed after a value is validated.",
        document: {
            type: "Function",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxValidator/Configuration/#onValidated"
    },
    {
        prefix: "validationGroup",
        detail: "VM265:1 Specifies the validation group the editor will be related to.",
        document: {
            type: "String",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxValidator/Configuration/#validationGroup"
    },
    {
        prefix: "validationRules",
        detail: "VM265:1 An array of validation rules to be checked for the editor with which the dxValidator object is associated.",
        document: {
            type: "Array<RequiredRule | NumericRule | RangeRule | StringLengthRule | CustomRule | CompareRule | PatternRule | EmailRule | AsyncRule>",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxValidator/Configuration/#validationRules"
    },
    {
        prefix: "width",
        detail: "VM265:1 Specifies the UI component's width.",
        document: {
            type: "Number | String | Function",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxValidator/Configuration/#width"
    },

]