
module.exports = [
    {
        prefix: "accessKey",
        detail: "Specifies the shortcut key that sets focus on the UI component.",
        document: {
            type: "String",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#accessKey"
    },
    {
        prefix: "activeStateEnabled",
        detail: "Specifies whether the UI component changes its state as a result of user interaction.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#activeStateEnabled"
    },
    {
        prefix: "allowColumnReordering",
        detail: "Specifies whether a user can reorder columns.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#allowColumnReordering"
    },
    {
        prefix: "allowColumnResizing",
        detail: "Specifies whether a user can resize columns.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#allowColumnResizing"
    },
    {
        prefix: "autoExpandAll",
        detail: "Specifies whether all rows are expanded initially.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#autoExpandAll"
    },
    {
        prefix: "autoNavigateToFocusedRow",
        detail: "Automatically scrolls to the focused row when the focusedRowKey is changed.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#autoNavigateToFocusedRow"
    },
    {
        prefix: "cacheEnabled",
        detail: "Specifies whether data should be cached.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#cacheEnabled"
    },
    {
        prefix: "cellHintEnabled",
        detail: "Enables a hint that appears when a user hovers the mouse pointer over a cell with truncated content.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#cellHintEnabled"
    },
    {
        prefix: "columnAutoWidth",
        detail: "Specifies whether columns should adjust their widths to the content.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#columnAutoWidth"
    },
    {
        prefix: "columnChooser",
        detail: "Configures the column chooser.",
        document: {
            type: "Object",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/columnChooser/"
    },
    {
        prefix: "columnFixing",
        detail: "Configures column fixing.",
        document: {
            type: "Object",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/columnFixing/"
    },
    {
        prefix: "columnHidingEnabled",
        detail: "Specifies whether the UI component should hide columns to adapt to the screen or container size. Ignored if allowColumnResizing is true and columnResizingMode is 'widget'.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#columnHidingEnabled"
    },
    {
        prefix: "columnMinWidth",
        detail: "Specifies the minimum width of columns.",
        document: {
            type: "Number",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#columnMinWidth"
    },
    {
        prefix: "columnResizingMode",
        detail: "Specifies how the UI component resizes columns. Applies only if allowColumnResizing is true.",
        document: {
            type: "String",
            defaultValue: "'nextColumn'",
            description: "",
            note: "",
            acceptValues: ['nextColumn', 'widget']
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#columnResizingMode"
    },
    {
        prefix: "columns",
        detail: "Configures columns.",
        document: {
            type: "Number | String",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/columns/"
    },
    {
        prefix: "columnWidth",
        detail: "Specifies the width for all data columns. Has a lower priority than the column.width property.",
        document: {
            type: "Function",
            defaultValue: "",
            description: "",
            note: "Data operations (sorting, filtering, summary) are unavailable for the columns created via customizeColumns. To create a fully functioning column, add it to the columns array.",
            acceptValues: ['auto']
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#columnWidth"
    },
    {
        prefix: "customizeColumns",
        detail: "Customizes columns after they are created.",
        document: {
            type: "Store | DataSource | DataSource Configuration | String | Array < any >",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#customizeColumns"
    },
    {
        prefix: "dataSource",
        detail: "Binds the UI component to data.",
        document: {
            type: "String",
            defaultValue: "'plain'",
            description: "",
            note: "Editing does not work with hierarchical data sources out of the box, but you can use the code sample from the following knowledge base article to implement it: TreeList - How to perform CRUD operations on a hierarchical data source.",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#dataSource"
    },
    {
        prefix: "dataStructure",
        detail: "Notifies the UI component of the used data structure.",
        document: {
            type: "String",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: ['plain', 'tree']
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#dataStructure"
    },
    {
        prefix: "dateSerializationFormat",
        detail: "Specifies the format in which date-time values should be sent to the server.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#dateSerializationFormat"
    },
    {
        prefix: "disabled",
        detail: "Specifies whether the UI component responds to user interaction.",
        document: {
            type: "Object",
            defaultValue: "",
            description: "",
            note: "Editing does not work with hierarchical data sources out of the box, but you can use the code sample from this KB to implement it.",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#disabled"
    },
    {
        prefix: "editing",
        detail: "Configures editing.",
        document: {
            type: "Object",
            defaultValue: "{}",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/editing/"
    },
    {
        prefix: "elementAttr",
        detail: "Specifies the global attributes to be attached to the UI component's container element.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#elementAttr"
    },
    {
        prefix: "errorRowEnabled",
        detail: "Indicates whether to show the error row.",
        document: {
            type: "Array < any >",
            defaultValue: "[]",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#errorRowEnabled"
    },
    {
        prefix: "expandedRowKeys",
        detail: "Specifies keys of the initially expanded rows.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#expandedRowKeys"
    },
    {
        prefix: "expandNodesOnFiltering",
        detail: "Specifies whether nodes appear expanded or collapsed after filtering is applied.",
        document: {
            type: "FilterBuilder Configuration",
            defaultValue: "{}",
            description: "",
            note: "In Angular and Vue, the nested component that configures the filterBuilder property does not support event bindings and two-way property bindings.",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#expandNodesOnFiltering"
    },
    {
        prefix: "filterBuilder",
        detail: "Configures the integrated filter builder.",
        document: {
            type: "Popup Configuration",
            defaultValue: "{}",
            description: "",
            note: "In Angular and Vue, the nested component that configures the filterBuilderPopup property does not support event bindings and two-way property bindings.",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#filterBuilder"
    },
    {
        prefix: "filterBuilderPopup",
        detail: "Configures the popup in which the integrated filter builder is shown.",
        document: {
            type: "String",
            defaultValue: "'withAncestors'",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#filterBuilderPopup"
    },
    {
        prefix: "filterMode",
        detail: "Specifies whether filter and search results should include matching rows only, matching rows with ancestors, or matching rows with ancestors and descendants (full branch).",
        document: {
            type: "Object",
            defaultValue: "{}",
            description: "",
            note: "",
            acceptValues: ['fullBranch', 'withAncestors', 'matchOnly']
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#filterMode"
    },
    {
        prefix: "filterPanel",
        detail: "Configures the filter panel.",
        document: {
            type: "Object",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/filterPanel/"
    },
    {
        prefix: "filterRow",
        detail: "Configures the filter row.",
        document: {
            type: "Boolean | String",
            defaultValue: "'auto'",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/filterRow/"
    },
    {
        prefix: "filterSyncEnabled",
        detail: "Specifies whether to synchronize the filter row, header filter, and filter builder. The synchronized filter expression is stored in the filterValue property.",
        document: {
            type: "Filter Expression",
            defaultValue: "null",
            description: "",
            note: "The DataSource does not support the 'between', 'anyof', 'noneof', and custom operations. Use the getCombinedFilter(returnDataField) method to get the DataSource-compatible filter expression.",
            acceptValues: ['auto']
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#filterSyncEnabled"
    },
    {
        prefix: "filterValue",
        detail: "Specifies a filter expression.",
        document: {
            type: "Number",
            defaultValue: "-1",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#filterValue"
    },
    {
        prefix: "focusedColumnIndex",
        detail: "The index of the column that contains the focused data cell. This index is taken from the columns array.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "Specify the UI component's keyExpr or the Store's key property to ensure that the focused row feature works properly.",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#focusedColumnIndex"
    },
    {
        prefix: "focusedRowEnabled",
        detail: "Specifies whether the focused row feature is enabled.",
        document: {
            type: "Number",
            defaultValue: "-1",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#focusedRowEnabled"
    },
    {
        prefix: "focusedRowIndex",
        detail: "Specifies or indicates the focused data row's index.",
        document: {
            type: "any",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#focusedRowIndex"
    },
    {
        prefix: "focusedRowKey",
        detail: "Specifies initially or currently focused grid row's key.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#focusedRowKey"
    },
    {
        prefix: "focusStateEnabled",
        detail: "Specifies whether the UI component can be focused using keyboard navigation.",
        document: {
            type: "String | Function",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#focusStateEnabled"
    },
    {
        prefix: "hasItemsExpr",
        detail: "Specifies which data field defines whether the node has children.",
        document: {
            type: "Object",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#hasItemsExpr"
    },
    {
        prefix: "headerFilter",
        detail: "Configures the header filter feature.",
        document: {
            type: "Number | String | Function",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/headerFilter/"
    },
    {
        prefix: "height",
        detail: "Specifies the UI component's height.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#height"
    },
    {
        prefix: "highlightChanges",
        detail: "Specifies whether to highlight rows and cells with edited data. repaintChangesOnly should be true.",
        document: {
            type: "String",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#highlightChanges"
    },
    {
        prefix: "hint",
        detail: "Specifies text for a hint that appears when a user pauses on the UI component.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#hint"
    },
    {
        prefix: "hoverStateEnabled",
        detail: "Specifies whether to highlight rows when a user moves the mouse pointer over them.",
        document: {
            type: "String | Function",
            defaultValue: "'items'",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#hoverStateEnabled"
    },
    {
        prefix: "itemsExpr",
        detail: "Specifies which data field contains nested items. Set this property when your data has a hierarchical structure.",
        document: {
            type: "Object",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#itemsExpr"
    },
    {
        prefix: "keyboardNavigation",
        detail: "Configures keyboard navigation.",
        document: {
            type: "String | Function",
            defaultValue: "'id'",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/keyboardNavigation/"
    },
    {
        prefix: "keyExpr",
        detail: "Specifies the key property (or properties) that provide(s) key values to access data items. Each key value must be unique.",
        document: {
            type: "Object",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#keyExpr"
    },
    {
        prefix: "loadPanel",
        detail: "Configures the load panel.",
        document: {
            type: "String",
            defaultValue: "'No data'",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/loadPanel/"
    },
    {
        prefix: "noDataText",
        detail: "Specifies a text string shown when the TreeList does not display any data.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "The following Form properties cannot be specified using formOptions:template,editorType,any event handler (properties whose name starts with 'on...')",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#noDataText"
    },
    {
        prefix: "onAdaptiveDetailRowPreparing",
        detail: "A function that is executed before an adaptive detail row is rendered.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#onAdaptiveDetailRowPreparing"
    },
    {
        prefix: "onCellClick",
        detail: "A function that is executed when a cell is clicked or tapped. Executed before onRowClick.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#onCellClick"
    },
    {
        prefix: "onCellDblClick",
        detail: "A function that is executed when a cell is double-clicked or double-tapped. Executed before onRowDblClick.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#onCellDblClick"
    },
    {
        prefix: "onCellHoverChanged",
        detail: "A function that is executed after the pointer enters or leaves a cell.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#onCellHoverChanged"
    },
    {
        prefix: "onCellPrepared",
        detail: "A function that is executed after a grid cell is created.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#onCellPrepared"
    },
    {
        prefix: "onContentReady",
        detail: "A function that is executed when the UI component's content is ready and each time the content is changed.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#onContentReady"
    },
    {
        prefix: "onContextMenuPreparing",
        detail: "A function that is executed before the context menu is rendered.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#onContextMenuPreparing"
    },
    {
        prefix: "onDataErrorOccurred",
        detail: "A function that is executed when an error occurs in the data source.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#onDataErrorOccurred"
    },
    {
        prefix: "onDisposing",
        detail: "A function that is executed before the UI component is disposed of.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#onDisposing"
    },
    {
        prefix: "onEditCanceled",
        detail: "A function that is executed after row changes are discarded.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#onEditCanceled"
    },
    {
        prefix: "onEditCanceling",
        detail: "A function that is executed when the edit operation is canceled, but row changes are not yet discarded.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#onEditCanceling"
    },
    {
        prefix: "onEditingStart",
        detail: "A function that is executed before a cell or row switches to the editing state.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#onEditingStart"
    },
    {
        prefix: "onEditorPrepared",
        detail: "A function that is executed after an editor is created. Not executed for cells with an editCellTemplate.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "We do not recommend that you use the onEditorPreparing function to specify an editor's default value. Use the onInitNewRow function instead.",
            acceptValues: []
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#onEditorPrepared"
    },
    {
        prefix: "onEditorPreparing",
        detail: "A function used to customize a cell's editor. Not executed for cells with an editCellTemplate.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#onEditorPreparing"
    },
    {
        prefix: "onFocusedCellChanged",
        detail: "A function that is executed after the focused cell changes. Applies only to cells in data rows.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#onFocusedCellChanged"
    },
    {
        prefix: "onFocusedCellChanging",
        detail: "A function that is executed before the focused cell changes. Applies only to cells in data rows.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#onFocusedCellChanging"
    },
    {
        prefix: "onFocusedRowChanged",
        detail: "A function that executed when the focused row changes. Applies only to data rows. focusedRowEnabled should be true.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#onFocusedRowChanged"
    },
    {
        prefix: "onFocusedRowChanging",
        detail: "A function that is executed before the focused row changes. Applies only to data rows. focusedRowEnabled should be true.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#onFocusedRowChanging"
    },
    {
        prefix: "onInitialized",
        detail: "A function used in JavaScript frameworks to save the UI component instance.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#onInitialized"
    },
    {
        prefix: "onInitNewRow",
        detail: "A function that is executed before a new row is added to the UI component.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#onInitNewRow"
    },
    {
        prefix: "onKeyDown",
        detail: "A function that is executed when the UI component is in focus and a key has been pressed down.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#onKeyDown"
    },
    {
        prefix: "onNodesInitialized",
        detail: "A function that is executed after the loaded nodes are initialized.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#onNodesInitialized"
    },
    {
        prefix: "onOptionChanged",
        detail: "A function that is executed after a UI component property is changed.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "When the clicked row is in the editing state or switches to this state, the onRowClick function is not executed. Instead, specify the onCellClick function.",
            acceptValues: []
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#onOptionChanged"
    },
    {
        prefix: "onRowClick",
        detail: "A function that is executed when a grid row is clicked or tapped.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#onRowClick"
    },
    {
        prefix: "onRowCollapsed",
        detail: "A function that is executed after a row is collapsed.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#onRowCollapsed"
    },
    {
        prefix: "onRowCollapsing",
        detail: "A function that is executed before a row is collapsed.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "onRowDblClick is not executed when the clicked row is in the editing state or switches to this state. You can use onCellDblClick instead.",
            acceptValues: []
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#onRowCollapsing"
    },
    {
        prefix: "onRowDblClick",
        detail: "A function that is executed when a row is double-clicked or double-tapped. Executed after onCellDblClick.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#onRowDblClick"
    },
    {
        prefix: "onRowExpanded",
        detail: "A function that is executed after a row is expanded.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#onRowExpanded"
    },
    {
        prefix: "onRowExpanding",
        detail: "A function that is executed before a row is expanded.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "In batch editing mode, if several rows have been inserted, this function will be executed for each row individually.",
            acceptValues: []
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#onRowExpanding"
    },
    {
        prefix: "onRowInserted",
        detail: "A function that is executed after a new row has been inserted into the data source.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "Do not use this function to insert data. If you need a custom insert logic, implement CustomStore's insert function.In batch editing mode, this function is executed for each row individually if several rows should be inserted.",
            acceptValues: []
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#onRowInserted"
    },
    {
        prefix: "onRowInserting",
        detail: "A function that is executed before a new row is inserted into the data source.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#onRowInserting"
    },
    {
        prefix: "onRowPrepared",
        detail: "A function that is executed after a row is created.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "In batch editing mode, if several rows have been removed, this function will be executed for each row individually.",
            acceptValues: []
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#onRowPrepared"
    },
    {
        prefix: "onRowRemoved",
        detail: "A function that is executed after a row has been removed from the data source.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "In batch editing mode, this function is executed for each row individually if several rows should be removed.",
            acceptValues: []
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#onRowRemoved"
    },
    {
        prefix: "onRowRemoving",
        detail: "A function that is executed before a row is removed from the data source.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "In batch editing mode, if several rows have been updated, this function will be executed for each row individually.",
            acceptValues: []
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#onRowRemoving"
    },
    {
        prefix: "onRowUpdated",
        detail: "A function that is executed after a row has been updated in the data source.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "You can use this function to change e.newData values, but do not use it to implement custom update logic. For this purpose, you can implement the onSaving or CustomStore's update function.In batch editing mode, this function is executed for each row individually if several rows should be updated.",
            acceptValues: []
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#onRowUpdated"
    },
    {
        prefix: "onRowUpdating",
        detail: "A function that is executed before a row is updated in the data source.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "In batch editing mode, if changes in several rows are committed simultaneously, this function is executed for each row.",
            acceptValues: []
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#onRowUpdating"
    },
    {
        prefix: "onRowValidating",
        detail: "A function that is executed after cells in a row are validated against validation rules.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#onRowValidating"
    },
    {
        prefix: "onSaved",
        detail: "A function that is executed after row changes are saved.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#onSaved"
    },
    {
        prefix: "onSaving",
        detail: "A function that is executed before pending row changes are saved.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#onSaving"
    },
    {
        prefix: "onSelectionChanged",
        detail: "A function that is executed after selecting a row or clearing its selection.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#onSelectionChanged"
    },
    {
        prefix: "pager",
        detail: "Configures the pager.",
        document: {
            type: "Object",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/pager/"
    },
    {
        prefix: "paging",
        detail: "Configures paging.",
        document: {
            type: "Object",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/paging/"
    },
    {
        prefix: "parentIdExpr",
        detail: "Specifies which data field provides parent keys.",
        document: {
            type: "String | Function",
            defaultValue: "'parentId'",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#parentIdExpr"
    },
    {
        prefix: "remoteOperations",
        detail: "Notifies the TreeList of the server's data processing operations. Applies only if data has a plain structure.",
        document: {
            type: "Object | String",
            defaultValue: "'auto'",
            description: "",
            note: "Filtering and sorting are performed on the server side for the ODataStore, but you can change them to the client side by setting the corresponding remoteOperations fields to false. Other operations are always client-side.",
            acceptValues: ['auto']
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/remoteOperations/"
    },
    {
        prefix: "renderAsync",
        detail: "Specifies whether to render the filter row, command columns, and columns with showEditorAlways set to true after other elements.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#renderAsync"
    },
    {
        prefix: "repaintChangesOnly",
        detail: "Specifies whether to repaint only those cells whose data changed.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#repaintChangesOnly"
    },
    {
        prefix: "rootValue",
        detail: "Specifies the root node's identifier. Applies if dataStructure is 'plain'.",
        document: {
            type: "any",
            defaultValue: "0",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#rootValue"
    },
    {
        prefix: "rowAlternationEnabled",
        detail: "Specifies whether rows should be shaded differently.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#rowAlternationEnabled"
    },
    {
        prefix: "rowDragging",
        detail: "Configures row reordering using drag and drop gestures.",
        document: {
            type: "Object",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/rowDragging/"
    },
    {
        prefix: "rtlEnabled",
        detail: "Switches the UI component to a right-to-left representation.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#rtlEnabled"
    },
    {
        prefix: "scrolling",
        detail: "Configures scrolling.",
        document: {
            type: "Object",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/scrolling/"
    },
    {
        prefix: "searchPanel",
        detail: "Configures the search panel.",
        document: {
            type: "Object",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/searchPanel/"
    },
    {
        prefix: "selectedRowKeys",
        detail: "Allows you to select rows or determine which rows are selected.",
        document: {
            type: "Array < any >",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#selectedRowKeys"
    },
    {
        prefix: "selection",
        detail: "Configures runtime selection.",
        document: {
            type: "Object",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/selection/"
    },
    {
        prefix: "showBorders",
        detail: "Specifies whether the outer borders of the UI component are visible.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#showBorders"
    },
    {
        prefix: "showColumnHeaders",
        detail: "Specifies whether column headers are visible.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#showColumnHeaders"
    },
    {
        prefix: "showColumnLines",
        detail: "Specifies whether vertical lines that separate one column from another are visible.",
        document: {
            type: "Boolean",
            defaultValue: "true, false (Material)",
            description: "",
            note: "If you use the Android or iOS theme, specifying this property doesn't affect anything. These themes avoid displaying column lines in order to provide a native look for the UI component. In case you still require the column lines to be displayed, choose another theme.",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#showColumnLines"
    },
    {
        prefix: "showRowLines",
        detail: "Specifies whether horizontal lines that separate one row from another are visible.",
        document: {
            type: "Boolean",
            defaultValue: "false, true (iOS, Material)",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#showRowLines"
    },
    {
        prefix: "sorting",
        detail: "Configures runtime sorting.",
        document: {
            type: "Object",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/sorting/"
    },
    {
        prefix: "stateStoring",
        detail: "Configures state storing.",
        document: {
            type: "Object",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/stateStoring/"
    },
    {
        prefix: "tabIndex",
        detail: "Specifies the number of the element when the Tab key is used for navigating.",
        document: {
            type: "Number",
            defaultValue: "0",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#tabIndex"
    },
    {
        prefix: "toolbar",
        detail: "Configures the toolbar.",
        document: {
            type: "dxTreeListToolbar",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/toolbar/"
    },
    {
        prefix: "twoWayBindingEnabled",
        detail: "Specifies whether to enable two-way data binding.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "The UI component provides two-way data binding through Knockout, Angular or AngularJS resources, so make sure to add these libraries to your app.",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#twoWayBindingEnabled"
    },
    {
        prefix: "visible",
        detail: "Specifies whether the UI component is visible.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#visible"
    },
    {
        prefix: "width",
        detail: "Specifies the UI component's width.",
        document: {
            type: "Number | String | Function",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#width"
    },
    {
        prefix: "wordWrapEnabled",
        detail: "Specifies whether text that does not fit into a column should be wrapped.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTreeList/Configuration/#wordWrapEnabled"
    },

]