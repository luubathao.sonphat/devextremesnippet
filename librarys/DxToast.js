
module.exports = [
    {
        prefix: "accessKey",
        detail: "Specifies the shortcut key that sets focus on the UI component.",
        document: {
            type: "String",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxToast/Configuration/#accessKey"
    },
    {
        prefix: "animation",
        detail: "Configures UI component visibility animations. This object contains two fields: show and hide.",
        document: {
            type: "Object",
            defaultValue: "{ show: { type: 'fade', duration: 400, from: 0, to: 1 }, hide: { type: 'fade', duration: 400, to: 0 } }, {show: {type: 'slide', duration: 200, from: { position: {my: 'top', at: 'bottom', of: window}}}, hide: { type: 'slide', duration: 200, to: { position: {my: 'top', at: 'bottom', of: window}}}} (Android)",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxToast/Configuration/animation/"
    },
    {
        prefix: "closeOnClick",
        detail: "A Boolean value specifying whether or not the toast is closed if a user clicks it.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxToast/Configuration/#closeOnClick"
    },
    {
        prefix: "closeOnOutsideClick",
        detail: "Specifies whether to close the UI component if a user clicks outside it.",
        document: {
            type: "Boolean | Function",
            defaultValue: "true (Android)",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxToast/Configuration/#closeOnOutsideClick"
    },
    {
        prefix: "closeOnSwipe",
        detail: "A Boolean value specifying whether or not the toast is closed if a user swipes it out of the screen boundaries.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxToast/Configuration/#closeOnSwipe"
    },
    {
        prefix: "contentComponent",
        detail: "An alias for the contentTemplate property specified in React. Accepts a custom component. Refer to Using a Custom Component for more information.",
        document: {
            type: "",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxToast/Configuration/#contentComponent"
    },
    {
        prefix: "contentRender",
        detail: "An alias for the contentTemplate property specified in React. Accepts a rendering function. Refer to Using a Rendering Function for more information.",
        document: {
            type: "",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxToast/Configuration/#contentRender"
    },
    {
        prefix: "contentTemplate",
        detail: "Specifies a custom template for the UI component content.",
        document: {
            type: "template",
            defaultValue: "'content'",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxToast/Configuration/#contentTemplate"
    },
    {
        prefix: "copyRootClassesToWrapper",
        detail: "Copies your custom CSS classes from the root element to the wrapper element.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxToast/Configuration/#copyRootClassesToWrapper"
    },
    {
        prefix: "deferRendering",
        detail: "Specifies whether to render the UI component's content when it is displayed. If false, the content is rendered immediately.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxToast/Configuration/#deferRendering"
    },
    {
        prefix: "displayTime",
        detail: "The time span in milliseconds during which the Toast UI component is visible.",
        document: {
            type: "Number",
            defaultValue: "2000, 4000 (Material)",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxToast/Configuration/#displayTime"
    },
    {
        prefix: "focusStateEnabled",
        detail: "Specifies whether the UI component can be focused using keyboard navigation.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxToast/Configuration/#focusStateEnabled"
    },
    {
        prefix: "height",
        detail: "Specifies the UI component's height.",
        document: {
            type: "Number | String | Function",
            defaultValue: "'auto'",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxToast/Configuration/#height"
    },
    {
        prefix: "hideOnParentScroll",
        detail: "Specifies whether to hide the Toast when users scroll one of its parent elements.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxToast/Configuration/#hideOnParentScroll"
    },
    {
        prefix: "hint",
        detail: "Specifies text for a hint that appears when a user pauses on the UI component.",
        document: {
            type: "String",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxToast/Configuration/#hint"
    },
    {
        prefix: "hoverStateEnabled",
        detail: "Specifies whether the UI component changes its state when a user pauses on it.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxToast/Configuration/#hoverStateEnabled"
    },
    {
        prefix: "maxHeight",
        detail: "Specifies the maximum height the UI component can reach while resizing.",
        document: {
            type: "Number | String | Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxToast/Configuration/#maxHeight"
    },
    {
        prefix: "maxWidth",
        detail: "Specifies the maximum width the UI component can reach while resizing.",
        document: {
            type: "Number | String | Function",
            defaultValue: "568 (Material)",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxToast/Configuration/#maxWidth"
    },
    {
        prefix: "message",
        detail: "The Toast message text.",
        document: {
            type: "String",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxToast/Configuration/#message"
    },
    {
        prefix: "minHeight",
        detail: "Specifies the minimum height the UI component can reach while resizing.",
        document: {
            type: "Number | String | Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxToast/Configuration/#minHeight"
    },
    {
        prefix: "minWidth",
        detail: "Specifies the minimum width the UI component can reach while resizing.",
        document: {
            type: "Number | String | Function",
            defaultValue: "344 (Material)",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxToast/Configuration/#minWidth"
    },
    {
        prefix: "onContentReady",
        detail: "A function that is executed when the UI component's content is ready and each time the content is changed.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxToast/Configuration/#onContentReady"
    },
    {
        prefix: "onDisposing",
        detail: "A function that is executed before the UI component is disposed of.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxToast/Configuration/#onDisposing"
    },
    {
        prefix: "onHidden",
        detail: "A function that is executed after the UI component is hidden.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxToast/Configuration/#onHidden"
    },
    {
        prefix: "onHiding",
        detail: "A function that is executed before the UI component is hidden.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxToast/Configuration/#onHiding"
    },
    {
        prefix: "onInitialized",
        detail: "A function used in JavaScript frameworks to save the UI component instance.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxToast/Configuration/#onInitialized"
    },
    {
        prefix: "onOptionChanged",
        detail: "A function that is executed after a UI component property is changed.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxToast/Configuration/#onOptionChanged"
    },
    {
        prefix: "onShowing",
        detail: "A function that is executed before the UI component is displayed.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxToast/Configuration/#onShowing"
    },
    {
        prefix: "onShown",
        detail: "A function that is executed after the UI component is displayed.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxToast/Configuration/#onShown"
    },
    {
        prefix: "position",
        detail: "Positions the UI component.",
        document: {
            type: "PositionConfig | String",
            defaultValue: "'bottom center', { at: 'bottom left', my: 'bottom left', offset: '20 -20'} (Android), { at: 'bottom center', my: 'bottom center', offset: '0 0' } (phones on Android)",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxToast/Configuration/#position"
    },
    {
        prefix: "rtlEnabled",
        detail: "Switches the UI component to a right-to-left representation.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxToast/Configuration/#rtlEnabled"
    },
    {
        prefix: "shading",
        detail: "Specifies whether to shade the background when the UI component is active.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxToast/Configuration/#shading"
    },
    {
        prefix: "shadingColor",
        detail: "Specifies the shading color. Applies only if shading is enabled.",
        document: {
            type: "String",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxToast/Configuration/#shadingColor"
    },
    {
        prefix: "tabIndex",
        detail: "Specifies the number of the element when the Tab key is used for navigating.",
        document: {
            type: "Number",
            defaultValue: "0",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxToast/Configuration/#tabIndex"
    },
    {
        prefix: "type",
        detail: "Specifies the Toast UI component type.",
        document: {
            type: "String",
            defaultValue: "'info'",
            description: "",
            note: "",
            acceptValues: ['custom', 'error', 'info', 'success', 'warning'],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxToast/Configuration/#type"
    },
    {
        prefix: "visible",
        detail: "A Boolean value specifying whether or not the UI component is visible.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxToast/Configuration/#visible"
    },
    {
        prefix: "width",
        detail: "Specifies the UI component's width.",
        document: {
            type: "Number | String | Function",
            defaultValue: "'80vw', 'auto' (Android), '100vw' (phones on Android)",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxToast/Configuration/#width"
    },
    {
        prefix: "wrapperAttr",
        detail: "Specifies the global attributes for the UI component's wrapper element",
        document: {
            type: "any",
            defaultValue: "{}",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxToast/Configuration/#wrapperAttr"
    },

]