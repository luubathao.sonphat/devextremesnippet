
module.exports = [
    {
        prefix: "elementAttr",
        detail: "Specifies the global attributes to be attached to the UI component's container element.",
        document: {
            type: "Object",
            defaultValue: "{}",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLoadIndicator/Configuration/#elementAttr"
    },
    {
        prefix: "height",
        detail: "VM346:1 Specifies the UI component's height.",
        document: {
            type: "Number | String | Function",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLoadIndicator/Configuration/#height"
    },
    {
        prefix: "hint",
        detail: "VM346:1 Specifies text for a hint that appears when a user pauses on the UI component.",
        document: {
            type: "String",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLoadIndicator/Configuration/#hint"
    },
    {
        prefix: "indicatorSrc",
        detail: "VM346:1 Specifies the path to an image used as the indicator.",
        document: {
            type: "String",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLoadIndicator/Configuration/#indicatorSrc"
    },
    {
        prefix: "onContentReady",
        detail: "VM346:1 A function that is executed when the UI component's content is ready and each time the content is changed.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLoadIndicator/Configuration/#onContentReady"
    },
    {
        prefix: "onDisposing",
        detail: "VM346:1 A function that is executed before the UI component is disposed of.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLoadIndicator/Configuration/#onDisposing"
    },
    {
        prefix: "onInitialized",
        detail: "VM346:1 A function used in JavaScript frameworks to save the UI component instance.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLoadIndicator/Configuration/#onInitialized"
    },
    {
        prefix: "onOptionChanged",
        detail: "VM346:1 A function that is executed after a UI component property is changed.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLoadIndicator/Configuration/#onOptionChanged"
    },
    {
        prefix: "rtlEnabled",
        detail: "VM346:1 Switches the UI component to a right-to-left representation.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLoadIndicator/Configuration/#rtlEnabled"
    },
    {
        prefix: "visible",
        detail: "VM346:1 Specifies whether the UI component is visible.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLoadIndicator/Configuration/#visible"
    },
    {
        prefix: "width",
        detail: "VM346:1 Specifies the UI component's width.",
        document: {
            type: "Number | String | Function",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLoadIndicator/Configuration/#width"
    },

]