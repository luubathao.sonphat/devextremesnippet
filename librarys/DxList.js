
module.exports = [
    {
        prefix: "accessKey",
        detail: "Specifies the shortcut key that sets focus on the UI component.",
        document: {
            type: "String",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxList/Configuration/#accessKey"
    },
    {
        prefix: "activeStateEnabled",
        detail: "Specifies whether or not the UI component changes its state when interacting with a user.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxList/Configuration/#activeStateEnabled"
    },
    {
        prefix: "allowItemDeleting",
        detail: "Specifies whether or not an end user can delete list items.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxList/Configuration/#allowItemDeleting"
    },
    {
        prefix: "bounceEnabled",
        detail: "A Boolean value specifying whether to enable or disable the bounce-back effect.",
        document: {
            type: "Boolean",
            defaultValue: "true, false (desktop)",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxList/Configuration/#bounceEnabled"
    },
    {
        prefix: "collapsibleGroups",
        detail: "Specifies whether or not an end-user can collapse groups.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxList/Configuration/#collapsibleGroups"
    },
    {
        prefix: "dataSource",
        detail: "Binds the UI component to data.",
        document: {
            type: "String | Array<String | dxListItem | any> | Store | DataSource | DataSource Configuration",
            defaultValue: "null",
            description: "",
            note: "Review the following notes about data binding",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxList/Configuration/#dataSource"
    },
    {
        prefix: "disabled",
        detail: "Specifies whether the UI component responds to user interaction.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxList/Configuration/#disabled"
    },
    {
        prefix: "displayExpr",
        detail: "Specifies the data field whose values should be displayed. Defaults to 'text' when the data source contains objects.",
        document: {
            type: "String | Function",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxList/Configuration/#displayExpr"
    },
    {
        prefix: "elementAttr",
        detail: "Specifies the global attributes to be attached to the UI component's container element.",
        document: {
            type: "Object",
            defaultValue: "{}",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxList/Configuration/#elementAttr"
    },
    {
        prefix: "focusStateEnabled",
        detail: "Specifies whether the UI component can be focused using keyboard navigation.",
        document: {
            type: "Boolean",
            defaultValue: "true (desktop)",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxList/Configuration/#focusStateEnabled"
    },
    {
        prefix: "groupComponent",
        detail: "An alias for the groupTemplate property specified in React. Accepts a custom component. Refer to Using a Custom Component for more information.",
        document: {
            type: "",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxList/Configuration/#groupComponent"
    },
    {
        prefix: "grouped",
        detail: "Specifies whether data items should be grouped.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "Only one-level grouping is supported.",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxList/Configuration/#grouped"
    },
    {
        prefix: "groupRender",
        detail: "An alias for the groupTemplate property specified in React. Accepts a rendering function. Refer to Using a Rendering Function for more information.",
        document: {
            type: "",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxList/Configuration/#groupRender"
    },
    {
        prefix: "groupTemplate",
        detail: "Specifies a custom template for group captions.",
        document: {
            type: "template",
            defaultValue: "'group'",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxList/Configuration/#groupTemplate"
    },
    {
        prefix: "height",
        detail: "Specifies the UI component's height.",
        document: {
            type: "Number | String | Function",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxList/Configuration/#height"
    },
    {
        prefix: "hint",
        detail: "Specifies text for a hint that appears when a user pauses on the UI component.",
        document: {
            type: "String",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxList/Configuration/#hint"
    },
    {
        prefix: "hoverStateEnabled",
        detail: "Specifies whether the UI component changes its state when a user pauses on it.",
        document: {
            defaultValue: "true",
            type: "Boolean",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxList/Configuration/#hoverStateEnabled"
    },
    {
        prefix: "indicateLoading",
        detail: "Specifies whether or not to show the loading panel when the DataSource bound to the UI component is loading data.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "This property affects only the load panel displayed when the bound DataSource is loading data after the load() method is called. The load indicator displayed when the list is being updated is always enabled regardless of the property value.",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxList/Configuration/#indicateLoading"
    },
    {
        prefix: "itemComponent",
        detail: "An alias for the itemTemplate property specified in React. Accepts a custom component. Refer to Using a Custom Component for more information.",
        document: {
            type: "",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxList/Configuration/#itemComponent"
    },
    {
        prefix: "itemDeleteMode",
        detail: "Specifies the way a user can delete items from the list.",
        document: {
            type: "String",
            defaultValue: "'static', 'slideItem' (iOS), 'swipe' (Android)",
            description: "",
            note: "If List items are supplied with the context menu, this property is ignored in favor of the menuMode property.",
            acceptValues: ['context', 'slideButton', 'slideItem', 'static', 'swipe', 'toggle'],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxList/Configuration/#itemDeleteMode"
    },
    {
        prefix: "itemDragging",
        detail: "Configures item reordering using drag and drop gestures.",
        document: {
            type: "Sortable configuration",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxList/Configuration/#itemDragging"
    },
    {
        prefix: "itemHoldTimeout",
        detail: "The time period in milliseconds before the onItemHold event is raised.",
        document: {
            type: "Number",
            defaultValue: "750",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxList/Configuration/#itemHoldTimeout"
    },
    {
        prefix: "itemRender",
        detail: "An alias for the itemTemplate property specified in React. Accepts a rendering function. Refer to Using a Rendering Function for more information.",
        document: {
            type: "",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxList/Configuration/#itemRender"
    },
    {
        prefix: "items",
        detail: "An array of items displayed by the UI component.",
        document: {
            type: "Array<String | dxListItem | any>",
            defaultValue: "",
            description: "",
            note: "Do not use the items property if you use dataSource, and vice versa.",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxList/Configuration/items/"
    },
    {
        prefix: "itemTemplate",
        detail: "Specifies a custom template for items.",
        document: {
            type: "template",
            defaultValue: "'item'",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxList/Configuration/#itemTemplate"
    },
    {
        prefix: "keyExpr",
        detail: "Specifies the key property that provides key values to access data items. Each key value must be unique.",
        document: {
            type: "String | Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxList/Configuration/#keyExpr"
    },
    {
        prefix: "menuItems",
        detail: "Specifies the array of items for a context menu called for a list item.",
        document: {
            type: "Array<Object>",
            defaultValue: "[]",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxList/Configuration/menuItems/"
    },
    {
        prefix: "menuMode",
        detail: "Specifies whether an item context menu is shown when a user holds or swipes an item.",
        document: {
            type: "String",
            defaultValue: "'context', 'slide' (iOS)",
            description: "",
            note: "",
            acceptValues: ['context', 'slide'],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxList/Configuration/#menuMode"
    },
    {
        prefix: "nextButtonText",
        detail: "The text displayed on the button used to load the next page from the data source.",
        document: {
            type: "String",
            defaultValue: "'More'",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxList/Configuration/#nextButtonText"
    },
    {
        prefix: "noDataText",
        detail: "Specifies the text or HTML markup displayed by the UI component if the item collection is empty.",
        document: {
            type: "String",
            defaultValue: "'No data to display'",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxList/Configuration/#noDataText"
    },
    {
        prefix: "onContentReady",
        detail: "A function that is executed when the UI component's content is ready and each time the content is changed.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxList/Configuration/#onContentReady"
    },
    {
        prefix: "onDisposing",
        detail: "A function that is executed before the UI component is disposed of.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxList/Configuration/#onDisposing"
    },
    {
        prefix: "onGroupRendered",
        detail: "A function that is executed when a group element is rendered.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxList/Configuration/#onGroupRendered"
    },
    {
        prefix: "onInitialized",
        detail: "A function used in JavaScript frameworks to save the UI component instance.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxList/Configuration/#onInitialized"
    },
    {
        prefix: "onItemClick",
        detail: "A function that is executed when a collection item is clicked or tapped.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxList/Configuration/#onItemClick"
    },
    {
        prefix: "onItemContextMenu",
        detail: "A function that is executed when a collection item is right-clicked or pressed.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxList/Configuration/#onItemContextMenu"
    },
    {
        prefix: "onItemDeleted",
        detail: "A function that is executed after a list item is deleted from the data source.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxList/Configuration/#onItemDeleted"
    },
    {
        prefix: "onItemDeleting",
        detail: "A function that is executed before a collection item is deleted from the data source.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxList/Configuration/#onItemDeleting"
    },
    {
        prefix: "onItemHold",
        detail: "A function that is executed when a collection item has been held for a specified period.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxList/Configuration/#onItemHold"
    },
    {
        prefix: "onItemRendered",
        detail: "A function that is executed after a collection item is rendered.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxList/Configuration/#onItemRendered"
    },
    {
        prefix: "onItemReordered",
        detail: "A function that is executed after a list item is moved to another position.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxList/Configuration/#onItemReordered"
    },
    {
        prefix: "onItemSwipe",
        detail: "A function that is executed when a list item is swiped.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxList/Configuration/#onItemSwipe"
    },
    {
        prefix: "onOptionChanged",
        detail: "A function that is executed after a UI component property is changed.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxList/Configuration/#onOptionChanged"
    },
    {
        prefix: "onPageLoading",
        detail: "A function that is executed before the next page is loaded.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxList/Configuration/#onPageLoading"
    },
    {
        prefix: "onPullRefresh",
        detail: "A function that is executed when the 'pull to refresh' gesture is performed. Supported on mobile devices only.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxList/Configuration/#onPullRefresh"
    },
    {
        prefix: "onScroll",
        detail: "A function that is executed on each scroll gesture.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxList/Configuration/#onScroll"
    },
    {
        prefix: "onSelectAllValueChanged",
        detail: "A function that is executed when the 'Select All' check box value is changed. Applies only if the selectionMode is 'all'.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxList/Configuration/#onSelectAllValueChanged"
    },
    {
        prefix: "onSelectionChanged",
        detail: "A function that is executed when a collection item is selected or selection is canceled.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxList/Configuration/#onSelectionChanged"
    },
    {
        prefix: "pageLoadingText",
        detail: "Specifies the text shown in the pullDown panel, which is displayed when the list is scrolled to the bottom.",
        document: {
            type: "String",
            defaultValue: "'Loading...',  (Material)",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxList/Configuration/#pageLoadingText"
    },
    {
        prefix: "pageLoadMode",
        detail: "Specifies whether the next page is loaded when a user scrolls the UI component to the bottom or when the 'next' button is clicked.",
        document: {
            type: "String",
            defaultValue: "'scrollBottom', 'nextButton' (desktop except Mac)",
            description: "",
            note: "",
            acceptValues: ['nextButton', 'scrollBottom'],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxList/Configuration/#pageLoadMode"
    },
    {
        prefix: "pulledDownText",
        detail: "Specifies the text displayed in the pullDown panel when the list is pulled below the refresh threshold.",
        document: {
            type: "String",
            defaultValue: "'Release to refresh...',  (Material)",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxList/Configuration/#pulledDownText"
    },
    {
        prefix: "pullingDownText",
        detail: "Specifies the text shown in the pullDown panel while the list is being pulled down to the refresh threshold.",
        document: {
            type: "String",
            defaultValue: "'Pull down to refresh...',  (Material)",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxList/Configuration/#pullingDownText"
    },
    {
        prefix: "pullRefreshEnabled",
        detail: "A Boolean value specifying whether or not the UI component supports the 'pull down to refresh' gesture.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "The 'pull down to refresh' gesture is not supported by desktop browsers. You can use it only on mobile devices.",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxList/Configuration/#pullRefreshEnabled"
    },
    {
        prefix: "refreshingText",
        detail: "Specifies the text displayed in the pullDown panel while the list is being refreshed.",
        document: {
            type: "String",
            defaultValue: "'Refreshing...',  (Material)",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxList/Configuration/#refreshingText"
    },
    {
        prefix: "repaintChangesOnly",
        detail: "Specifies whether to repaint only those elements whose data changed.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxList/Configuration/#repaintChangesOnly"
    },
    {
        prefix: "rtlEnabled",
        detail: "Switches the UI component to a right-to-left representation.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxList/Configuration/#rtlEnabled"
    },
    {
        prefix: "scrollByContent",
        detail: "A Boolean value specifying if the list is scrolled by content.",
        document: {
            type: "Boolean",
            defaultValue: "true, false (non-touch devices)",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxList/Configuration/#scrollByContent"
    },
    {
        prefix: "scrollByThumb",
        detail: "Specifies whether a user can scroll the content with the scrollbar. Applies only if useNativeScrolling is false.",
        document: {
            type: "Boolean",
            defaultValue: "false, true (desktop)",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxList/Configuration/#scrollByThumb"
    },
    {
        prefix: "scrollingEnabled",
        detail: "A Boolean value specifying whether to enable or disable list scrolling.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxList/Configuration/#scrollingEnabled"
    },
    {
        prefix: "searchEditorOptions",
        detail: "Configures the search panel.",
        document: {
            type: "TextBox Configuration",
            defaultValue: "{}",
            description: "",
            note: "In Angular and Vue, the nested component that configures the searchEditorOptions property does not support event bindings and two-way property bindings.",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxList/Configuration/#searchEditorOptions"
    },
    {
        prefix: "searchEnabled",
        detail: "Specifies whether the search panel is visible.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "Searching works only if data is specified using the dataSource property and has a plain structure. Subsequently, data can be transformed to hierarchical structure using the DataSource's group property.",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxList/Configuration/#searchEnabled"
    },
    {
        prefix: "searchExpr",
        detail: "Specifies a data object's field name or an expression whose value is compared to the search string.",
        document: {
            type: "getter | Array<getter>",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxList/Configuration/#searchExpr"
    },
    {
        prefix: "searchMode",
        detail: "Specifies a comparison operation used to search UI component items.",
        document: {
            type: "String",
            defaultValue: "'contains'",
            description: "",
            note: "",
            acceptValues: ['contains', 'startswith', 'equals'],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxList/Configuration/#searchMode"
    },
    {
        prefix: "searchTimeout",
        detail: "Specifies a delay in milliseconds between when a user finishes typing, and the search is executed.",
        document: {
            type: "Number",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxList/Configuration/#searchTimeout"
    },
    {
        prefix: "searchValue",
        detail: "Specifies the current search string.",
        document: {
            type: "String",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxList/Configuration/#searchValue"
    },
    {
        prefix: "selectAllMode",
        detail: "Specifies the mode in which all items are selected.",
        document: {
            type: "String",
            defaultValue: "'page'",
            description: "",
            note: "The selectAllMode applies only if the selectionMode is set to all and the selection controls are shown.",
            acceptValues: ['allPages', 'page'],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxList/Configuration/#selectAllMode"
    },
    {
        prefix: "selectAllText",
        detail: "Specifies the text displayed at the 'Select All' check box.",
        document: {
            type: "String",
            defaultValue: "'Select All'",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxList/Configuration/#selectAllText"
    },
    {
        prefix: "selectedItemKeys",
        detail: "Specifies an array of currently selected item keys.",
        document: {
            type: "Array<any>",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxList/Configuration/#selectedItemKeys"
    },
    {
        prefix: "selectedItems",
        detail: "An array of currently selected item objects.",
        document: {
            type: "Array<any>",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxList/Configuration/#selectedItems"
    },
    {
        prefix: "selectionMode",
        detail: "Specifies item selection mode.",
        document: {
            type: "String",
            defaultValue: "'none'",
            description: "",
            note: "",
            acceptValues: ['all', 'multiple', 'none', 'single'],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxList/Configuration/#selectionMode"
    },
    {
        prefix: "showScrollbar",
        detail: "Specifies when the UI component shows the scrollbar.",
        document: {
            type: "String",
            defaultValue: "'onScroll', 'onHover' (desktop)",
            description: "",
            note: "",
            acceptValues: ['always', 'never', 'onHover', 'onScroll'],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxList/Configuration/#showScrollbar"
    },
    {
        prefix: "showSelectionControls",
        detail: "Specifies whether or not to display controls used to select list items.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxList/Configuration/#showSelectionControls"
    },
    {
        prefix: "tabIndex",
        detail: "Specifies the number of the element when the Tab key is used for navigating.",
        document: {
            type: "Number",
            defaultValue: "0",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxList/Configuration/#tabIndex"
    },
    {
        prefix: "useNativeScrolling",
        detail: "Specifies whether or not the UI component uses native scrolling.",
        document: {
            type: "Boolean",
            defaultValue: "true, false (desktop except Mac)",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxList/Configuration/#useNativeScrolling"
    },
    {
        prefix: "visible",
        detail: "Specifies whether the UI component is visible.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxList/Configuration/#visible"
    },
    {
        prefix: "width",
        detail: "Specifies the UI component's width.",
        document: {
            type: "Number | String | Function",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxList/Configuration/#width"
    },

]