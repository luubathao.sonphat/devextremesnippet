
module.exports = [
    {
        prefix: "accessKey",
        detail: "Specifies the shortcut key that sets focus on the UI component.",
        document: {
            type: "String",
            defaultValue: "undefined",
            description: "The value of this property will be passed to the accesskey attribute of the HTML element that underlies the UI component.",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#accessKey"
    },
    {
        prefix: "activeStateEnabled",
        detail: "Specifies whether or not the UI component changes its state when interacting with a user.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "This property is used when the UI component is displayed on a platform whose guidelines include the active state change for UI components.",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#activeStateEnabled"
    },
    {
        prefix: "allowColumnReordering",
        detail: "Specifies whether a user can reorder columns.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "Built-in buttons should also be declared in this array. You can find an example in the following demo:",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#allowColumnReordering"
    },
    {
        prefix: "allowColumnResizing",
        detail: "Specifies whether a user can resize columns.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#allowColumnResizing"
    },
    {
        prefix: "autoNavigateToFocusedRow",
        detail: "Automatically scrolls to the focused row when the focusedRowKey is changed. Incompatible with infinite scrolling mode.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#autoNavigateToFocusedRow"
    },
    {
        prefix: "cacheEnabled",
        detail: "Specifies whether data should be cached.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "If you fetch data from the server, some operations with data can be executed remotely, while others - locally. If you perform basic operations (sorting, filtering, and paging) remotely and advanced operations (grouping and summary calculation) locally, certain user actions will force the DataGrid to query the server for data repeatedly despite caching being enabled. Particularly, the advanced operations demand data to be reloaded completely from the server to provide correct results.",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#cacheEnabled"
    },
    {
        prefix: "cellHintEnabled",
        detail: "Enables a hint that appears when a user hovers the mouse pointer over a cell with truncated content.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "The UI component's height.",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#cellHintEnabled"
    },
    {
        prefix: "columnAutoWidth",
        detail: "Specifies whether columns should adjust their widths to the content.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#columnAutoWidth"
    },
    {
        prefix: "columnChooser",
        detail: "Configures the column chooser.",
        document: {
            type: "Object",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/columnChooser/"
    },
    {
        prefix: "columnFixing",
        detail: "Configures column fixing.",
        document: {
            type: "Object",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/columnFixing/"
    },
    {
        prefix: "columnHidingEnabled",
        detail: "Specifies whether the UI component should hide columns to adapt to the screen or container size. Ignored if allowColumnResizing is true and columnResizingMode is 'widget'.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#columnHidingEnabled"
    },
    {
        prefix: "columnMinWidth",
        detail: "Specifies the minimum width of columns.",
        document: {
            type: "Number",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#columnMinWidth"
    },
    {
        prefix: "columnResizingMode",
        detail: "Specifies how the UI component resizes columns. Applies only if allowColumnResizing is true.",
        document: {
            type: "String",
            defaultValue: "'nextColumn'",
            description: "",
            note: "",
            acceptValues: ['nextColumn', 'widget'],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#columnResizingMode"
    },
    {
        prefix: "columns",
        detail: "An array of grid columns.",
        document: {
            type: "Array<DataGrid Column | String>",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/columns/"
    },
    {
        prefix: "columnWidth",
        detail: "Specifies the width for all data columns. Has a lower priority than the column.width property.",
        document: {
            type: "'Number | String'",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: ['auto'],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#columnWidth"
    },
    {
        prefix: "customizeColumns",
        detail: "Customizes columns after they are created.",
        document: {
            type: "Function",
            defaultValue: "",
            description: "",
            note: "Data operations (sorting, filtering, summary) are unavailable for the columns created via customizeColumns. To create a fully functioning column, add it to the columns array.",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#customizeColumns"
    },
    {
        prefix: "dataRowComponent",
        detail: "An alias for the dataRowTemplate property specified in React. Accepts a custom component. Refer to Using a Custom Component for more information.",
        document: {
            type: "",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#dataRowComponent"
    },
    {
        prefix: "dataRowRender",
        detail: "An alias for the dataRowTemplate property specified in React. Accepts a rendering function. Refer to Using a Rendering Function for more information.",
        document: {
            type: "",
            defaultValue: "",
            description: "If the number of entered characters reaches the value assigned to this property, the UI component does not allow you to enter any more characters",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#dataRowRender"
    },
    {
        prefix: "dataRowTemplate",
        detail: "Specifies a custom template for data rows.",
        document: {
            type: "template",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#dataRowTemplate"
    },
    {
        prefix: "dataSource",
        detail: "Binds the UI component to data.",
        document: {
            type: "'Store | DataSource | DataSource Configuration | String | Array<any>'",
            defaultValue: "null",
            description: "Specify this property if the UI component lies within an HTML form that will be submitted",
            note: "Review the following notes about data binding:",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#dataSource"
    },
    {
        prefix: "dateSerializationFormat",
        detail: "Specifies the format in which date-time values should be sent to the server.",
        document: {
            type: "String",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#dateSerializationFormat"
    },
    {
        prefix: "disabled",
        detail: "Specifies whether the UI component responds to user interaction.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#disabled"
    },
    {
        prefix: "editing",
        detail: "Configures editing.",
        document: {
            type: "Object",
            defaultValue: "",
            description: "",
            note: "Before allowing a user to add, update, and delete, make sure that your data source supports these actions.",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/editing/"
    },
    {
        prefix: "elementAttr",
        detail: "Specifies the global attributes to be attached to the UI component's container element.",
        document: {
            type: "Object",
            defaultValue: "{}",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#elementAttr"
    },
    {
        prefix: "errorRowEnabled",
        detail: "Indicates whether to show the error row.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#errorRowEnabled"
    },
    {
        prefix: "export",
        detail: "Configures client-side exporting.",
        document: {
            type: "Object",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/export/"
    },
    {
        prefix: "filterBuilder",
        detail: "Configures the integrated filter builder.",
        document: {
            type: "FilterBuilder Configuration",
            defaultValue: "{}",
            description: "",
            note: "In Angular and Vue, the nested component that configures the filterBuilder property does not support event bindings and two-way property bindings.",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#filterBuilder"
    },
    {
        prefix: "filterBuilderPopup",
        detail: "Configures the popup in which the integrated filter builder is shown.",
        document: {
            type: "Popup Configuration",
            defaultValue: "{}",
            description: "",
            note: "In Angular and Vue, the nested component that configures the filterBuilderPopup property does not support event bindings and two-way property bindings.",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#filterBuilderPopup"
    },
    {
        prefix: "filterPanel",
        detail: "Configures the filter panel.",
        document: {
            type: "Object",
            defaultValue: "{}",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/filterPanel/"
    },
    {
        prefix: "filterRow",
        detail: "Configures the filter row.",
        document: {
            type: "Object",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/filterRow/"
    },
    {
        prefix: "filterSyncEnabled",
        detail: "Specifies whether to synchronize the filter row, header filter, and filter builder. The synchronized filter expression is stored in the filterValue property.",
        document: {
            type: "'Boolean | String'",
            defaultValue: "'auto'",
            description: "",
            note: "",
            acceptValues: ['auto'],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#filterSyncEnabled"
    },
    {
        prefix: "filterValue",
        detail: "Specifies a filter expression.",
        document: {
            type: "Filter Expression",
            defaultValue: "null",
            description: "",
            note: "The DataSource does not support the 'between', 'anyof', 'noneof', and custom operations. Use the getCombinedFilter(returnDataField) method to get the DataSource-compatible filter expression.",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#filterValue"
    },
    {
        prefix: "focusedColumnIndex",
        detail: "The index of the column that contains the focused data cell. This index is taken from the columns array.",
        document: {
            type: "Number",
            defaultValue: "-1",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#focusedColumnIndex"
    },
    {
        prefix: "focusedRowEnabled",
        detail: "Specifies whether the focused row feature is enabled.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "Specify the UI component's keyExpr or the Store's key property to ensure that the focused row feature works properly.",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#focusedRowEnabled"
    },
    {
        prefix: "focusedRowIndex",
        detail: "Specifies or indicates the focused data row's index.",
        document: {
            type: "Number",
            defaultValue: "-1",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#focusedRowIndex"
    },
    {
        prefix: "focusedRowKey",
        detail: "Specifies initially or currently focused grid row's key.",
        document: {
            type: "any",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#focusedRowKey"
    },
    {
        prefix: "focusStateEnabled",
        detail: "Specifies whether the UI component can be focused using keyboard navigation.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#focusStateEnabled"
    },
    {
        prefix: "grouping",
        detail: "Configures grouping.",
        document: {
            type: "Object",
            defaultValue: "",
            description: "When this property is set to true, the UI component text flows from right to left, and the layout of elements is reversed",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/grouping/"
    },
    {
        prefix: "groupPanel",
        detail: "Configures the group panel.",
        document: {
            type: "Object",
            defaultValue: "",
            description: "The Clear button calls the reset method",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/groupPanel/"
    },
    {
        prefix: "headerFilter",
        detail: "Configures the header filter feature.",
        document: {
            type: "Object",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/headerFilter/"
    },
    {
        prefix: "height",
        detail: "Specifies the UI component's height.",
        document: {
            type: "'Number | String | Function'",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#height"
    },
    {
        prefix: "highlightChanges",
        detail: "Specifies whether to highlight rows and cells with edited data. repaintChangesOnly should be true.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "You can also use the global editorStylingMode setting to specify how the text fields of all editors in your application are styled",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#highlightChanges"
    },
    {
        prefix: "hint",
        detail: "Specifies text for a hint that appears when a user pauses on the UI component.",
        document: {
            type: "String",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#hint"
    },
    {
        prefix: "hoverStateEnabled",
        detail: "Specifies whether to highlight rows when a user moves the mouse pointer over them.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "Read-only",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#hoverStateEnabled"
    },
    {
        prefix: "keyboardNavigation",
        detail: "Configures keyboard navigation.",
        document: {
            type: "Object",
            defaultValue: "",
            description: "If false, the value contains raw user input; if true, the value contains mask characters as well. This property applies only if a mask is specified",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/keyboardNavigation/"
    },
    {
        prefix: "keyExpr",
        detail: "Specifies the key property (or properties) that provide(s) key values to access data items. Each key value must be unique. This property applies only if data is a simple array.",
        document: {
            type: "'String | Array<String>'",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#keyExpr"
    },
    {
        prefix: "loadPanel",
        detail: "Configures the load panel.",
        document: {
            type: "Object",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/loadPanel/"
    },
    {
        prefix: "masterDetail",
        detail: "Allows you to build a master-detail interface in the grid.",
        document: {
            type: "Object",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/masterDetail/"
    },
    {
        prefix: "noDataText",
        detail: "Specifies a text string shown when the DataGrid does not display any data.",
        document: {
            type: "String",
            defaultValue: "'No data'",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#noDataText"
    },
    {
        prefix: "onAdaptiveDetailRowPreparing",
        detail: "A function that is executed before an adaptive detail row is rendered.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "The recommended events are 'keyup', 'blur', 'change', 'input', and 'focusout', but you can use other events as well",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#onAdaptiveDetailRowPreparing"
    },
    {
        prefix: "onCellClick",
        detail: "A function that is executed when a cell is clicked or tapped. Executed before onRowClick.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#onCellClick"
    },
    {
        prefix: "onCellDblClick",
        detail: "A function that is executed when a cell is double-clicked or double-tapped. Executed before onRowDblClick.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#onCellDblClick"
    },
    {
        prefix: "onCellHoverChanged",
        detail: "A function that is executed after the pointer enters or leaves a cell.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "The value of this property will be passed to the accesskey attribute of the HTML element that underlies the UI component.",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#onCellHoverChanged"
    },
    {
        prefix: "onCellPrepared",
        detail: "A function that is executed after a grid cell is created.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "This property is used when the UI component is displayed on a platform whose guidelines include the active state change for UI components.",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#onCellPrepared"
    },
    {
        prefix: "onContentReady",
        detail: "A function that is executed when the UI component's content is ready and each time the content is changed.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "Built-in buttons should also be declared in this array. You can find an example in the following demo:",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#onContentReady"
    },
    {
        prefix: "onContextMenuPreparing",
        detail: "A function that is executed before the context menu is rendered.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#onContextMenuPreparing"
    },
    {
        prefix: "onDataErrorOccurred",
        detail: "A function that is executed when an error occurs in the data source.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#onDataErrorOccurred"
    },
    {
        prefix: "onDisposing",
        detail: "A function that is executed before the UI component is disposed of.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#onDisposing"
    },
    {
        prefix: "onEditCanceled",
        detail: "A function that is executed after row changes are discarded.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "The UI component's height.",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#onEditCanceled"
    },
    {
        prefix: "onEditCanceling",
        detail: "A function that is executed when the edit operation is canceled, but row changes are not yet discarded.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#onEditCanceling"
    },
    {
        prefix: "onEditingStart",
        detail: "A function that is executed before a cell or row switches to the editing state.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#onEditingStart"
    },
    {
        prefix: "onEditorPrepared",
        detail: "A function that is executed after an editor is created. Not executed for cells with an editCellTemplate.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#onEditorPrepared"
    },
    {
        prefix: "onEditorPreparing",
        detail: "A function used to customize a cell's editor. Not executed for cells with an editCellTemplate.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "We do not recommend that you use the onEditorPreparing function to specify an editor's default value. Use the onInitNewRow function instead.",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#onEditorPreparing"
    },
    {
        prefix: "onExporting",
        detail: "A function that is executed before data is exported.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#onExporting"
    },
    {
        prefix: "onFocusedCellChanged",
        detail: "A function that is executed after the focused cell changes. Applies only to cells in data or group rows.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#onFocusedCellChanged"
    },
    {
        prefix: "onFocusedCellChanging",
        detail: "A function that is executed before the focused cell changes. Applies only to cells in data or group rows.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#onFocusedCellChanging"
    },
    {
        prefix: "onFocusedRowChanged",
        detail: "A function that is executed after the focused row changes. Applies only to data or group rows. focusedRowEnabled should be true.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#onFocusedRowChanged"
    },
    {
        prefix: "onFocusedRowChanging",
        detail: "A function that is executed before the focused row changes. Applies only to data or group rows. focusedRowEnabled should be true.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#onFocusedRowChanging"
    },
    {
        prefix: "onInitialized",
        detail: "A function used in JavaScript frameworks to save the UI component instance.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#onInitialized"
    },
    {
        prefix: "onInitNewRow",
        detail: "A function that is executed before a new row is added to the UI component.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "If the number of entered characters reaches the value assigned to this property, the UI component does not allow you to enter any more characters",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#onInitNewRow"
    },
    {
        prefix: "onKeyDown",
        detail: "A function that is executed when the UI component is in focus and a key has been pressed down.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#onKeyDown"
    },
    {
        prefix: "onOptionChanged",
        detail: "A function that is executed after a UI component property is changed.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "Specify this property if the UI component lies within an HTML form that will be submitted",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#onOptionChanged"
    },
    {
        prefix: "onRowClick",
        detail: "A function that is executed when a row is clicked or tapped.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "The onRowClick function is not executed when the clicked row enters or is in the editing state. Instead, specify the onCellClick function.",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#onRowClick"
    },
    {
        prefix: "onRowCollapsed",
        detail: "A function that is executed after a row is collapsed.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#onRowCollapsed"
    },
    {
        prefix: "onRowCollapsing",
        detail: "A function that is executed before a row is collapsed.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#onRowCollapsing"
    },
    {
        prefix: "onRowDblClick",
        detail: "A function that is executed when a row is double-clicked or double-tapped. Executed after onCellDblClick.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "onRowDblClick is not executed when the clicked row enters or is in the editing state. You can use onCellDblClick instead.",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#onRowDblClick"
    },
    {
        prefix: "onRowExpanded",
        detail: "A function that is executed after a row is expanded.",
        document: {
            function: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#onRowExpanded"
    },
    {
        prefix: "onRowExpanding",
        detail: "A function that is executed before a row is expanded.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#onRowExpanding"
    },
    {
        prefix: "onRowInserted",
        detail: "A function that is executed after a new row has been inserted into the data source.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "In batch editing mode, if several rows have been inserted, this function will be executed for each row individually.",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#onRowInserted"
    },
    {
        prefix: "onRowInserting",
        detail: "A function that is executed before a new row is inserted into the data source.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "Do not use this function to insert data. If you need a custom insert logic, implement CustomStore's insert function.In batch editing mode, this function is executed for each row individually if several rows should be inserted.",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#onRowInserting"
    },
    {
        prefix: "onRowPrepared",
        detail: "A function that is executed after a row is created.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#onRowPrepared"
    },
    {
        prefix: "onRowRemoved",
        detail: "A function that is executed after a row has been removed from the data source.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "In batch editing mode, if several rows have been removed, this function will be executed for each row individually.",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#onRowRemoved"
    },
    {
        prefix: "onRowRemoving",
        detail: "A function that is executed before a row is removed from the data source.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "In batch editing mode, this function is executed for each row individually if several rows should be removed.",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#onRowRemoving"
    },
    {
        prefix: "onRowUpdated",
        detail: "A function that is executed after a row has been updated in the data source.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "In batch editing mode, if several rows have been updated, this function will be executed for each row individually.",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#onRowUpdated"
    },
    {
        prefix: "onRowUpdating",
        detail: "A function that is executed before a row is updated in the data source.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "You can use this function to change e.newData values, but do not use it to implement custom update logic. For this purpose, you can implement the onSaving or CustomStore's update function.In batch editing mode, this function is executed for each row individually if several rows should be updated.",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#onRowUpdating"
    },
    {
        prefix: "onRowValidating",
        detail: "A function that is executed after cells in a row are validated against validation rules.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "In batch editing mode, if changes in several rows are committed simultaneously, this function is executed for each row.",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#onRowValidating"
    },
    {
        prefix: "onSaved",
        detail: "A function that is executed after row changes are saved.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#onSaved"
    },
    {
        prefix: "onSaving",
        detail: "A function that is executed before pending row changes are saved.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#onSaving"
    },
    {
        prefix: "onSelectionChanged",
        detail: "A function that is executed after selecting a row or clearing its selection.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#onSelectionChanged"
    },
    {
        prefix: "pager",
        detail: "Configures the pager.",
        document: {
            type: "Object",
            defaultValue: "",
            description: "When this property is set to true, the UI component text flows from right to left, and the layout of elements is reversed",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/pager/"
    },
    {
        prefix: "paging",
        detail: "Configures paging.",
        document: {
            type: "Object",
            defaultValue: "",
            description: "The Clear button calls the reset method",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/paging/"
    },
    {
        prefix: "remoteOperations",
        detail: "Notifies the DataGrid of the server's data processing operations.",
        document: {
            type: "'Boolean | Object | String'",
            defaultValue: "'auto'",
            description: "",
            note: "Paging, filtering, and sorting are performed on the server side for the ODataStore, but you can change them to the client side by setting the corresponding remoteOperations fields to false. Other operations are always client-side.",
            acceptValues: ['auto'],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/remoteOperations/"
    },
    {
        prefix: "renderAsync",
        detail: "Specifies whether to render the filter row, command columns, and columns with showEditorAlways set to true after other elements.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#renderAsync"
    },
    {
        prefix: "repaintChangesOnly",
        detail: "Specifies whether to repaint only those cells whose data changed.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "You can also use the global editorStylingMode setting to specify how the text fields of all editors in your application are styled",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#repaintChangesOnly"
    },
    {
        prefix: "rowAlternationEnabled",
        detail: "Specifies whether rows should be shaded differently.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#rowAlternationEnabled"
    },
    {
        prefix: "rowDragging",
        detail: "Configures row reordering using drag and drop gestures.",
        document: {
            type: "Object",
            defaultValue: "",
            description: "Read-only",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/rowDragging/"
    },
    {
        prefix: "rtlEnabled",
        detail: "Switches the UI component to a right-to-left representation.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "If false, the value contains raw user input; if true, the value contains mask characters as well. This property applies only if a mask is specified",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#rtlEnabled"
    },
    {
        prefix: "scrolling",
        detail: "Configures scrolling.",
        document: {
            type: "Object",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/scrolling/"
    },
    {
        prefix: "searchPanel",
        detail: "Configures the search panel.",
        document: {
            type: "Object",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/searchPanel/"
    },
    {
        prefix: "selectedRowKeys",
        detail: "Allows you to select rows or determine which rows are selected. Applies only if selection.deferred is false.",
        document: {
            type: "Array<any>",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#selectedRowKeys"
    },
    {
        prefix: "selection",
        detail: "Configures runtime selection.",
        document: {
            type: "Object",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/selection/"
    },
    {
        prefix: "selectionFilter",
        detail: "Specifies filters for the rows that must be selected initially. Applies only if selection.deferred is true.",
        document: {
            type: "Filter Expression",
            defaultValue: "[]",
            description: "The recommended events are 'keyup', 'blur', 'change', 'input', and 'focusout', but you can use other events as well",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#selectionFilter"
    },
    {
        prefix: "showBorders",
        detail: "Specifies whether the outer borders of the UI component are visible.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#showBorders"
    },
    {
        prefix: "showColumnHeaders",
        detail: "Specifies whether column headers are visible.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#showColumnHeaders"
    },
    {
        prefix: "showColumnLines",
        detail: "Specifies whether vertical lines that separate one column from another are visible.",
        document: {
            type: "Boolean",
            defaultValue: "true, false (Material)",
            description: "",
            note: "If you use the Android or iOS theme, specifying this property doesn't affect anything. These themes avoid displaying column lines in order to provide a native look for the UI component. In case you still require the column lines to be displayed, choose another theme.",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#showColumnLines"
    },
    {
        prefix: "showRowLines",
        detail: "Specifies whether horizontal lines that separate one row from another are visible.",
        document: {
            type: "Boolean",
            defaultValue: "false, true (iOS, Material)",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#showRowLines"
    },
    {
        prefix: "sortByGroupSummaryInfo",
        detail: "Allows you to sort groups according to the values of group summary items.",
        document: {
            type: "Array<Object>",
            defaultValue: "undefined",
            description: "",
            note: "This feature does not work when remoteOperations.groupPaging is set to true.",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/sortByGroupSummaryInfo/"
    },
    {
        prefix: "sorting",
        detail: "Configures runtime sorting.",
        document: {
            type: "Object",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/sorting/"
    },
    {
        prefix: "stateStoring",
        detail: "Configures state storing.",
        document: {
            type: "Object",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/stateStoring/"
    },
    {
        prefix: "summary",
        detail: "Specifies the properties of the grid summary.",
        document: {
            type: "Object",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/summary/"
    },
    {
        prefix: "tabIndex",
        detail: "Specifies the number of the element when the Tab key is used for navigating.",
        document: {
            type: "Number",
            defaultValue: "0",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#tabIndex"
    },
    {
        prefix: "toolbar",
        detail: "Configures the toolbar.",
        document: {
            type: "dxDataGridToolbar",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/toolbar/"
    },
    {
        prefix: "twoWayBindingEnabled",
        detail: "Specifies whether to enable two-way data binding.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "The UI component provides two-way data binding through Knockout, Angular or AngularJS resources, so make sure to add these libraries to your app.",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#twoWayBindingEnabled"
    },
    {
        prefix: "visible",
        detail: "Specifies whether the UI component is visible.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#visible"
    },
    {
        prefix: "width",
        detail: "Specifies the UI component's width.",
        document: {
            type: "'Number | String | Function'",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#width"
    },
    {
        prefix: "wordWrapEnabled",
        detail: "Specifies whether text that does not fit into a column should be wrapped.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/#wordWrapEnabled"
    },

]