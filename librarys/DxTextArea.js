
module.exports = [

    {
        prefix: "accessKey",
        detail: "Specifies the shortcut key that sets focus on the UI component.",
        document: {
            type: "String",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTextArea/Configuration/#accessKey"
    },
    {
        prefix: "activeStateEnabled",
        detail: "Specifies whether or not the UI component changes its state when interacting with a user.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTextArea/Configuration/#activeStateEnabled"
    },
    {
        prefix: "autoResizeEnabled",
        detail: "A Boolean value specifying whether or not the auto resizing mode is enabled.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTextArea/Configuration/#autoResizeEnabled"
    },
    {
        prefix: "disabled",
        detail: "Specifies whether the UI component responds to user interaction.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTextArea/Configuration/#disabled"
    },
    {
        prefix: "elementAttr",
        detail: "Specifies the global attributes to be attached to the UI component's container element.",
        document: {
            type: "Object",
            defaultValue: "{}",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTextArea/Configuration/#elementAttr"
    },
    {
        prefix: "focusStateEnabled",
        detail: "Specifies whether the UI component can be focused using keyboard navigation.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTextArea/Configuration/#focusStateEnabled"
    },
    {
        prefix: "height",
        detail: "Specifies the UI component's height.",
        document: {
            type: "Number | String | Function",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTextArea/Configuration/#height"
    },
    {
        prefix: "hint",
        detail: "Specifies text for a hint that appears when a user pauses on the UI component.",
        document: {
            type: "String",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTextArea/Configuration/#hint"
    },
    {
        prefix: "hoverStateEnabled",
        detail: "Specifies whether the UI component changes its state when a user pauses on it.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTextArea/Configuration/#hoverStateEnabled"
    },
    {
        prefix: "inputAttr",
        detail: "Specifies the attributes to be passed on to the underlying HTML element.",
        document: {
            type: "any",
            defaultValue: "{}",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTextArea/Configuration/#inputAttr"
    },
    {
        prefix: "isValid",
        detail: "Specifies or indicates whether the editor's value is valid.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "When you use async rules, isValid is true if the status is 'pending' or 'valid'",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTextArea/Configuration/#isValid"
    },
    {
        prefix: "label",
        detail: "Specifies a text string used to annotate the editor's value.",
        document: {
            type: "String",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTextArea/Configuration/#label"
    },
    {
        prefix: "labelMode",
        detail: "Specifies the label's display mode.",
        document: {
            type: "String",
            defaultValue: "'static', 'floating' (Material)",
            description: "",
            note: "If autofill is enabled in the browser, we do not recommend that you use 'floating' mode. The autofill values will overlap the label when it is displayed as a placeholder. Use 'static' mode instead",
            acceptValues: ['static', 'floating', 'hidden'],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTextArea/Configuration/#labelMode"
    },
    {
        prefix: "maxHeight",
        detail: "Specifies the maximum height of the UI component.",
        document: {
            type: "Number | String",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTextArea/Configuration/#maxHeight"
    },
    {
        prefix: "maxLength",
        detail: "Specifies the maximum number of characters you can enter into the textbox.",
        document: {
            type: "String | Number",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTextArea/Configuration/#maxLength"
    },
    {
        prefix: "minHeight",
        detail: "Specifies the minimum height of the UI component.",
        document: {
            type: "Number | String",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTextArea/Configuration/#minHeight"
    },
    {
        prefix: "name",
        detail: "The value to be assigned to the name attribute of the underlying HTML element.",
        document: {
            type: "String",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTextArea/Configuration/#name"
    },
    {
        prefix: "onChange",
        detail: "A function that is executed when the UI component loses focus after the text field's content was changed using the keyboard.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTextArea/Configuration/#onChange"
    },
    {
        prefix: "onContentReady",
        detail: "A function that is executed when the UI component's content is ready and each time the content is changed.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTextArea/Configuration/#onContentReady"
    },
    {
        prefix: "onCopy",
        detail: "A function that is executed when the UI component's input has been copied.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTextArea/Configuration/#onCopy"
    },
    {
        prefix: "onCut",
        detail: "A function that is executed when the UI component's input has been cut.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTextArea/Configuration/#onCut"
    },
    {
        prefix: "onDisposing",
        detail: "A function that is executed before the UI component is disposed of.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTextArea/Configuration/#onDisposing"
    },
    {
        prefix: "onEnterKey",
        detail: "A function that is executed when the Enter key has been pressed while the UI component is focused.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "This function is executed after the onKeyUp and onKeyDown functions",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTextArea/Configuration/#onEnterKey"
    },
    {
        prefix: "onFocusIn",
        detail: "A function that is executed when the UI component gets focus.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTextArea/Configuration/#onFocusIn"
    },
    {
        prefix: "disabledonFocusOut",
        detail: "A function that is executed when the UI component loses focus.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTextArea/Configuration/#onFocusOut"
    },
    {
        prefix: "onInitialized",
        detail: "A function used in JavaScript frameworks to save the UI component instance.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTextArea/Configuration/#onInitialized"
    },
    {
        prefix: "onInput",
        detail: "A function that is executed each time the UI component's input is changed while the UI component is focused.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTextArea/Configuration/#onInput"
    },
    {
        prefix: "onKeyDown",
        detail: "A function that is executed when a user is pressing a key on the keyboard.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTextArea/Configuration/#onKeyDown"
    },
    {
        prefix: "onKeyUp",
        detail: "A function that is executed when a user releases a key on the keyboard.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTextArea/Configuration/#onKeyUp"
    },
    {
        prefix: "onOptionChanged",
        detail: "A function that is executed after a UI component property is changed.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTextArea/Configuration/#onOptionChanged"
    },
    {
        prefix: "onPaste",
        detail: "A function that is executed when the UI component's input has been pasted.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTextArea/Configuration/#onPaste"
    },
    {
        prefix: "onValueChanged",
        detail: "A function that is executed after the UI component's value is changed.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTextArea/Configuration/#onValueChanged"
    },
    {
        prefix: "placeholder",
        detail: "Specifies a text string displayed when the editor's value is empty.",
        document: {
            type: "String",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTextArea/Configuration/#placeholder"
    },
    {
        prefix: "readOnly",
        detail: "Specifies whether the editor is read-only.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTextArea/Configuration/#readOnly"
    },
    {
        prefix: "rtlEnabled",
        detail: "Switches the UI component to a right-to-left representation.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTextArea/Configuration/#rtlEnabled"
    },
    {
        prefix: "spellcheck",
        detail: "Specifies whether or not the UI component checks the inner text for spelling mistakes.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTextArea/Configuration/#spellcheck"
    },
    {
        prefix: "stylingMode",
        detail: "Specifies how the UI component's text field is styled.",
        document: {
            type: "String",
            defaultValue: "'outlined', 'filled' (Material)",
            description: "",
            note: "",
            acceptValues: ['outlined', 'underlined', 'filled'],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTextArea/Configuration/#stylingMode"
    },
    {
        prefix: "tabIndex",
        detail: "Specifies the number of the element when the Tab key is used for navigating.",
        document: {
            type: "Number",
            defaultValue: "0",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTextArea/Configuration/#tabIndex"
    },
    {
        prefix: "text",
        detail: "The read-only property that holds the text displayed by the UI component input element.",
        document: {
            type: "String",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTextArea/Configuration/#text"
    },
    {
        prefix: "validationError",
        detail: "Information on the broken validation rule. Contains the first item from the validationErrors array.",
        document: {
            type: "any",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTextArea/Configuration/#validationError"
    },
    {
        prefix: "validationErrors",
        detail: "An array of the validation rules that failed.",
        document: {
            type: "Array<any>",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTextArea/Configuration/#validationErrors"
    },
    {
        prefix: "validationMessageMode",
        detail: "Specifies how the message about the validation rules that are not satisfied by this editor's value is displayed.",
        document: {
            type: "String",
            defaultValue: "'auto'",
            description: "",
            note: "",
            acceptValues: ['always', 'auto'],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTextArea/Configuration/#validationMessageMode"
    },
    {
        prefix: "validationStatus",
        detail: "Indicates or specifies the current validation status.",
        document: {
            type: "String",
            defaultValue: "'valid'",
            description: "",
            note: "",
            acceptValues: ['valid', 'invalid', 'pending'],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTextArea/Configuration/#validationStatus"
    },
    {
        prefix: "value",
        detail: "Specifies a value the UI component displays.",
        document: {
            type: "String",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTextArea/Configuration/#value"
    },
    {
        prefix: "valueChangeEvent",
        detail: "Specifies the DOM events after which the UI component's value should be updated.",
        document: {
            type: "String",
            defaultValue: "'change'",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTextArea/Configuration/#valueChangeEvent"
    },
    {
        prefix: "visible",
        detail: "Specifies whether the UI component is visible.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTextArea/Configuration/#visible"
    },
    {
        prefix: "width",
        detail: "Specifies the UI component's width.",
        document: {
            type: "Number | String | Function",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTextArea/Configuration/#width"
    },

]