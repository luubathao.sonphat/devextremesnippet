
module.exports = [
    {
        prefix: "dataSource",
        detail: "Binds the UI component to data.",
        document: {
            type: "String | Array<String | dxToolbarItem | any> | Store | DataSource | DataSource Configuration",
            defaultValue: "null",
            description: "",
            note: "Review the following notes about data binding",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxToolbar/Configuration/#dataSource"
    },
    {
        prefix: "disabled",
        detail: "Specifies whether the UI component responds to user interaction.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxToolbar/Configuration/#disabled"
    },
    {
        prefix: "elementAttr",
        detail: "Specifies the global attributes to be attached to the UI component's container element.",
        document: {
            type: "Object",
            defaultValue: "{}",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxToolbar/Configuration/#elementAttr"
    },
    {
        prefix: "hint",
        detail: "Specifies text for a hint that appears when a user pauses on the UI component.",
        document: {
            type: "String",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxToolbar/Configuration/#hint"
    },
    {
        prefix: "hoverStateEnabled",
        detail: "Specifies whether the UI component changes its state when a user pauses on it.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxToolbar/Configuration/#hoverStateEnabled"
    },
    {
        prefix: "itemComponent",
        detail: "An alias for the itemTemplate property specified in React. Accepts a custom component. Refer to Using a Custom Component for more information.",
        document: {
            type: "'",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxToolbar/Configuration/#itemComponent"
    },
    {
        prefix: "itemHoldTimeout",
        detail: "The time period in milliseconds before the onItemHold event is raised.",
        document: {
            type: "Number",
            defaultValue: "750",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxToolbar/Configuration/#itemHoldTimeout"
    },
    {
        prefix: "itemRender",
        detail: "An alias for the itemTemplate property specified in React. Accepts a rendering function. Refer to Using a Rendering Function for more information.",
        document: {
            type: "''",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxToolbar/Configuration/#itemRender"
    },
    {
        prefix: "items",
        detail: "An array of items displayed by the UI component.",
        document: {
            type: "Array<String | dxToolbarItem | any>",
            defaultValue: "''",
            description: "",
            note: "Do not use the items property if you use dataSource, and vice versa",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxToolbar/Configuration/items/"
    },
    {
        prefix: "itemTemplate",
        detail: "Specifies a custom template for items.",
        document: {
            type: "template",
            defaultValue: "'item'",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxToolbar/Configuration/#itemTemplate"
    },
    {
        prefix: "menuItemComponent",
        detail: "An alias for the menuItemTemplate property specified in React. Accepts a custom component. Refer to Using a Custom Component for more information.",
        document: {
            type: "''",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxToolbar/Configuration/#menuItemComponent"
    },
    {
        prefix: "menuItemRender",
        detail: "An alias for the menuItemTemplate property specified in React. Accepts a rendering function. Refer to Using a Rendering Function for more information.",
        document: {
            type: "''",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxToolbar/Configuration/#menuItemRender"
    },
    {
        prefix: "menuItemTemplate",
        detail: "Specifies a custom template for menu items.",
        document: {
            type: "template",
            defaultValue: "'menuItem'",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxToolbar/Configuration/#menuItemTemplate"
    },
    {
        prefix: "noDataText",
        detail: "Specifies the text or HTML markup displayed by the UI component if the item collection is empty.",
        document: {
            type: "String",
            defaultValue: "'No data to display'",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxToolbar/Configuration/#noDataText"
    },
    {
        prefix: "onContentReady",
        detail: "A function that is executed when the UI component's content is ready and each time the content is changed.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxToolbar/Configuration/#onContentReady"
    },
    {
        prefix: "onDisposing",
        detail: "A function that is executed before the UI component is disposed of.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxToolbar/Configuration/#onDisposing"
    },
    {
        prefix: "onInitialized",
        detail: "A function used in JavaScript frameworks to save the UI component instance.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxToolbar/Configuration/#onInitialized"
    },
    {
        prefix: "onItemClick",
        detail: "A function that is executed when a collection item is clicked or tapped.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxToolbar/Configuration/#onItemClick"
    },
    {
        prefix: "onItemContextMenu",
        detail: "A function that is executed when a collection item is right-clicked or pressed.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxToolbar/Configuration/#onItemContextMenu"
    },
    {
        prefix: "onItemHold",
        detail: "A function that is executed when a collection item has been held for a specified period.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxToolbar/Configuration/#onItemHold"
    },
    {
        prefix: "onItemRendered",
        detail: "A function that is executed after a collection item is rendered.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxToolbar/Configuration/#onItemRendered"
    },
    {
        prefix: "onOptionChanged",
        detail: "A function that is executed after a UI component property is changed.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxToolbar/Configuration/#onOptionChanged"
    },
    {
        prefix: "rtlEnabled",
        detail: "Switches the UI component to a right-to-left representation.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxToolbar/Configuration/#rtlEnabled"
    },
    {
        prefix: "visible",
        detail: "Specifies whether the UI component is visible.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxToolbar/Configuration/#visible"
    },
    {
        prefix: "width",
        detail: "Specifies the UI component's width.",
        document: {
            type: "Number | String | Function",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxToolbar/Configuration/#width"
    },

]