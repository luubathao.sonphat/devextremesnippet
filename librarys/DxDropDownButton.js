
module.exports = [
    {
        prefix: "accessKey",
        detail: "Specifies the shortcut key that sets focus on the UI component.",
        document: {
            type: "String",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDropDownButton/Configuration/#accessKey"
    },
    {
        prefix: "activeStateEnabled",
        detail: "Specifies whether or not the UI component changes its state when interacting with a user.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDropDownButton/Configuration/#activeStateEnabled"
    },
    {
        prefix: "dataSource",
        detail: "Provides data for the drop-down menu.",
        document: {
            type: "String | Array<dxDropDownButtonItem | any> | Store | DataSource | DataSource Configuration",
            defaultValue: "null",
            description: "",
            note: "Review the following notes about data binding",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDropDownButton/Configuration/#dataSource"
    },
    {
        prefix: "deferRendering",
        detail: "Specifies whether to wait until the drop-down menu is opened the first time to render its content.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDropDownButton/Configuration/#deferRendering"
    },
    {
        prefix: "disabled",
        detail: "Specifies whether the UI component responds to user interaction.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDropDownButton/Configuration/#disabled"
    },
    {
        prefix: "displayExpr",
        detail: "Specifies the data field whose values should be displayed in the drop-down menu.",
        document: {
            type: "String | Function",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDropDownButton/Configuration/#displayExpr"
    },
    {
        prefix: "dropDownContentComponent",
        detail: "An alias for the dropDownContentTemplate property specified in React. Accepts a custom component. Refer to Using a Custom Component for more information.",
        document: {
            type: "",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDropDownButton/Configuration/#dropDownContentComponent"
    },
    {
        prefix: "dropDownContentRender",
        detail: "An alias for the dropDownContentTemplate property specified in React. Accepts a rendering function. Refer to Using a Rendering Function for more information.",
        document: {
            type: "",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDropDownButton/Configuration/#dropDownContentRender"
    },
    {
        prefix: "dropDownContentTemplate",
        detail: "Specifies custom content for the drop-down field.",
        document: {
            type: "template",
            defaultValue: "'content'",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDropDownButton/Configuration/#dropDownContentTemplate"
    },
    {
        prefix: "dropDownOptions",
        detail: "Configures the drop-down field.",
        document: {
            type: "Popup Configuration",
            defaultValue: "{}",
            description: "",
            note: "In Angular and Vue, the nested component that configures the dropDownOptions property does not support event bindings and two-way property bindings.",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDropDownButton/Configuration/#dropDownOptions"
    },
    {
        prefix: "elementAttr",
        detail: "Specifies the global attributes to be attached to the UI component's container element.",
        document: {
            type: "Object",
            defaultValue: "{}",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDropDownButton/Configuration/#elementAttr"
    },
    {
        prefix: "focusStateEnabled",
        detail: "Specifies whether users can use keyboard to focus the UI component.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDropDownButton/Configuration/#focusStateEnabled"
    },
    {
        prefix: "height",
        detail: "Specifies the UI component's height.",
        document: {
            type: "Number | String | Function",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDropDownButton/Configuration/#height"
    },
    {
        prefix: "hint",
        detail: "Specifies text for a hint that appears when a user pauses on the UI component.",
        document: {
            type: "String",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDropDownButton/Configuration/#hint"
    },
    {
        prefix: "hoverStateEnabled",
        detail: "Specifies whether the UI component changes its state when a user hovers the mouse pointer over it.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDropDownButton/Configuration/#hoverStateEnabled"
    },
    {
        prefix: "icon",
        detail: "Specifies the button's icon.",
        document: {
            type: "String",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDropDownButton/Configuration/#icon"
    },
    {
        prefix: "itemComponent",
        detail: "An alias for the itemTemplate property specified in React. Accepts a custom component. Refer to Using a Custom Component for more information.",
        document: {
            type: "",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDropDownButton/Configuration/#itemComponent"
    },
    {
        prefix: "itemRender",
        detail: "An alias for the itemTemplate property specified in React. Accepts a rendering function. Refer to Using a Rendering Function for more information.",
        document: {
            type: "",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDropDownButton/Configuration/#itemRender"
    },
    {
        prefix: "items",
        detail: "Provides drop-down menu items.",
        document: {
            type: "Array<dxDropDownButtonItem | any>",
            defaultValue: "null",
            description: "",
            note: "Do not use the items property if you use dataSource, and vice versa.",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDropDownButton/Configuration/items/"
    },
    {
        prefix: "itemTemplate",
        detail: "Specifies a custom template for drop-down menu items.",
        document: {
            type: "template",
            defaultValue: "'item'",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDropDownButton/Configuration/#itemTemplate"
    },
    {
        prefix: "keyExpr",
        detail: "Specifies which data field provides keys used to distinguish between the selected drop-down menu items.",
        document: {
            type: "String",
            defaultValue: "'this'",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDropDownButton/Configuration/#keyExpr"
    },
    {
        prefix: "noDataText",
        detail: "Specifies the text or HTML markup displayed in the drop-down menu when it does not contain any items.",
        document: {
            defaultValue: "'No data to display'",
            type: "String",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDropDownButton/Configuration/#noDataText"
    },
    {
        prefix: "onButtonClick",
        detail: "A function that is executed when the button is clicked or tapped. If splitButton is true, this function is executed for the action button only.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDropDownButton/Configuration/#onButtonClick"
    },
    {
        prefix: "onContentReady",
        detail: "A function that is executed when the UI component's content is ready and each time the content is changed.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDropDownButton/Configuration/#onContentReady"
    },
    {
        prefix: "onDisposing",
        detail: "A function that is executed before the UI component is disposed of.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDropDownButton/Configuration/#onDisposing"
    },
    {
        prefix: "onInitialized",
        detail: "A function used in JavaScript frameworks to save the UI component instance.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDropDownButton/Configuration/#onInitialized"
    },
    {
        prefix: "onItemClick",
        detail: "A function that is executed when a drop-down menu item is clicked.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDropDownButton/Configuration/#onItemClick"
    },
    {
        prefix: "onOptionChanged",
        detail: "A function that is executed after a UI component property is changed.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDropDownButton/Configuration/#onOptionChanged"
    },
    {
        prefix: "onSelectionChanged",
        detail: "A function that is executed when an item is selected or selection is canceled. In effect when useSelectMode is true.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDropDownButton/Configuration/#onSelectionChanged"
    },
    {
        prefix: "opened",
        detail: "Specifies whether the drop-down menu is opened.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDropDownButton/Configuration/#opened"
    },
    {
        prefix: "rtlEnabled",
        detail: "Switches the UI component to a right-to-left representation.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDropDownButton/Configuration/#rtlEnabled"
    },
    {
        prefix: "selectedItem",
        detail: "Contains the selected item's data. Available when useSelectMode is true.",
        document: {
            type: "String | Number | anyRead-only",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDropDownButton/Configuration/#selectedItem"
    },
    {
        prefix: "selectedItemKey",
        detail: "Contains the selected item's key and allows you to specify the initially selected item. Applies when useSelectMode is true.",
        document: {
            type: "String | Number",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDropDownButton/Configuration/#selectedItemKey"
    },
    {
        prefix: "showArrowIcon",
        detail: "Specifies whether the arrow icon should be displayed.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDropDownButton/Configuration/#showArrowIcon"
    },
    {
        prefix: "splitButton",
        detail: "Specifies whether to split the button in two: one executes an action, the other opens and closes the drop-down menu.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDropDownButton/Configuration/#splitButton"
    },
    {
        prefix: "stylingMode",
        detail: "Specifies how the button is styled.",
        document: {
            type: "String",
            defaultValue: "'outlined'",
            description: "",
            note: "",
            acceptValues: ['text', 'outlined', 'contained'],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDropDownButton/Configuration/#stylingMode"
    },
    {
        prefix: "tabIndex",
        detail: "Specifies the number of the element when the Tab key is used for navigating.",
        document: {
            type: "Number",
            defaultValue: "0",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDropDownButton/Configuration/#tabIndex"
    },
    {
        prefix: "text",
        detail: "Specifies the button's text. Applies only if useSelectMode is false.",
        document: {
            type: "String",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDropDownButton/Configuration/#text"
    },
    {
        prefix: "useItemTextAsTitle",
        detail: "Specifies whether the DropDownButton uses item's text a title attribute.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDropDownButton/Configuration/#useItemTextAsTitle"
    },
    {
        prefix: "useSelectMode",
        detail: "Specifies whether the UI component stores the selected drop-down menu item.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDropDownButton/Configuration/#useSelectMode"
    },
    {
        prefix: "visible",
        detail: "Specifies whether the UI component is visible.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDropDownButton/Configuration/#visible"
    },
    {
        prefix: "width",
        detail: "Specifies the UI component's width.",
        document: {
            type: "Number | String | Function",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDropDownButton/Configuration/#width"
    },
    {
        prefix: "wrapItemText",
        detail: "Specifies whether text that exceeds the drop-down list width should be wrapped.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDropDownButton/Configuration/#wrapItemText"
    },
]