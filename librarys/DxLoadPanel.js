
module.exports = [
    {
        prefix: "animation",
        detail: "Configures UI component visibility animations. This object contains two fields: show and hide.",
        document: {
            type: "Object",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLoadPanel/Configuration/animation/"
    },
    {
        prefix: "closeOnOutsideClick",
        detail: "Specifies whether to close the UI component if a user clicks outside it.",
        document: {
            type: "Boolean | Function",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLoadPanel/Configuration/#closeOnOutsideClick"
    },
    {
        prefix: "container",
        detail: "Specifies the UI component's container.",
        document: {
            type: "String | HTMLElement | jQuery",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLoadPanel/Configuration/#container"
    },
    {
        prefix: "contentComponent",
        detail: "An alias for the contentTemplate property specified in React. Accepts a custom component. Refer to Using a Custom Component for more information.",
        document: {
            type: "''",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLoadPanel/Configuration/#contentComponent"
    },
    {
        prefix: "contentRender",
        detail: "An alias for the contentTemplate property specified in React. Accepts a rendering function. Refer to Using a Rendering Function for more information.",
        document: {
            type: "''",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLoadPanel/Configuration/#contentRender"
    },
    {
        prefix: "copyRootClassesToWrapper",
        detail: "Copies your custom CSS classes from the root element to the wrapper element.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLoadPanel/Configuration/#copyRootClassesToWrapper"
    },
    {
        prefix: "deferRendering",
        detail: "Specifies whether to render the UI component's content when it is displayed. If false, the content is rendered immediately.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLoadPanel/Configuration/#deferRendering"
    },
    {
        prefix: "delay",
        detail: "The delay in milliseconds after which the load panel is displayed.",
        document: {
            type: "Number",
            defaultValue: "0",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLoadPanel/Configuration/#delay"
    },
    {
        prefix: "focusStateEnabled",
        detail: "Specifies whether or not the UI component can be focused.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLoadPanel/Configuration/#focusStateEnabled"
    },
    {
        prefix: "height",
        detail: "Specifies the UI component's height.",
        document: {
            type: "Number | String | Function",
            defaultValue: "90, 60 (Material)",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLoadPanel/Configuration/#height"
    },
    {
        prefix: "hideOnParentScroll",
        detail: "Specifies whether to hide the LoadPanel when users scroll one of its parent elements.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLoadPanel/Configuration/#hideOnParentScroll"
    },
    {
        prefix: "hint",
        detail: "Specifies text for a hint that appears when a user pauses on the UI component.",
        document: {
            type: "String",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLoadPanel/Configuration/#hint"
    },
    {
        prefix: "hoverStateEnabled",
        detail: "Specifies whether the UI component changes its state when a user pauses on it.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLoadPanel/Configuration/#hoverStateEnabled"
    },
    {
        prefix: "indicatorSrc",
        detail: "A URL pointing to an image to be used as a load indicator.",
        document: {
            type: "String",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLoadPanel/Configuration/#indicatorSrc"
    },
    {
        prefix: "maxHeight",
        detail: "Specifies the maximum height the UI component can reach while resizing.",
        document: {
            type: "Number | String | Function",
            defaultValue: "60 (Material)",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLoadPanel/Configuration/#maxHeight"
    },
    {
        prefix: "maxWidth",
        detail: "Specifies the maximum width the UI component can reach while resizing.",
        document: {
            type: "Number | String | Function",
            defaultValue: "60 (Material)",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLoadPanel/Configuration/#maxWidth"
    },
    {
        prefix: "message",
        detail: "Specifies the text displayed in the load panel. Ignored in the Material Design theme.",
        document: {
            type: "String",
            defaultValue: "'Loading ...', '' (Material)",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLoadPanel/Configuration/#message"
    },
    {
        prefix: "minHeight",
        detail: "Specifies the minimum height the UI component can reach while resizing.",
        document: {
            type: "Number | String | Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLoadPanel/Configuration/#minHeight"
    },
    {
        prefix: "minWidth",
        detail: "Specifies the minimum width the UI component can reach while resizing.",
        document: {
            type: "Number | String | Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLoadPanel/Configuration/#minWidth"
    },
    {
        prefix: "onContentReady",
        detail: "A function that is executed when the UI component's content is ready and each time the content is changed.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLoadPanel/Configuration/#onContentReady"
    },
    {
        prefix: "onDisposing",
        detail: "A function that is executed before the UI component is disposed of.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLoadPanel/Configuration/#onDisposing"
    },
    {
        prefix: "onHidden",
        detail: "A function that is executed after the UI component is hidden.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLoadPanel/Configuration/#onHidden"
    },
    {
        prefix: "onHiding",
        detail: "A function that is executed before the UI component is hidden.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLoadPanel/Configuration/#onHiding"
    },
    {
        prefix: "onInitialized",
        detail: "A function used in JavaScript frameworks to save the UI component instance.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLoadPanel/Configuration/#onInitialized"
    },
    {
        prefix: "onOptionChanged",
        detail: "A function that is executed after a UI component property is changed.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLoadPanel/Configuration/#onOptionChanged"
    },
    {
        prefix: "onShowing",
        detail: "A function that is executed before the UI component is displayed.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLoadPanel/Configuration/#onShowing"
    },
    {
        prefix: "onShown",
        detail: "A function that is executed after the UI component is displayed.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLoadPanel/Configuration/#onShown"
    },
    {
        prefix: "position",
        detail: "Positions the UI component.",
        document: {
            type: "String | PositionConfig | Function",
            defaultValue: "{ my: 'center', at: 'center', of: window }",
            description: "",
            note: "",
            acceptValues: ['bottom', 'center', 'left', 'left bottom', 'left top', 'right', 'right bottom', 'right top', 'top'],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLoadPanel/Configuration/#position"
    },
    {
        prefix: "rtlEnabled",
        detail: "Switches the UI component to a right-to-left representation.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLoadPanel/Configuration/#rtlEnabled"
    },
    {
        prefix: "shading",
        detail: "Specifies whether to shade the background when the UI component is active.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLoadPanel/Configuration/#shading"
    },
    {
        prefix: "shadingColor",
        detail: "Specifies the shading color. Applies only if shading is enabled.",
        document: {
            type: "String",
            defaultValue: "'transparent', '' (Android, iOS)",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLoadPanel/Configuration/#shadingColor"
    },
    {
        prefix: "showIndicator",
        detail: "A Boolean value specifying whether or not to show a load indicator.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLoadPanel/Configuration/#showIndicator"
    },
    {
        prefix: "showPane",
        detail: "A Boolean value specifying whether or not to show the pane behind the load indicator.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLoadPanel/Configuration/#showPane"
    },
    {
        prefix: "visible",
        detail: "A Boolean value specifying whether or not the UI component is visible.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLoadPanel/Configuration/#visible"
    },
    {
        prefix: "width",
        detail: "Specifies the UI component's width.",
        document: {
            type: "Number | String | Function",
            defaultValue: "222, 60 (Material)",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLoadPanel/Configuration/#width"
    },
    {
        prefix: "wrapperAttr",
        detail: "Specifies the global attributes for the UI component's wrapper element.",
        document: {
            type: "any",
            defaultValue: "{}",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLoadPanel/Configuration/#wrapperAttr"
    },

]