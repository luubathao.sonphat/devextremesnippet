
module.exports = [
    {
        prefix: "acceptCustomValue",
        detail: "Specifies whether the UI component allows a user to enter a custom value. Requires the onCustomItemCreating handler implementation.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#acceptCustomValue"
    },
    {
        prefix: "accessKey",
        detail: "Specifies the shortcut key that sets focus on the UI component.",
        document: {
            type: "String",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#accessKey"
    },
    {
        prefix: "activeStateEnabled",
        detail: "Specifies whether or not the UI component changes its state when interacting with a user.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#activeStateEnabled"
    },
    {
        prefix: "applyValueMode",
        detail: "Specifies how the UI component applies values.",
        document: {
            type: "String",
            defaultValue: "'instantly'",
            description: "",
            note: "",
            acceptValues: ['instantly', 'useButtons'],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#applyValueMode"
    },
    {
        prefix: "buttons",
        detail: "Allows you to add custom buttons to the input text field.",
        document: {
            type: "Array<String | Object>",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: ['clear', 'dropDown'],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/buttons/"
    },
    {
        prefix: "dataSource",
        detail: "Binds the UI component to data.",
        document: {
            type: "Store | DataSource | DataSource Configuration | String | Array<CollectionWidgetItem | any>",
            defaultValue: "null",
            description: "",
            note: "Review the following notes about data binding",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#dataSource"
    },
    {
        prefix: "deferRendering",
        detail: "Specifies whether to render the drop-down field's content when it is displayed. If false, the content is rendered immediately.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#deferRendering"
    },
    {
        prefix: "disabled",
        detail: "Specifies whether the UI component responds to user interaction.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#disabled"
    },
    {
        prefix: "displayExpr",
        detail: "Specifies the data field whose values should be displayed.",
        document: {
            type: "String | Function",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#displayExpr"
    },
    {
        prefix: "dropDownButtonComponent",
        detail: "An alias for the dropDownButtonTemplate property specified in React. Accepts a custom component. Refer to Using a Custom Component for more information.",
        document: {
            type: "",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#dropDownButtonComponent"
    },
    {
        prefix: "dropDownButtonRender",
        detail: "An alias for the dropDownButtonTemplate property specified in React. Accepts a rendering function. Refer to Using a Rendering Function for more information.",
        document: {
            type: "",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#dropDownButtonRender"
    },
    {
        prefix: "dropDownButtonTemplate",
        detail: "Specifies a custom template for the drop-down button.",
        document: {
            type: "template",
            defaultValue: "'dropDownButton'",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#dropDownButtonTemplate"
    },
    {
        prefix: "dropDownOptions",
        detail: "Configures the drop-down field which holds the content.",
        document: {
            type: "Popup Configuration",
            defaultValue: "{}",
            description: "",
            note: "In Angular and Vue, the nested component that configures the dropDownOptions property does not support event bindings and two-way property bindings.",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#dropDownOptions"
    },
    {
        prefix: "elementAttr",
        detail: "Specifies the global attributes to be attached to the UI component's container element.",
        document: {
            type: "Object",
            defaultValue: "{}",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#elementAttr"
    },
    {
        prefix: "fieldComponent",
        detail: "An alias for the fieldTemplate property specified in React. Accepts a custom component. Refer to Using a Custom Component for more information.",
        document: {
            type: "",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#fieldComponent"
    },
    {
        prefix: "fieldRender",
        detail: "An alias for the fieldTemplate property specified in React. Accepts a rendering function. Refer to Using a Rendering Function for more information.",
        document: {
            type: "",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#fieldRender"
    },
    {
        prefix: "fieldTemplate",
        detail: "Specifies a custom template for the text field. Must contain the TextBox UI component.",
        document: {
            type: "template",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#fieldTemplate"
    },
    {
        prefix: "focusStateEnabled",
        detail: "Specifies whether the UI component can be focused using keyboard navigation.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#focusStateEnabled"
    },
    {
        prefix: "groupComponent",
        detail: "An alias for the groupTemplate property specified in React. Accepts a custom component. Refer to Using a Custom Component for more information.",
        document: {
            type: "",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#groupComponent"
    },
    {
        prefix: "grouped",
        detail: "Specifies whether data items should be grouped.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "Only one-level grouping is supported.",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#grouped"
    },
    {
        prefix: "groupRender",
        detail: "An alias for the groupTemplate property specified in React. Accepts a rendering function. Refer to Using a Rendering Function for more information.",
        document: {
            type: "",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#groupRender"
    },
    {
        prefix: "groupTemplate",
        detail: "Specifies a custom template for group captions.",
        document: {
            type: "template",
            defaultValue: "'group'",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#groupTemplate"
    },
    {
        prefix: "height",
        detail: "Specifies the UI component's height.",
        document: {
            type: "Number | String | Function",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#height"
    },
    {
        prefix: "hideSelectedItems",
        detail: "A Boolean value specifying whether or not to hide selected items.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#hideSelectedItems"
    },
    {
        prefix: "hint",
        detail: "Specifies text for a hint that appears when a user pauses on the UI component.",
        document: {
            type: "String",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#hint"
    },
    {
        prefix: "hoverStateEnabled",
        detail: "Specifies whether the UI component changes its state when a user pauses on it.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#hoverStateEnabled"
    },
    {
        prefix: "inputAttr",
        detail: "Specifies the attributes to be passed on to the underlying HTML element.",
        document: {
            type: "any",
            defaultValue: "{}",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#inputAttr"
    },
    {
        prefix: "isValid",
        detail: "Specifies or indicates whether the editor's value is valid.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "When you use async rules, isValid is true if the status is 'pending' or 'valid'.",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#isValid"
    },
    {
        prefix: "itemComponent",
        detail: "An alias for the itemTemplate property specified in React. Accepts a custom component. Refer to Using a Custom Component for more information.",
        document: {
            type: "",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#itemComponent"
    },
    {
        prefix: "itemRender",
        detail: "An alias for the itemTemplate property specified in React. Accepts a rendering function. Refer to Using a Rendering Function for more information.",
        document: {
            type: "",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#itemRender"
    },
    {
        prefix: "items",
        detail: "An array of items displayed by the UI component.",
        document: {
            type: "Array<CollectionWidgetItem | any>",
            defaultValue: "",
            description: "",
            note: "Do not use the items property if you use dataSource, and vice versa.",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/items/"
    },
    {
        prefix: "itemTemplate",
        detail: "Specifies a custom template for items.",
        document: {
            type: "template",
            defaultValue: "'item'",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#itemTemplate"
    },
    {
        prefix: "label",
        detail: "Specifies a text string used to annotate the editor's value.",
        document: {
            type: "String",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#label"
    },
    {
        prefix: "labelMode",
        detail: "Specifies the label's display mode.",
        document: {
            type: "String",
            defaultValue: "'static', 'floating' (Material)",
            description: "",
            note: "If autofill is enabled in the browser, we do not recommend that you use 'floating' mode. The autofill values will overlap the label when it is displayed as a placeholder. Use 'static' mode instead.",
            acceptValues: ['static', 'floating', 'hidden'],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#labelMode"
    },
    {
        prefix: "maxDisplayedTags",
        detail: "Specifies the limit on displayed tags. On exceeding it, the UI component replaces all tags with a single multi-tag that displays the number of selected items.",
        document: {
            type: "Number",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#maxDisplayedTags"
    },
    {
        prefix: "maxFilterQueryLength",
        detail: "Specifies the maximum filter query length in characters.",
        document: {
            type: "Number",
            defaultValue: "1500",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#maxFilterQueryLength"
    },
    {
        prefix: "maxLength",
        detail: "Specifies the maximum number of characters you can enter into the textbox.",
        document: {
            type: "String | Number",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#maxLength"
    },
    {
        prefix: "minSearchLength",
        detail: "The minimum number of characters that must be entered into the text box to begin a search. Applies only if searchEnabled is true.",
        document: {
            type: "Number",
            defaultValue: "0",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#minSearchLength"
    },
    {
        prefix: "multiline",
        detail: "A Boolean value specifying whether or not the UI component is multiline.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#multiline"
    },
    {
        prefix: "name",
        detail: "The value to be assigned to the name attribute of the underlying HTML element.",
        document: {
            type: "String",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#name"
    },
    {
        prefix: "noDataText",
        detail: "Specifies the text or HTML markup displayed by the UI component if the item collection is empty.",
        document: {
            type: "String",
            defaultValue: "'No data to display'",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#noDataText"
    },
    {
        prefix: "onChange",
        detail: "A function that is executed when the UI component loses focus after the text field's content was changed using the keyboard.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#onChange"
    },
    {
        prefix: "onClosed",
        detail: "A function that is executed once the drop-down editor is closed.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#onClosed"
    },
    {
        prefix: "onContentReady",
        detail: "A function that is executed when the UI component's content is ready and each time the content is changed.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#onContentReady"
    },
    {
        prefix: "onCustomItemCreating",
        detail: "A function that is executed when a user adds a custom item. Requires acceptCustomValue to be set to true.",
        document: {
            type: "Function",
            defaultValue: "function(e) { if(!e.customItem) { e.customItem = e.text; } }",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#onCustomItemCreating"
    },
    {
        prefix: "onDisposing",
        detail: "A function that is executed before the UI component is disposed of.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#onDisposing"
    },
    {
        prefix: "onEnterKey",
        detail: "A function that is executed when the Enter key has been pressed while the UI component is focused.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "This function is executed after the onKeyUp and onKeyDown functions.",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#onEnterKey"
    },
    {
        prefix: "onFocusIn",
        detail: "A function that is executed when the UI component gets focus.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#onFocusIn"
    },
    {
        prefix: "onFocusOut",
        detail: "A function that is executed when the UI component loses focus.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#onFocusOut"
    },
    {
        prefix: "onInitialized",
        detail: "A function used in JavaScript frameworks to save the UI component instance.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#onInitialized"
    },
    {
        prefix: "onInput",
        detail: "A function that is executed each time the UI component's input is changed while the UI component is focused.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#onInput"
    },
    {
        prefix: "onItemClick",
        detail: "A function that is executed when a list item is clicked or tapped.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#onItemClick"
    },
    {
        prefix: "onKeyDown",
        detail: "A function that is executed when a user is pressing a key on the keyboard.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#onKeyDown"
    },
    {
        prefix: "onKeyUp",
        detail: "A function that is executed when a user releases a key on the keyboard.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#onKeyUp"
    },
    {
        prefix: "onMultiTagPreparing",
        detail: "A function that is executed before the multi-tag is rendered.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#onMultiTagPreparing"
    },
    {
        prefix: "onOpened",
        detail: "A function that is executed once the drop-down editor is opened.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#onOpened"
    },
    {
        prefix: "onOptionChanged",
        detail: "A function that is executed after a UI component property is changed.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#onOptionChanged"
    },
    {
        prefix: "onSelectAllValueChanged",
        detail: "A function that is executed when the 'Select All' check box value is changed. Applies only if showSelectionControls is true.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#onSelectAllValueChanged"
    },
    {
        prefix: "onSelectionChanged",
        detail: "A function that is executed when a list item is selected or selection is canceled.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "When applyValueMode is 'useButtons', the onSelectionChanged handler is executed only when users click the OK button.",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#onSelectionChanged"
    },
    {
        prefix: "onValueChanged",
        detail: "A function that is executed after the UI component's value is changed.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#onValueChanged"
    },
    {
        prefix: "opened",
        detail: "Specifies whether or not the drop-down editor is displayed.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#opened"
    },
    {
        prefix: "openOnFieldClick",
        detail: "Specifies whether a user can open the drop-down list by clicking a text field.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#openOnFieldClick"
    },
    {
        prefix: "placeholder",
        detail: "The text that is provided as a hint in the select box editor.",
        document: {
            type: "String",
            defaultValue: "'Select'",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#placeholder"
    },
    {
        prefix: "readOnly",
        detail: "Specifies whether the editor is read-only.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#readOnly"
    },
    {
        prefix: "rtlEnabled",
        detail: "Switches the UI component to a right-to-left representation.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#rtlEnabled"
    },
    {
        prefix: "searchEnabled",
        detail: "Specifies whether to allow searching.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "Searching works with source data of plain structure only. Subsequently, data can be transformed to hierarchical structure using the DataSource's group property.",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#searchEnabled"
    },
    {
        prefix: "searchExpr",
        detail: "Specifies the name of a data source item field or an expression whose value is compared to the search criterion.",
        document: {
            type: "getter | Array<getter>",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#searchExpr"
    },
    {
        prefix: "searchMode",
        detail: "Specifies a comparison operation used to search UI component items.",
        document: {
            type: "String",
            defaultValue: "'contains'",
            description: "",
            note: "",
            acceptValues: ['contains', 'startswith'],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#searchMode"
    },
    {
        prefix: "searchTimeout",
        detail: "Specifies the time delay, in milliseconds, after the last character has been typed in, before a search is executed.",
        document: {
            type: "Number",
            defaultValue: "500",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#searchTimeout"
    },
    {
        prefix: "selectAllMode",
        detail: "Specifies the mode in which all items are selected.",
        document: {
            type: "String",
            defaultValue: "'page'",
            description: "",
            note: "",
            acceptValues: ['allPages', 'page'],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#selectAllMode"
    },
    {
        prefix: "selectAllText",
        detail: "Specifies the text displayed at the 'Select All' check box.",
        document: {
            type: "String",
            defaultValue: "'Select All'",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#selectAllText"
    },
    {
        prefix: "selectedItems",
        detail: "Gets the currently selected items.",
        document: {
            type: "Array<String | Number | any>",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#selectedItems"
    },
    {
        prefix: "showClearButton",
        detail: "Specifies whether to display the Clear button in the UI component.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#showClearButton"
    },
    {
        prefix: "showDataBeforeSearch",
        detail: "Specifies whether or not the UI component displays unfiltered values until a user types a number of characters exceeding the minSearchLength property value.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#showDataBeforeSearch"
    },
    {
        prefix: "showDropDownButton",
        detail: "Specifies whether the drop-down button is visible.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#showDropDownButton"
    },
    {
        prefix: "showMultiTagOnly",
        detail: "Specifies whether the multi-tag is shown without ordinary tags.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#showMultiTagOnly"
    },
    {
        prefix: "showSelectionControls",
        detail: "Specifies whether or not to display selection controls.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#showSelectionControls"
    },
    {
        prefix: "stylingMode",
        detail: "Specifies how the UI component's text field is styled.",
        document: {
            type: "String",
            defaultValue: "'outlined', 'filled' (Material)",
            description: "",
            note: "",
            acceptValues: ['outlined', 'underlined', 'filled'],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#stylingMode"
    },
    {
        prefix: "tabIndex",
        detail: "Specifies the number of the element when the Tab key is used for navigating.",
        document: {
            type: "Number",
            defaultValue: "0",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#tabIndex"
    },
    {
        prefix: "tagComponent",
        detail: "An alias for the tagTemplate property specified in React. Accepts a custom component. Refer to Using a Custom Component for more information.",
        document: {
            type: "",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#tagComponent"
    },
    {
        prefix: "tagRender",
        detail: "An alias for the tagTemplate property specified in React. Accepts a rendering function. Refer to Using a Rendering Function for more information.",
        document: {
            type: "",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#tagRender"
    },
    {
        prefix: "tagTemplate",
        detail: "Specifies a custom template for tags.",
        document: {
            type: "template",
            defaultValue: "'tag'",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#tagTemplate"
    },
    {
        prefix: "text",
        detail: "The read-only property that holds the text displayed by the UI component input element.",
        document: {
            type: "String",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#text"
    },
    {
        prefix: "useItemTextAsTitle",
        detail: "Specifies whether the TagBox uses item's text a title attribute.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#useItemTextAsTitle"
    },
    {
        prefix: "validationError",
        detail: "Information on the broken validation rule. Contains the first item from the validationErrors array.",
        document: {
            type: "any",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#validationError"
    },
    {
        prefix: "validationErrors",
        detail: "An array of the validation rules that failed.",
        document: {
            type: "Array<any>",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#validationErrors"
    },
    {
        prefix: "validationMessageMode",
        detail: "Specifies how the message about the validation rules that are not satisfied by this editor's value is displayed.",
        document: {
            type: "String",
            defaultValue: "'auto'",
            description: "",
            note: "",
            acceptValues: ['always', 'auto'],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#validationMessageMode"
    },
    {
        prefix: "validationStatus",
        detail: "Indicates or specifies the current validation status.",
        document: {
            type: "String",
            defaultValue: "'valid'",
            description: "",
            note: "",
            acceptValues: ['valid', 'invalid', 'pending'],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#validationStatus"
    },
    {
        prefix: "value",
        detail: "Specifies the selected items.",
        document: {
            type: "Array<String | Number | any>",
            defaultValue: "[]",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#value"
    },
    {
        prefix: "valueExpr",
        detail: "Specifies which data field provides unique values to the UI component's value.",
        document: {
            type: "String | Function",
            defaultValue: "'this'",
            description: "",
            note: "You cannot specify valueExpr as a function when the UI component is bound to a remote data source. This is because valueExpr is used in a filter the UI component sends to the server when querying data. Functions with custom logic cannot be serialized for this filter.",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#valueExpr"
    },
    {
        prefix: "visible",
        detail: "Specifies whether the UI component is visible.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#visible"
    },
    {
        prefix: "width",
        detail: "Specifies the UI component's width.",
        document: {
            type: "Number | String | Function",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#width"
    },
    {
        prefix: "wrapItemText",
        detail: "Specifies whether text that exceeds the drop-down list width should be wrapped.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTagBox/Configuration/#wrapItemText"
    },

]