
module.exports = [
    {
        prefix: "accessKey",
        detail: "Specifies the shortcut key that sets focus on the UI component.",
        document: {
            type: "String",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTabs/Configuration/#accessKey"
    },
    {
        prefix: "dataSource",
        detail: "VM2070:1 Binds the UI component to data.",
        document: {
            type: "String | Array < String | dxTabsItem | any > | Store | DataSource | DataSource Configuration",
            defaultValue: "null",
            description: "",
            note: "Review the following notes about data binding",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTabs/Configuration/#dataSource"
    },
    {
        prefix: "disabled",
        detail: "VM2070:1 Specifies whether the UI component responds to user interaction.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTabs/Configuration/#disabled"
    },
    {
        prefix: "elementAttr",
        detail: "VM2070:1 Specifies the global attributes to be attached to the UI component's container element.",
        document: {
            type: "Object",
            defaultValue: "{}",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTabs/Configuration/#elementAttr"
    },
    {
        prefix: "focusStateEnabled",
        detail: "VM2070:1 Specifies whether the UI component can be focused using keyboard navigation.",
        document: {
            type: "Boolean",
            defaultValue: "true (desktop)",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTabs/Configuration/#focusStateEnabled"
    },
    {
        prefix: "height",
        detail: "VM2070:1 Specifies the UI component's height.",
        document: {
            type: "Number | String | Function",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTabs/Configuration/#height"
    },
    {
        prefix: "hint",
        detail: "VM2070:1 Specifies text for a hint that appears when a user pauses on the UI component.",
        document: {
            type: "String",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTabs/Configuration/#hint"
    },
    {
        prefix: "hoverStateEnabled",
        detail: "VM2070:1 Specifies whether the UI component changes its state when a user pauses on it.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTabs/Configuration/#hoverStateEnabled"
    },
    {
        prefix: "itemComponent",
        detail: "VM2070:1 An alias for the itemTemplate property specified in React. Accepts a custom component. Refer to Using a Custom Component for more information.",
        document: {
            type: "''",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTabs/Configuration/#itemComponent"
    },
    {
        prefix: "itemHoldTimeout",
        detail: "VM2070:1 The time period in milliseconds before the onItemHold event is raised.",
        document: {
            type: "Number",
            defaultValue: "750",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTabs/Configuration/#itemHoldTimeout"
    },
    {
        prefix: "itemRender",
        detail: "VM2070:1 An alias for the itemTemplate property specified in React. Accepts a rendering function. Refer to Using a Rendering Function for more information.",
        document: {
            type: "''",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTabs/Configuration/#itemRender"
    },
    {
        prefix: "items",
        detail: "VM2070:1 An array of items displayed by the UI component.",
        document: {
            type: "Array < String | dxTabsItem | any >",
            defaultValue: "''",
            description: "",
            note: "Do not use the items property if you use dataSource, and vice versa",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTabs/Configuration/items/"
    },
    {
        prefix: "itemTemplate",
        detail: "VM2070:1 Specifies a custom template for items.",
        document: {
            type: "template",
            defaultValue: "'item'",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTabs/Configuration/#itemTemplate"
    },
    {
        prefix: "keyExpr",
        detail: "VM2070:1 Specifies the key property that provides key values to access data items. Each key value must be unique.",
        document: {
            type: "String | Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTabs/Configuration/#keyExpr"
    },
    {
        prefix: "noDataText",
        detail: "VM2070:1 Specifies the text or HTML markup displayed by the UI component if the item collection is empty.",
        document: {
            type: "String",
            defaultValue: "'No data to display'",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTabs/Configuration/#noDataText"
    },
    {
        prefix: "onContentReady",
        detail: "VM2070:1 A function that is executed when the UI component's content is ready and each time the content is changed.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTabs/Configuration/#onContentReady"
    },
    {
        prefix: "onDisposing",
        detail: "VM2070:1 A function that is executed before the UI component is disposed of.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTabs/Configuration/#onDisposing"
    },
    {
        prefix: "onInitialized",
        detail: "VM2070:1 A function used in JavaScript frameworks to save the UI component instance.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTabs/Configuration/#onInitialized"
    },
    {
        prefix: "onItemClick",
        detail: "VM2070:1 A function that is executed when a collection item is clicked or tapped.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTabs/Configuration/#onItemClick"
    },
    {
        prefix: "onItemContextMenu",
        detail: "VM2070:1 A function that is executed when a collection item is right-clicked or pressed.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTabs/Configuration/#onItemContextMenu"
    },
    {
        prefix: "onItemHold",
        detail: "VM2070:1 A function that is executed when a collection item has been held for a specified period.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTabs/Configuration/#onItemHold"
    },
    {
        prefix: "onItemRendered",
        detail: "VM2070:1 A function that is executed after a collection item is rendered.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTabs/Configuration/#onItemRendered"
    },
    {
        prefix: "onOptionChanged",
        detail: "VM2070:1 A function that is executed after a UI component property is changed.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTabs/Configuration/#onOptionChanged"
    },
    {
        prefix: "onSelectionChanged",
        detail: "VM2070:1 A function that is executed when a collection item is selected or selection is canceled.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTabs/Configuration/#onSelectionChanged"
    },
    {
        prefix: "repaintChangesOnly",
        detail: "VM2070:1 Specifies whether to repaint only those elements whose data changed.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTabs/Configuration/#repaintChangesOnly"
    },
    {
        prefix: "rtlEnabled",
        detail: "VM2070:1 Switches the UI component to a right-to-left representation.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTabs/Configuration/#rtlEnabled"
    },
    {
        prefix: "scrollByContent",
        detail: "VM2070:1 Specifies whether or not an end-user can scroll tabs by swiping.",
        document: {
            type: "Boolean",
            defaultValue: "true, false (desktop)",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTabs/Configuration/#scrollByContent"
    },
    {
        prefix: "scrollingEnabled",
        detail: "VM2070:1 Specifies whether or not an end-user can scroll tabs.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTabs/Configuration/#scrollingEnabled"
    },
    {
        prefix: "selectedIndex",
        detail: "VM2070:1 The index of the currently selected UI component item.",
        document: {
            type: "Number",
            defaultValue: "-1",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTabs/Configuration/#selectedIndex"
    },
    {
        prefix: "selectedItem",
        detail: "VM2070:1 The selected item object.",
        document: {
            type: "any",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTabs/Configuration/#selectedItem"
    },
    {
        prefix: "selectedItemKeys",
        detail: "VM2070:1 Specifies an array of currently selected item keys.",
        document: {
            type: "Array < any >",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTabs/Configuration/#selectedItemKeys"
    },
    {
        prefix: "selectedItems",
        detail: "VM2070:1 An array of currently selected item objects.",
        document: {
            type: "Array < any >",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTabs/Configuration/#selectedItems"
    },
    {
        prefix: "selectionMode",
        detail: "VM2070:1 Specifies whether the UI component enables an end-user to select only a single item or multiple items.",
        document: {
            type: "String",
            defaultValue: "'single'",
            description: "",
            note: "",
            acceptValues: ['multiple', 'single'],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTabs/Configuration/#selectionMode"
    },
    {
        prefix: "showNavButtons",
        detail: "VM2070:1 Specifies whether navigation buttons should be available when tabs exceed the UI component's width.",
        document: {
            type: "Boolean",
            defaultValue: "true, false (mobile devices)",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTabs/Configuration/#showNavButtons"
    },
    {
        prefix: "tabIndex",
        detail: "VM2070:1 Specifies the number of the element when the Tab key is used for navigating.",
        document: {
            type: "Number",
            defaultValue: "0",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTabs/Configuration/#tabIndex"
    },
    {
        prefix: "visible",
        detail: "VM2070:1 Specifies whether the UI component is visible.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTabs/Configuration/#visible"
    },
    {
        prefix: "width",
        detail: "VM2070:1 Specifies the UI component's width.",
        document: {
            type: "Number | String | Function",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTabs/Configuration/#width"
    },

]