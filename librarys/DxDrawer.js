
module.exports = [
    {
        prefix: "activeStateEnabled",
        detail: "Specifies whether the UI component changes its state as a result of user interaction.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxButton/Configuration/#activeStateEnabled"
    },
    {
        prefix: "animationDuration",
        detail: "Specifies the duration of the drawer's opening and closing animation (in milliseconds). Applies only if animationEnabled is true.",
        document: {
            type: "Number",
            defaultValue: "400",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxButton/Configuration/#animationDuration"
    },
    {
        prefix: "animationEnabled",
        detail: "Specifies whether to use an opening and closing animation.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxButton/Configuration/#animationEnabled"
    },
    {
        prefix: "closeOnOutsideClick",
        detail: "Specifies whether to close the drawer if a user clicks or taps the view area.",
        document: {
            type: "Boolean | Function",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxButton/Configuration/#closeOnOutsideClick"
    },
    {
        prefix: "component",
        detail: "An alias for the template property specified in React. Accepts a custom component. Refer to Using a Custom Component for more information.",
        document: {
            type: "''",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxButton/Configuration/#component"
    },
    {
        prefix: "disabled",
        detail: "Specifies whether the UI component responds to user interaction.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxButton/Configuration/#disabled"
    },
    {
        prefix: "elementAttr",
        detail: "Specifies the global attributes to be attached to the UI component's container element.",
        document: {
            type: "Object",
            defaultValue: "{}",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxButton/Configuration/#elementAttr"
    },
    {
        prefix: "height",
        detail: "Specifies the view's height.",
        document: {
            type: "Number | String | Function",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxButton/Configuration/#height"
    },
    {
        prefix: "hint",
        detail: "Specifies text for a hint that appears when a user pauses on the UI component.",
        document: {
            type: "String",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxButton/Configuration/#hint"
    },
    {
        prefix: "hoverStateEnabled",
        detail: "Specifies whether the UI component changes its state when a user pauses on it.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxButton/Configuration/#hoverStateEnabled"
    },
    {
        prefix: "maxSize",
        detail: "Specifies the drawer's width or height (depending on the drawer's position) in the opened state.",
        document: {
            type: "Number",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxButton/Configuration/#maxSize"
    },
    {
        prefix: "minSize",
        detail: "Specifies the drawer's width or height (depending on the drawer's position) in the closed state.",
        document: {
            type: "Number",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxButton/Configuration/#minSize"
    },
    {
        prefix: "onDisposing",
        detail: "A function that is executed before the UI component is disposed of.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxButton/Configuration/#onDisposing"
    },
    {
        prefix: "onInitialized",
        detail: "A function used in JavaScript frameworks to save the UI component instance.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxButton/Configuration/#onInitialized"
    },
    {
        prefix: "onOptionChanged",
        detail: "A function that is executed after a UI component property is changed.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxButton/Configuration/#onOptionChanged"
    },
    {
        prefix: "opened",
        detail: "Specifies whether the drawer is opened.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxButton/Configuration/#opened"
    },
    {
        prefix: "openedStateMode",
        detail: "Specifies how the drawer interacts with the view in the opened state.",
        document: {
            type: "String",
            defaultValue: "'shrink'",
            description: "",
            note: "",
            acceptValues: ['overlap', 'shrink', 'push'],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxButton/Configuration/#openedStateMode"
    },
    {
        prefix: "position",
        detail: "Specifies the drawer's position in relation to the view.",
        document: {
            type: "String",
            defaultValue: "'left'",
            description: "",
            note: "",
            acceptValues: ['left', 'right', 'top', 'bottom', 'before', 'after'],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxButton/Configuration/#position"
    },
    {
        prefix: "render",
        detail: "An alias for the template property specified in React. Accepts a rendering function. Refer to Using a Rendering Function for more information.",
        document: {
            type: "''",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxButton/Configuration/#render"
    },
    {
        prefix: "revealMode",
        detail: "Specifies the drawer's reveal mode.",
        document: {
            type: "String",
            defaultValue: "'slide'",
            description: "",
            note: "",
            acceptValues: ['slide', 'expand'],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxButton/Configuration/#revealMode"
    },
    {
        prefix: "rtlEnabled",
        detail: "Switches the UI component to a right-to-left representation.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxButton/Configuration/#rtlEnabled"
    },
    {
        prefix: "shading",
        detail: "Specifies whether to shade the view when the drawer is opened.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxButton/Configuration/#shading"
    },
    {
        prefix: "template",
        detail: "Specifies the drawer's content.",
        document: {
            type: "template",
            defaultValue: "'panel'",
            description: "",
            note: "The Drawer UI component is not designed to contain another Drawer. Do not use nested Drawers to avoid possible issues in your application",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxButton/Configuration/#template"
    },
    {
        prefix: "visible",
        detail: "Specifies whether the Drawer UI component (including the view) is visible.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxButton/Configuration/#visible"
    },
    {
        prefix: "width",
        detail: "Specifies the view's width.",
        document: {
            type: "Number | String | Function",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxButton/Configuration/#width"
    },

]