
module.exports = [
    {
        prefix: "animation",
        detail: "Configures UI component visibility animations. This object contains two fields: show and hide.",
        document: {
            type: "Object",
            defaultValue: "{ show: { type: 'fade', from: 0, to: 1 }, hide: { type: 'fade', to: 0 } }",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTooltip/Configuration/animation/"
    },
    {
        prefix: "closeOnOutsideClick",
        detail: "VM859:1 Specifies whether to close the UI component if a user clicks outside the popover window or outside the target element.",
        document: {
            type: "Boolean | Function",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTooltip/Configuration/#closeOnOutsideClick"
    },
    {
        prefix: "container",
        detail: "VM859:1 Specifies the container in which to render the UI component.",
        document: {
            type: "String | HTMLElement | jQuery",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTooltip/Configuration/#container"
    },
    {
        prefix: "contentComponent",
        detail: "VM859:1 An alias for the contentTemplate property specified in React. Accepts a custom component. Refer to Using a Custom Component for more information.",
        document: {
            type: "''",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTooltip/Configuration/#contentComponent"
    },
    {
        prefix: "contentRender",
        detail: "VM859:1 An alias for the contentTemplate property specified in React. Accepts a rendering function. Refer to Using a Rendering Function for more information.",
        document: {
            type: "''",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTooltip/Configuration/#contentRender"
    },
    {
        prefix: "contentTemplate",
        detail: "VM859:1 Specifies a custom template for the UI component content.",
        document: {
            type: "template",
            defaultValue: "'content'",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTooltip/Configuration/#contentTemplate"
    },
    {
        prefix: "copyRootClassesToWrapper",
        detail: "VM859:1 Copies your custom CSS classes from the root element to the wrapper element.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTooltip/Configuration/#copyRootClassesToWrapper"
    },
    {
        prefix: "deferRendering",
        detail: "VM859:1 Specifies whether to render the UI component's content when it is displayed. If false, the content is rendered immediately.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTooltip/Configuration/#deferRendering"
    },
    {
        prefix: "disabled",
        detail: "VM859:1 Specifies whether the UI component responds to user interaction.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTooltip/Configuration/#disabled"
    },
    {
        prefix: "height",
        detail: "VM859:1 Specifies the UI component's height.",
        document: {
            type: "Number | String | Function",
            defaultValue: "'auto'",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTooltip/Configuration/#height"
    },
    {
        prefix: "hideEvent",
        detail: "VM859:1 Specifies properties of popover hiding. Ignored if the shading property is set to true.",
        document: {
            type: "Object | String",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTooltip/Configuration/hideEvent/"
    },
    {
        prefix: "hideOnParentScroll",
        detail: "VM859:1 Specifies whether to hide the Tooltip when users scroll one of its parent elements.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTooltip/Configuration/#hideOnParentScroll"
    },
    {
        prefix: "hint",
        detail: "VM859:1 Specifies text for a hint that appears when a user pauses on the UI component.",
        document: {
            type: "String",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTooltip/Configuration/#hint"
    },
    {
        prefix: "hoverStateEnabled",
        detail: "VM859:1 Specifies whether the UI component changes its state when a user pauses on it.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTooltip/Configuration/#hoverStateEnabled"
    },
    {
        prefix: "maxHeight",
        detail: "VM859:1 Specifies the maximum height the UI component can reach while resizing.",
        document: {
            type: "Number | String | Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTooltip/Configuration/#maxHeight"
    },
    {
        prefix: "maxWidth",
        detail: "VM859:1 Specifies the maximum width the UI component can reach while resizing.",
        document: {
            type: "Number | String | Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTooltip/Configuration/#maxWidth"
    },
    {
        prefix: "minHeight",
        detail: "VM859:1 Specifies the minimum height the UI component can reach while resizing.",
        document: {
            type: "Number | String | Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTooltip/Configuration/#minHeight"
    },
    {
        prefix: "minWidth",
        detail: "VM859:1 Specifies the minimum width the UI component can reach while resizing.",
        document: {
            type: "Number | String | Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTooltip/Configuration/#minWidth"
    },
    {
        prefix: "onContentReady",
        detail: "VM859:1 A function that is executed when the UI component's content is ready and each time the content is changed.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTooltip/Configuration/#onContentReady"
    },
    {
        prefix: "onDisposing",
        detail: "VM859:1 A function that is executed before the UI component is disposed of.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTooltip/Configuration/#onDisposing"
    },
    {
        prefix: "onHidden",
        detail: "VM859:1 A function that is executed after the UI component is hidden.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTooltip/Configuration/#onHidden"
    },
    {
        prefix: "onHiding",
        detail: "VM859:1 A function that is executed before the UI component is hidden.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTooltip/Configuration/#onHiding"
    },
    {
        prefix: "onInitialized",
        detail: "VM859:1 A function used in JavaScript frameworks to save the UI component instance.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTooltip/Configuration/#onInitialized"
    },
    {
        prefix: "onOptionChanged",
        detail: "VM859:1 A function that is executed after a UI component property is changed.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTooltip/Configuration/#onOptionChanged"
    },
    {
        prefix: "onShowing",
        detail: "VM859:1 A function that is executed before the UI component is displayed.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTooltip/Configuration/#onShowing"
    },
    {
        prefix: "onShown",
        detail: "VM859:1 A function that is executed after the UI component is displayed.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTooltip/Configuration/#onShown"
    },
    {
        prefix: "position",
        detail: "VM859:1 An object defining UI component positioning properties.",
        document: {
            type: "String | PositionConfig",
            defaultValue: "{ my: 'top center', at: 'bottom center', collision: 'fit flip' }",
            description: "",
            note: "",
            acceptValues: ['bottom', 'left', 'right', 'top'],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTooltip/Configuration/#position"
    },
    {
        prefix: "rtlEnabled",
        detail: "VM859:1 Switches the UI component to a right-to-left representation.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTooltip/Configuration/#rtlEnabled"
    },
    {
        prefix: "shading",
        detail: "VM859:1 Specifies whether to shade the background when the UI component is active.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTooltip/Configuration/#shading"
    },
    {
        prefix: "shadingColor",
        detail: "VM859:1 Specifies the shading color. Applies only if shading is enabled.",
        document: {
            type: "String",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTooltip/Configuration/#shadingColor"
    },
    {
        prefix: "showEvent",
        detail: "VM859:1 Specifies properties for displaying the UI component.",
        document: {
            type: "Object | String",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTooltip/Configuration/showEvent/"
    },
    {
        prefix: "target",
        detail: "VM859:1 Specifies the element against which to position the Tooltip.",
        document: {
            type: "String | HTMLElement | jQuery",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTooltip/Configuration/#target"
    },
    {
        prefix: "titleComponent",
        detail: "VM859:1 An alias for the titleTemplate property specified in React. Accepts a custom component. Refer to Using a Custom Component for more information.",
        document: {
            type: "''",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTooltip/Configuration/#titleComponent"
    },
    {
        prefix: "titleRender",
        detail: "VM859:1 An alias for the titleTemplate property specified in React. Accepts a rendering function. Refer to Using a Rendering Function for more information.",
        document: {
            type: "''",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTooltip/Configuration/#titleRender"
    },
    {
        prefix: "visible",
        detail: "VM859:1 A Boolean value specifying whether or not the UI component is visible.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTooltip/Configuration/#visible"
    },
    {
        prefix: "width",
        detail: "VM859:1 Specifies the UI component's width.",
        document: {
            type: "Number | String | Function",
            defaultValue: "'auto'",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTooltip/Configuration/#width"
    },
    {
        prefix: "wrapperAttr",
        detail: "VM859:1 Specifies the global attributes for the UI component's wrapper element.",
        document: {
            type: "any",
            defaultValue: "{}",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxTooltip/Configuration/#wrapperAttr"
    },

]