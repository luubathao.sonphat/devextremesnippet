
module.exports = [
    {
        prefix: "animation",
        detail: "Configures UI component visibility animations. This object contains two fields: show and hide.",
        document: {
            type: "Object",
            defaultValue: "{ show: { type: 'fade', from: 0, to: 1 }, hide: { type: 'fade', to: 0 } }",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopover/Configuration/animation/"
    },
    {
        prefix: "closeOnOutsideClick",
        detail: "VM923:1 Specifies whether to close the UI component if a user clicks outside the popover window or outside the target element.",
        document: {
            type: "Boolean | Function",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopover/Configuration/#closeOnOutsideClick"
    },
    {
        prefix: "container",
        detail: "VM923:1 Specifies the container in which to render the UI component.",
        document: {
            type: "String | HTMLElement | jQuery",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopover/Configuration/#container"
    },
    {
        prefix: "contentComponent",
        detail: "VM923:1 An alias for the contentTemplate property specified in React. Accepts a custom component. Refer to Using a Custom Component for more information.",
        document: {
            type: "''",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopover/Configuration/#contentComponent"
    },
    {
        prefix: "contentRender",
        detail: "VM923:1 An alias for the contentTemplate property specified in React. Accepts a rendering function. Refer to Using a Rendering Function for more information.",
        document: {
            type: "''",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopover/Configuration/#contentRender"
    },
    {
        prefix: "contentTemplate",
        detail: "VM923:1 Specifies a custom template for the UI component content.",
        document: {
            type: "template",
            defaultValue: "'content'",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopover/Configuration/#contentTemplate"
    },
    {
        prefix: "copyRootClassesToWrapper",
        detail: "VM923:1 Copies your custom CSS classes from the root element to the wrapper element.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopover/Configuration/#copyRootClassesToWrapper"
    },
    {
        prefix: "deferRendering",
        detail: "VM923:1 Specifies whether to render the UI component's content when it is displayed. If false, the content is rendered immediately.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopover/Configuration/#deferRendering"
    },
    {
        prefix: "disabled",
        detail: "VM923:1 Specifies whether the UI component responds to user interaction.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopover/Configuration/#disabled"
    },
    {
        prefix: "height",
        detail: "VM923:1 Specifies the UI component's height.",
        document: {
            type: "Number | String | Function",
            defaultValue: "'auto'",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopover/Configuration/#height"
    },
    {
        prefix: "hideEvent",
        detail: "VM923:1 Specifies properties of popover hiding. Ignored if the shading property is set to true.",
        document: {
            type: "Object | String",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopover/Configuration/hideEvent/"
    },
    {
        prefix: "hideOnParentScroll",
        detail: "VM923:1 Specifies whether to hide the Popover when users scroll one of its parent elements.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopover/Configuration/#hideOnParentScroll"
    },
    {
        prefix: "hint",
        detail: "VM923:1 Specifies text for a hint that appears when a user pauses on the UI component.",
        document: {
            type: "String",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopover/Configuration/#hint"
    },
    {
        prefix: "hoverStateEnabled",
        detail: "VM923:1 Specifies whether the UI component changes its state when a user pauses on it.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopover/Configuration/#hoverStateEnabled"
    },
    {
        prefix: "maxHeight",
        detail: "VM923:1 Specifies the maximum height the UI component can reach while resizing.",
        document: {
            type: "Number | String | Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopover/Configuration/#maxHeight"
    },
    {
        prefix: "maxWidth",
        detail: "VM923:1 Specifies the maximum width the UI component can reach while resizing.",
        document: {
            type: "Number | String | Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopover/Configuration/#maxWidth"
    },
    {
        prefix: "minHeight",
        detail: "VM923:1 Specifies the minimum height the UI component can reach while resizing.",
        document: {
            type: "Number | String | Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopover/Configuration/#minHeight"
    },
    {
        prefix: "minWidth",
        detail: "VM923:1 Specifies the minimum width the UI component can reach while resizing.",
        document: {
            type: "Number | String | Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopover/Configuration/#minWidth"
    },
    {
        prefix: "onContentReady",
        detail: "VM923:1 A function that is executed when the UI component's content is ready and each time the content is changed.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopover/Configuration/#onContentReady"
    },
    {
        prefix: "onDisposing",
        detail: "VM923:1 A function that is executed before the UI component is disposed of.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopover/Configuration/#onDisposing"
    },
    {
        prefix: "onHidden",
        detail: "VM923:1 A function that is executed after the UI component is hidden.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopover/Configuration/#onHidden"
    },
    {
        prefix: "onHiding",
        detail: "VM923:1 A function that is executed before the UI component is hidden.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopover/Configuration/#onHiding"
    },
    {
        prefix: "onInitialized",
        detail: "VM923:1 A function used in JavaScript frameworks to save the UI component instance.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopover/Configuration/#onInitialized"
    },
    {
        prefix: "onOptionChanged",
        detail: "VM923:1 A function that is executed after a UI component property is changed.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopover/Configuration/#onOptionChanged"
    },
    {
        prefix: "onShowing",
        detail: "VM923:1 A function that is executed before the UI component is displayed.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopover/Configuration/#onShowing"
    },
    {
        prefix: "onShown",
        detail: "VM923:1 A function that is executed after the UI component is displayed.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopover/Configuration/#onShown"
    },
    {
        prefix: "onTitleRendered",
        detail: "VM923:1 A function that is executed when the UI component's title is rendered.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopover/Configuration/#onTitleRendered"
    },
    {
        prefix: "position",
        detail: "VM923:1 An object defining UI component positioning properties.",
        document: {
            type: "String | PositionConfig",
            defaultValue: "{ my: 'top center', at: 'bottom center', collision: 'fit flip' }",
            description: "",
            note: "",
            acceptValues: ['bottom', 'left', 'right', 'top'],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopover/Configuration/#position"
    },
    {
        prefix: "rtlEnabled",
        detail: "VM923:1 Switches the UI component to a right-to-left representation.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopover/Configuration/#rtlEnabled"
    },
    {
        prefix: "shading",
        detail: "VM923:1 Specifies whether to shade the background when the UI component is active.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopover/Configuration/#shading"
    },
    {
        prefix: "shadingColor",
        detail: "VM923:1 Specifies the shading color. Applies only if shading is enabled.",
        document: {
            type: "String",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopover/Configuration/#shadingColor"
    },
    {
        prefix: "showCloseButton",
        detail: "VM923:1 Specifies whether or not the UI component displays the Close button.",
        document: {
            type: "Boolean",
            defaultValue: "false, true (desktop), false (Material)",
            description: "",
            note: "The property makes sense only if the showTitle property is set to true",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopover/Configuration/#showCloseButton"
    },
    {
        prefix: "showEvent",
        detail: "VM923:1 Specifies properties for displaying the UI component.",
        document: {
            type: "Object | String",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopover/Configuration/showEvent/"
    },
    {
        prefix: "showTitle",
        detail: "VM923:1 A Boolean value specifying whether or not to display the title in the overlay window.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopover/Configuration/#showTitle"
    },
    {
        prefix: "target",
        detail: "VM923:1 Specifies the element against which to position the Popover.",
        document: {
            type: "String | HTMLElement | jQuery",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopover/Configuration/#target"
    },
    {
        prefix: "title",
        detail: "VM923:1 The title in the overlay window.",
        document: {
            type: "String",
            defaultValue: "''",
            description: "",
            note: "If the title property is specified, the titleTemplate property value is ignored",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopover/Configuration/#title"
    },
    {
        prefix: "titleComponent",
        detail: "VM923:1 An alias for the titleTemplate property specified in React. Accepts a custom component. Refer to Using a Custom Component for more information.",
        document: {
            type: "''",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopover/Configuration/#titleComponent"
    },
    {
        prefix: "titleRender",
        detail: "VM923:1 An alias for the titleTemplate property specified in React. Accepts a rendering function. Refer to Using a Rendering Function for more information.",
        document: {
            type: "''",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopover/Configuration/#titleRender"
    },
    {
        prefix: "titleTemplate",
        detail: "VM923:1 Specifies a custom template for the UI component title. Does not apply if the title is defined.",
        document: {
            type: "template",
            defaultValue: "'title'",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopover/Configuration/#titleTemplate"
    },
    {
        prefix: "toolbarItems",
        detail: "VM923:1 Configures toolbar items.",
        document: {
            type: "Array<Object>",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopover/Configuration/toolbarItems/"
    },
    {
        prefix: "visible",
        detail: "VM923:1 A Boolean value specifying whether or not the UI component is visible.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopover/Configuration/#visible"
    },
    {
        prefix: "width",
        detail: "VM923:1 Specifies the UI component's width.",
        document: {
            type: "Number | String | Function",
            defaultValue: "'auto'",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopover/Configuration/#width"
    },
    {
        prefix: "wrapperAttr",
        detail: "VM923:1 Specifies the global attributes for the UI component's wrapper element.",
        document: {
            type: "any",
            defaultValue: "{}",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopover/Configuration/#wrapperAttr"
    },

]