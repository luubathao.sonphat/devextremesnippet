
module.exports = [
    {
        prefix: "alignByColumn",
        detail: "Indicates whether to display group summary items in parentheses after the group row header or to align them by the corresponding columns within the group row.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "If this property is set to true for the first column of the grid, this setting is ignored (the summary item will be displayed in parentheses after the group row header). It happens because in the described case, the group header and the summary value occupy the same cell",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/summary/groupItems/#alignByColumn"
    },
    {
        prefix: "column",
        detail: "Specifies the column that provides data for a group summary item.",
        document: {
            type: "String",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: ['text', 'outlined', 'contained'],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/summary/groupItems/#column"
    },
    {
        prefix: "customizeText",
        detail: "Customizes the text to be displayed in the summary item.",
        document: {
            type: "Function",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/summary/groupItems/#customizeText"
    },
    {
        prefix: "displayFormat",
        detail: "Specifies the summary item's text.",
        document: {
            type: "String",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/summary/groupItems/#displayFormat"
    },
    {
        prefix: "name",
        detail: "Specifies the group summary item's identifier.",
        document: {
            type: "String",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/summary/groupItems/#name"
    },
    {
        prefix: "showInColumn",
        detail: "Specifies the column that must hold the summary item when this item is displayed in the group footer or aligned by a column in the group row.",
        document: {
            type: "String",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: ['back', 'danger', 'default', 'normal', 'success'],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/summary/groupItems/#showInColumn"
    },
    {
        prefix: "showInGroupFooter",
        detail: "Specifies whether or not a summary item must be displayed in the group footer.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/summary/groupItems/#showInGroupFooter"
    },
    {
        prefix: "skipEmptyValues",
        detail: "Specifies whether to skip empty strings, null, and undefined values when calculating a summary. Does not apply when you use a remote data source.",
        document: {
            type: "Boolean",
            defaultValue: "''",
            description: "",
            note: "Summaries of the count type do not skip empty values regardless of the skipEmptyValues property. However, you can implement a custom summary that skips empty values. Refer to the global skipEmptyValues description for a code example",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/summary/groupItems/#skipEmptyValues"
    },
    {
        prefix: "summaryType",
        detail: "Specifies how to aggregate data for the group summary item.",
        document: {
            type: "String",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: ['avg', 'count', 'custom', 'max', 'min', 'sum'],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/summary/groupItems/#summaryType"
    },
    {
        prefix: "valueFormat",
        detail: "Specifies a summary item value's display format.",
        document: {
            type: "Format",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/summary/groupItems/#valueFormat"
    },

]