
module.exports = [
    {
        prefix: "alignment",
        detail: "Specifies the alignment of a summary item.",
        document: {
            type: "String",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: ['center', 'left', 'right'],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/summary/totalItems/#alignment"
    },
    {
        prefix: "column",
        detail: "Specifies the column that provides data for a summary item.",
        document: {
            type: "String",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/summary/totalItems/#column"
    },
    {
        prefix: "cssClass",
        detail: "Specifies a CSS class to be applied to a summary item.",
        document: {
            type: "String",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/summary/totalItems/#cssClass"
    },
    {
        prefix: "customizeText",
        detail: "Customizes the text to be displayed in the summary item.",
        document: {
            type: "Function",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/summary/totalItems/#customizeText"
    },
    {
        prefix: "displayFormat",
        detail: "Specifies the summary item's text.",
        document: {
            type: "String",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/summary/totalItems/#displayFormat"
    },
    {
        prefix: "name",
        detail: "Specifies the total summary item's identifier.",
        document: {
            type: "String",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: ['back', 'danger', 'default', 'normal', 'success'],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/summary/totalItems/#name"
    },
    {
        prefix: "showInColumn",
        detail: "Specifies the column that must hold the summary item.",
        document: {
            type: "String",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/summary/totalItems/#showInColumn"
    },
    {
        prefix: "skipEmptyValues",
        detail: "Specifies whether to skip empty strings, null, and undefined values when calculating a summary. Does not apply when you use a remote data source.",
        document: {
            type: "Boolean",
            defaultValue: "''",
            description: "",
            note: "Summaries of the count type do not skip empty values regardless of the skipEmptyValues property. However, you can implement a custom summary that skips empty values. Refer to the global skipEmptyValues description for a code example",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/summary/totalItems/#skipEmptyValues"
    },
    {
        prefix: "summaryType",
        detail: "Specifies how to aggregate data for the total summary item.",
        document: {
            type: "String",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: ['avg', 'count', 'custom', 'max', 'min', 'sum'],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/summary/totalItems/#summaryType"
    },
    {
        prefix: "valueFormat",
        detail: "Specifies a summary item value's display format.",
        document: {
            type: "Format",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/summary/totalItems/#valueFormat"
    },

]