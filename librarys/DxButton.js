
module.exports = [
    {
        prefix: "accessKey",
        detail: "Specifies the shortcut key that sets focus on the UI component.",
        document: {
            type: "String",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxButton/Configuration/#accessKey"
    },
    {
        prefix: "activeStateEnabled",
        detail: "A Boolean value specifying whether or not the UI component changes its state when interacting with a user.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxButton/Configuration/#activeStateEnabled"
    },
    {
        prefix: "component",
        detail: "An alias for the template property specified in React. Accepts a custom component. Refer to Using a Custom Component for more information.",
        document: {
            type: "''",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxButton/Configuration/#component"
    },
    {
        prefix: "disabled",
        detail: "Specifies whether the UI component responds to user interaction.",
        document: {
            type: "Boolean",
            defaultValue: "{}",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxButton/Configuration/#disabled"
    },
    {
        prefix: "elementAttr",
        detail: "Specifies the global attributes to be attached to the UI component's container element.",
        document: {
            type: "Object",
            defaultValue: "true (desktop)",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxButton/Configuration/#elementAttr"
    },
    {
        prefix: "focusStateEnabled",
        detail: "Specifies whether the UI component can be focused using keyboard navigation.",
        document: {
            type: "Boolean",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxButton/Configuration/#focusStateEnabled"
    },
    {
        prefix: "height",
        detail: "Specifies the UI component's height.",
        document: {
            type: "'Number | String | Function'",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxButton/Configuration/#height"
    },
    {
        prefix: "hint",
        detail: "Specifies text for a hint that appears when a user pauses on the UI component.",
        document: {
            type: "String",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxButton/Configuration/#hint"
    },
    {
        prefix: "hoverStateEnabled",
        detail: "Specifies whether the UI component changes its state when a user pauses on it.",
        document: {
            type: "Boolean",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxButton/Configuration/#hoverStateEnabled"
    },
    {
        prefix: "icon",
        detail: "Specifies the icon to be displayed on the button.",
        document: {
            type: "String",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxButton/Configuration/#icon"
    },
    {
        prefix: "onClick",
        detail: "A function that is executed when the Button is clicked or tapped.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxButton/Configuration/#onClick"
    },
    {
        prefix: "onContentReady",
        detail: "A function that is executed when the UI component's content is ready and each time the content is changed.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxButton/Configuration/#onContentReady"
    },
    {
        prefix: "onDisposing",
        detail: "A function that is executed before the UI component is disposed of.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxButton/Configuration/#onDisposing"
    },
    {
        prefix: "onInitialized",
        detail: "A function used in JavaScript frameworks to save the UI component instance.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxButton/Configuration/#onInitialized"
    },
    {
        prefix: "onOptionChanged",
        detail: "A function that is executed after a UI component property is changed.",
        document: {
            type: "Function",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxButton/Configuration/#onOptionChanged"
    },
    {
        prefix: "render",
        detail: "An alias for the template property specified in React. Accepts a rendering function. Refer to Using a Rendering Function for more information.",
        document: {
            type: "''",
            defaultValue: "'contained'",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxButton/Configuration/#render"
    },
    {
        prefix: "rtlEnabled",
        detail: "Switches the UI component to a right-to-left representation.",
        document: {
            type: "Boolean",
            defaultValue: "Accepted Values: 'text' | 'outlined' | 'contained'",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxButton/Configuration/#rtlEnabled"
    },
    {
        prefix: "stylingMode",
        detail: "Specifies how the button is styled.",
        document: {
            type: "String",
            defaultValue: "0",
            description: "",
            note: "",
            acceptValues: ['text', 'outlined', 'contained'],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxButton/Configuration/#stylingMode"
    },
    {
        prefix: "tabIndex",
        detail: "Specifies the number of the element when the Tab key is used for navigating.",
        document: {
            type: "Number",
            defaultValue: "Default Name: 'content'",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxButton/Configuration/#tabIndex"
    },
    {
        prefix: "template",
        detail: "Specifies a custom template for the Button UI component.",
        document: {
            type: "template",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxButton/Configuration/#template"
    },
    {
        prefix: "text",
        detail: "The text displayed on the button.",
        document: {
            type: "String",
            defaultValue: "'normal'",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxButton/Configuration/#text"
    },
    {
        prefix: "type",
        detail: "Specifies the button type.",
        document: {
            type: "String",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: ['back', 'danger', 'default', 'normal', 'success'],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxButton/Configuration/#type"
    },
    {
        prefix: "useSubmitBehavior",
        detail: "Specifies whether the button submits an HTML form.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxButton/Configuration/#useSubmitBehavior"
    },
    {
        prefix: "validationGroup",
        detail: "Specifies the name of the validation group to be accessed in the click event handler.",
        document: {
            type: "String",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxButton/Configuration/#validationGroup"
    },
    {
        prefix: "visible",
        detail: "Specifies whether the UI component is visible.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxButton/Configuration/#visible"
    },
    {
        prefix: "width",
        detail: "Specifies the UI component's width.",
        document: {
            type: "'Number | String | Function'",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxButton/Configuration/#width"
    },

]