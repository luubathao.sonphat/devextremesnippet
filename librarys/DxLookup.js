
module.exports = [
    {
        prefix: "accessKey",
        detail: "Specifies the shortcut key that sets focus on the UI component.",
        document: {
            type: "String",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#accessKey"
    },
    {
        prefix: "activeStateEnabled",
        detail: "Specifies whether the UI component changes its state as a result of user interaction.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#activeStateEnabled"
    },
    {
        prefix: "applyButtonText",
        detail: "The text displayed on the Apply button.",
        document: {
            type: "String",
            defaultValue: "'OK'",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#applyButtonText"
    },
    {
        prefix: "applyValueMode",
        detail: "Specifies the way an end-user applies the selected value.",
        document: {
            type: "String",
            defaultValue: "'instantly'",
            description: "",
            note: "",
            acceptValues: ['instantly', 'useButtons'],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#applyValueMode"
    },
    {
        prefix: "cancelButtonText",
        detail: "The text displayed on the Cancel button.",
        document: {
            type: "String",
            defaultValue: "'Cancel'",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#cancelButtonText"
    },
    {
        prefix: "cleanSearchOnOpening",
        detail: "Specifies whether or not the UI component cleans the search box when the popup window is displayed.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#cleanSearchOnOpening"
    },
    {
        prefix: "clearButtonText",
        detail: "The text displayed on the Clear button.",
        document: {
            type: "String",
            defaultValue: "'Clear'",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#clearButtonText"
    },
    {
        prefix: "dataSource",
        detail: "Binds the UI component to data.",
        document: {
            type: "Store | DataSource | DataSource Configuration | String | Array < CollectionWidgetItem | any >",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#dataSource"
    },
    {
        prefix: "deferRendering",
        detail: "Specifies whether to render the drop-down field's content when it is displayed. If false, the content is rendered immediately.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#deferRendering"
    },
    {
        prefix: "disabled",
        detail: "Specifies whether the UI component responds to user interaction.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#disabled"
    },
    {
        prefix: "displayExpr",
        detail: "Specifies the data field whose values should be displayed.",
        document: {
            type: "String | Function",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#displayExpr"
    },
    {
        prefix: "displayValue",
        detail: "Returns the value currently displayed by the UI component.",
        document: {
            type: "String",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#displayValue"
    },
    {
        prefix: "dropDownButtonComponent",
        detail: "An alias for the dropDownButtonTemplate property specified in React. Accepts a custom component. Refer to Using a Custom Component for more information.",
        document: {
            type: "''",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#dropDownButtonComponent"
    },
    {
        prefix: "dropDownButtonRender",
        detail: "An alias for the dropDownButtonTemplate property specified in React. Accepts a rendering function. Refer to Using a Rendering Function for more information.",
        document: {
            type: "''",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#dropDownButtonRender"
    },
    {
        prefix: "dropDownCentered",
        detail: "Specifies whether to vertically align the drop-down menu so that the selected item is in its center. Applies only in Material Design themes.",
        document: {
            type: "Boolean",
            defaultValue: "false, true (Material)",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#dropDownCentered"
    },
    {
        prefix: "dropDownOptions",
        detail: "Configures the drop-down field.",
        document: {
            type: "Popover Configuration",
            defaultValue: "{}",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#dropDownOptions"
    },
    {
        prefix: "elementAttr",
        detail: "Specifies the global attributes to be attached to the UI component's container element.",
        document: {
            type: "Object",
            defaultValue: "{}",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#elementAttr"
    },
    {
        prefix: "fieldComponent",
        detail: "An alias for the fieldTemplate property specified in React. Accepts a custom component. Refer to Using a Custom Component for more information.",
        document: {
            type: "''",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#fieldComponent"
    },
    {
        prefix: "fieldRender",
        detail: "An alias for the fieldTemplate property specified in React. Accepts a rendering function. Refer to Using a Rendering Function for more information.",
        document: {
            type: "''",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#fieldRender"
    },
    {
        prefix: "fieldTemplate",
        detail: "Specifies a custom template for the input field.",
        document: {
            type: "template",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#fieldTemplate"
    },
    {
        prefix: "focusStateEnabled",
        detail: "Specifies whether the UI component can be focused using keyboard navigation.",
        document: {
            type: "Boolean",
            defaultValue: "false, true (desktop)",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#focusStateEnabled"
    },
    {
        prefix: "groupComponent",
        detail: "An alias for the groupTemplate property specified in React. Accepts a custom component. Refer to Using a Custom Component for more information.",
        document: {
            type: "''",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#groupComponent"
    },
    {
        prefix: "grouped",
        detail: "A Boolean value specifying whether or not to group UI component items.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "Only one-level grouping is supported",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#grouped"
    },
    {
        prefix: "groupRender",
        detail: "An alias for the groupTemplate property specified in React. Accepts a rendering function. Refer to Using a Rendering Function for more information.",
        document: {
            type: "''",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#groupRender"
    },
    {
        prefix: "groupTemplate",
        detail: "Specifies a custom template for group captions.",
        document: {
            type: "template",
            defaultValue: "'group'",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#groupTemplate"
    },
    {
        prefix: "height",
        detail: "Specifies the UI component's height.",
        document: {
            type: "Number | String | Function",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#height"
    },
    {
        prefix: "hint",
        detail: "Specifies text for a hint that appears when a user pauses on the UI component.",
        document: {
            type: "String",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#hint"
    },
    {
        prefix: "hoverStateEnabled",
        detail: "Specifies whether the UI component changes its state when a user pauses on it.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#hoverStateEnabled"
    },
    {
        prefix: "inputAttr",
        detail: "Specifies the attributes to be passed on to the underlying HTML element.",
        document: {
            type: "any",
            defaultValue: "{}",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#inputAttr"
    },
    {
        prefix: "isValid",
        detail: "Specifies or indicates whether the editor's value is valid.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "Do not use the items property if you use dataSource, and vice versa",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#isValid"
    },
    {
        prefix: "itemComponent",
        detail: "An alias for the itemTemplate property specified in React. Accepts a custom component. Refer to Using a Custom Component for more information.",
        document: {
            type: "''",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#itemComponent"
    },
    {
        prefix: "itemRender",
        detail: "An alias for the itemTemplate property specified in React. Accepts a rendering function. Refer to Using a Rendering Function for more information.",
        document: {
            type: "''",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#itemRender"
    },
    {
        prefix: "items",
        detail: "An array of items displayed by the UI component.",
        document: {
            type: "Array < CollectionWidgetItem | any >",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/items/"
    },
    {
        prefix: "itemTemplate",
        detail: "Specifies a custom template for items.",
        document: {
            type: "template",
            defaultValue: "'item'",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#itemTemplate"
    },
    {
        prefix: "label",
        detail: "Specifies a text string used to annotate the editor's value.",
        document: {
            type: "String",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#label"
    },
    {
        prefix: "labelMode",
        detail: "Specifies the label's display mode.",
        document: {
            type: "String",
            defaultValue: "'static', 'floating' (Material)",
            description: "",
            note: "If autofill is enabled in the browser, we do not recommend that you use 'floating' mode. The autofill values will overlap the label when it is displayed as a placeholder. Use 'static' mode instead",
            acceptValues: ['static', 'floating', 'hidden'],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#labelMode"
    },
    {
        prefix: "minSearchLength",
        detail: "The minimum number of characters that must be entered into the text box to begin a search. Applies only if searchEnabled is true.",
        document: {
            type: "Number",
            defaultValue: "0",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#minSearchLength"
    },
    {
        prefix: "name",
        detail: "The value to be assigned to the name attribute of the underlying HTML element.",
        document: {
            type: "String",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#name"
    },
    {
        prefix: "nextButtonText",
        detail: "The text displayed on the button used to load the next page from the data source.",
        document: {
            type: "String",
            defaultValue: "'More'",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#nextButtonText"
    },
    {
        prefix: "noDataText",
        detail: "Specifies the text or HTML markup displayed by the UI component if the item collection is empty.",
        document: {
            type: "String",
            defaultValue: "'No data to display'",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#noDataText"
    },
    {
        prefix: "onClosed",
        detail: "A function that is executed once the drop-down editor is closed.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#onClosed"
    },
    {
        prefix: "onContentReady",
        detail: "A function that is executed when the UI component's content is ready and each time the content is changed.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#onContentReady"
    },
    {
        prefix: "onDisposing",
        detail: "A function that is executed before the UI component is disposed of.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#onDisposing"
    },
    {
        prefix: "onInitialized",
        detail: "A function used in JavaScript frameworks to save the UI component instance.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#onInitialized"
    },
    {
        prefix: "onItemClick",
        detail: "A function that is executed when a list item is clicked or tapped.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#onItemClick"
    },
    {
        prefix: "onOpened",
        detail: "A function that is executed once the drop-down editor is opened.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#onOpened"
    },
    {
        prefix: "onOptionChanged",
        detail: "A function that is executed after a UI component property is changed.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#onOptionChanged"
    },
    {
        prefix: "onPageLoading",
        detail: "A function that is executed before the next page is loaded.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#onPageLoading"
    },
    {
        prefix: "onPullRefresh",
        detail: "A function that is executed when the 'pull to refresh' gesture is performed on the drop-down item list. Supported on mobile devices only.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#onPullRefresh"
    },
    {
        prefix: "onScroll",
        detail: "A function that is executed on each scroll gesture performed on the drop-down item list.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#onScroll"
    },
    {
        prefix: "onSelectionChanged",
        detail: "A function that is executed when a list item is selected or selection is canceled.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "When applyValueMode is 'useButtons', the onSelectionChanged handler is executed only when users click the OK button",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#onSelectionChanged"
    },
    {
        prefix: "onValueChanged",
        detail: "A function that is executed after the UI component's value is changed.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#onValueChanged"
    },
    {
        prefix: "opened",
        detail: "Specifies whether or not the drop-down editor is displayed.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#opened"
    },
    {
        prefix: "pageLoadingText",
        detail: "Specifies the text shown in the pullDown panel, which is displayed when the UI component is scrolled to the bottom.",
        document: {
            type: "String",
            defaultValue: "'Loading...'",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#pageLoadingText"
    },
    {
        prefix: "pageLoadMode",
        detail: "Specifies whether the next page is loaded when a user scrolls the UI component to the bottom or when the 'next' button is clicked.",
        document: {
            type: "String",
            defaultValue: "'scrollBottom'",
            description: "",
            note: "",
            acceptValues: ['nextButton', 'scrollBottom'],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#pageLoadMode"
    },
    {
        prefix: "placeholder",
        detail: "The text displayed by the UI component when nothing is selected.",
        document: {
            type: "String",
            defaultValue: "'Select'",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#placeholder"
    },
    {
        prefix: "pulledDownText",
        detail: "Specifies the text displayed in the pullDown panel when the UI component is pulled below the refresh threshold.",
        document: {
            type: "String",
            defaultValue: "'Release to refresh...'",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#pulledDownText"
    },
    {
        prefix: "pullingDownText",
        detail: "Specifies the text shown in the pullDown panel while the list is being pulled down to the refresh threshold.",
        document: {
            type: "String",
            defaultValue: "'Pull down to refresh...'",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#pullingDownText"
    },
    {
        prefix: "pullRefreshEnabled",
        detail: "A Boolean value specifying whether or not the UI component supports the 'pull down to refresh' gesture.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "The 'pull down to refresh' gesture is not supported by desktop browsers. You can use it only on mobile devices",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#pullRefreshEnabled"
    },
    {
        prefix: "refreshingText",
        detail: "Specifies the text displayed in the pullDown panel while the UI component is being refreshed.",
        document: {
            type: "String",
            defaultValue: "'Refreshing...'",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#refreshingText"
    },
    {
        prefix: "rtlEnabled",
        detail: "Switches the UI component to a right-to-left representation.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#rtlEnabled"
    },
    {
        prefix: "searchEnabled",
        detail: "Specifies whether the search box is visible.",
        document: {
            type: "Boolean",
            defaultValue: "true, false (Material)",
            description: "",
            note: "Searching works with source data of plain structure only. Subsequently, data can be transformed to hierarchical structure using the DataSource's group property",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#searchEnabled"
    },
    {
        prefix: "searchExpr",
        detail: "Specifies the name of a data source item field or an expression whose value is compared to the search criterion.",
        document: {
            type: "getter | Array < getter >",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#searchExpr"
    },
    {
        prefix: "searchMode",
        detail: "Specifies a comparison operation used to search UI component items.",
        document: {
            type: "String",
            defaultValue: "'contains'",
            description: "",
            note: "",
            acceptValues: ['contains', 'startswith'],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#searchMode"
    },
    {
        prefix: "searchPlaceholder",
        detail: "The text that is provided as a hint in the lookup's search bar.",
        document: {
            type: "String",
            defaultValue: "'Search'",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#searchPlaceholder"
    },
    {
        prefix: "searchTimeout",
        detail: "Specifies the time delay, in milliseconds, after the last character has been typed in, before a search is executed.",
        document: {
            type: "Number",
            defaultValue: "500",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#searchTimeout"
    },
    {
        prefix: "selectedItem",
        detail: "Gets the currently selected item.",
        document: {
            type: "any",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#selectedItem"
    },
    {
        prefix: "showCancelButton",
        detail: "Specifies whether to display the Cancel button in the lookup window.",
        document: {
            type: "Boolean",
            defaultValue: "true, false (Material)",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#showCancelButton"
    },
    {
        prefix: "showClearButton",
        detail: "Specifies whether to display the Clear button in the lookup window.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#showClearButton"
    },
    {
        prefix: "showDataBeforeSearch",
        detail: "Specifies whether or not the UI component displays unfiltered values until a user types a number of characters exceeding the minSearchLength property value.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#showDataBeforeSearch"
    },
    {
        prefix: "stylingMode",
        detail: "Specifies how the UI component's text field is styled.",
        document: {
            type: "String",
            defaultValue: "'outlined', 'filled' (Material)",
            description: "",
            note: "",
            acceptValues: ['outlined', 'underlined', 'filled'],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#stylingMode"
    },
    {
        prefix: "tabIndex",
        detail: "Specifies the number of the element when the Tab key is used for navigating.",
        document: {
            type: "Number",
            defaultValue: "0",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#tabIndex"
    },
    {
        prefix: "text",
        detail: "The read-only property that holds the text displayed by the UI component input element.",
        document: {
            type: "String",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#text"
    },
    {
        prefix: "useItemTextAsTitle",
        detail: "Specifies whether the Lookup uses item's text a title attribute.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#useItemTextAsTitle"
    },
    {
        prefix: "useNativeScrolling",
        detail: "Specifies whether or not the UI component uses native scrolling.",
        document: {
            type: "Boolean",
            defaultValue: "true, false (desktop except Mac)",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#useNativeScrolling"
    },
    {
        prefix: "usePopover",
        detail: "Specifies whether to show lookup contents in the Popover UI component.",
        document: {
            type: "Boolean",
            defaultValue: "false, true (desktop, iOS), false (Material)",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#usePopover"
    },
    {
        prefix: "validationError",
        detail: "Information on the broken validation rule. Contains the first item from the validationErrors array.",
        document: {
            type: "any",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#validationError"
    },
    {
        prefix: "validationErrors",
        detail: "An array of the validation rules that failed.",
        document: {
            type: "Array < any >",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#validationErrors"
    },
    {
        prefix: "validationMessageMode",
        detail: "Specifies how the message about the validation rules that are not satisfied by this editor's value is displayed.",
        document: {
            type: "String",
            defaultValue: "'auto'",
            description: "",
            note: "",
            acceptValues: ['always', 'auto'],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#validationMessageMode"
    },
    {
        prefix: "validationStatus",
        detail: "Indicates or specifies the current validation status.",
        document: {
            type: "String",
            defaultValue: "'valid'",
            description: "",
            note: "",
            acceptValues: ['valid', 'invalid', 'pending'],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#validationStatus"
    },
    {
        prefix: "value",
        detail: "Specifies the currently selected value. May be an object if dataSource contains objects and valueExpr is not set.",
        document: {
            type: "any",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#value"
    },
    {
        prefix: "valueChangeEvent",
        detail: "Specifies the DOM events after which the UI component's search results should be updated.",
        document: {
            type: "String",
            defaultValue: "'input change keyup'",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#valueChangeEvent"
    },
    {
        prefix: "valueExpr",
        detail: "Specifies which data field provides unique values to the UI component's value.",
        document: {
            type: "String | Function",
            defaultValue: "'this'",
            description: "",
            note: "You cannot specify valueExpr as a function when the UI component is bound to a remote data source. This is because valueExpr is used in a filter the UI component sends to the server when querying data. Functions with custom logic cannot be serialized for this filter",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#valueExpr"
    },
    {
        prefix: "visible",
        detail: "Specifies whether the UI component is visible.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#visible"
    },
    {
        prefix: "width",
        detail: "Specifies the UI component's width.",
        document: {
            type: "Number | String | Function",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#width"
    },
    {
        prefix: "wrapItemText",
        detail: "Specifies whether text that exceeds the drop-down list width should be wrapped.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxLookup/Configuration/#wrapItemText"
    }
]