
module.exports = [
    {
        prefix: "background",
        detail: "Specifies the properties for the range selector's background.",
        document: {
            type: "Object",
            defaultValue: "",
            description: "",
            note: "You can use this property only when the range selector displays an image or chart. If not, use the selectedRangeColor and tick to customize the appearance of your UI component",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxRangeSelector/Configuration/background/"
    },
    {
        prefix: "behavior",
        detail: "VM1043:1 Specifies the RangeSelector's behavior properties.",
        document: {
            type: "Object",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxRangeSelector/Configuration/behavior/"
    },
    {
        prefix: "chart",
        detail: "VM1043:1 Specifies the properties required to display a chart as the range selector's background.",
        document: {
            type: "Object",
            defaultValue: "",
            description: "",
            note: "You may need to use the rangeValue1Field and rangeValue2Field properties instead of the valueField property if you define range-like series. For the candleStick and stock series types, specific properties should also be specified instead of the valueField property",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxRangeSelector/Configuration/chart/"
    },
    {
        prefix: "containerBackgroundColor",
        detail: "VM1043:1 Specifies the color of the parent page element.",
        document: {
            type: "String",
            defaultValue: "'#FFFFFF'",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxRangeSelector/Configuration/#containerBackgroundColor"
    },
    {
        prefix: "dataSource",
        detail: "VM1043:1 Specifies a data source for the scale values and for the chart at the background.",
        document: {
            type: "Store | DataSource | DataSource Configuration | String | Array<any>",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxRangeSelector/Configuration/#dataSource"
    },
    {
        prefix: "dataSourceField",
        detail: "VM1043:1 Specifies the data source field that provides data for the scale.",
        document: {
            type: "String",
            defaultValue: "'arg'",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxRangeSelector/Configuration/#dataSourceField"
    },
    {
        prefix: "disabled",
        detail: "VM1043:1 Specifies whether the UI component responds to user interaction.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxRangeSelector/Configuration/#disabled"
    },
    {
        prefix: "elementAttr",
        detail: "VM1043:1 Specifies the global attributes to be attached to the UI component's container element.",
        document: {
            type: "Object",
            defaultValue: "{}",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxRangeSelector/Configuration/#elementAttr"
    },
    {
        prefix: "export",
        detail: "VM1043:1 Configures the exporting and printing features.",
        document: {
            type: "Object",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxRangeSelector/Configuration/export/"
    },
    {
        prefix: "indent",
        detail: "VM1043:1 Range selector's indent properties.",
        document: {
            type: "Object",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxRangeSelector/Configuration/indent/"
    },
    {
        prefix: "loadingIndicator",
        detail: "VM1043:1 Configures the loading indicator.",
        document: {
            type: "Object",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxRangeSelector/Configuration/loadingIndicator/"
    },
    {
        prefix: "margin",
        detail: "VM1043:1 Generates space around the UI component.",
        document: {
            type: "Object",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxRangeSelector/Configuration/margin/"
    },
    {
        prefix: "onDisposing",
        detail: "VM1043:1 A function that is executed before the UI component is disposed of.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxRangeSelector/Configuration/#onDisposing"
    },
    {
        prefix: "onDrawn",
        detail: "VM1043:1 A function that is executed when the UI component's rendering has finished.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxRangeSelector/Configuration/#onDrawn"
    },
    {
        prefix: "onExported",
        detail: "VM1043:1 A function that is executed after the UI component is exported.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxRangeSelector/Configuration/#onExported"
    },
    {
        prefix: "onExporting",
        detail: "VM1043:1 A function that is executed before the UI component is exported.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxRangeSelector/Configuration/#onExporting"
    },
    {
        prefix: "onFileSaving",
        detail: "VM1043:1 A function that is executed before a file with exported UI component is saved to the user's local storage.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxRangeSelector/Configuration/#onFileSaving"
    },
    {
        prefix: "onIncidentOccurred",
        detail: "VM1043:1 A function that is executed when an error or warning occurs.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxRangeSelector/Configuration/#onIncidentOccurred"
    },
    {
        prefix: "onInitialized",
        detail: "VM1043:1 A function used in JavaScript frameworks to save the UI component instance.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxRangeSelector/Configuration/#onInitialized"
    },
    {
        prefix: "onOptionChanged",
        detail: "VM1043:1 A function that is executed after a UI component property is changed.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxRangeSelector/Configuration/#onOptionChanged"
    },
    {
        prefix: "onValueChanged",
        detail: "VM1043:1 A function that is executed after the UI component's value is changed.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxRangeSelector/Configuration/#onValueChanged"
    },
    {
        prefix: "pathModified",
        detail: "VM1043:1 Notifies the UI component that it is embedded into an HTML page that uses a tag modifying the path.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxRangeSelector/Configuration/#pathModified"
    },
    {
        prefix: "redrawOnResize",
        detail: "VM1043:1 Specifies whether to redraw the UI component when the size of the parent browser window changes or a mobile device rotates.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "To redraw the UI component after the size of its container has changed, call the render() method of the UI component's instance",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxRangeSelector/Configuration/#redrawOnResize"
    },
    {
        prefix: "rtlEnabled",
        detail: "VM1043:1 Switches the UI component to a right-to-left representation.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "In a right-to-left representation, SVG elements have the direction attribute with the rtl value. This might cause problems when rendering left-to-right texts. Use this property if you have only right-to-left texts",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxRangeSelector/Configuration/#rtlEnabled"
    },
    {
        prefix: "scale",
        detail: "VM1043:1 Specifies properties of the range selector's scale.",
        document: {
            type: "Object",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxRangeSelector/Configuration/scale/"
    },
    {
        prefix: "selectedRangeColor",
        detail: "VM1043:1 Specifies the color of the selected range.",
        document: {
            type: "String",
            defaultValue: "'#606060'",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxRangeSelector/Configuration/#selectedRangeColor"
    },
    {
        prefix: "selectedRangeUpdateMode",
        detail: "VM1043:1 Specifies how the selected range should behave when data is updated. Applies only when the RangeSelector is bound to a data source.",
        document: {
            type: "String",
            defaultValue: "'reset'",
            description: "",
            note: "",
            acceptValues: ['auto', 'keep', 'reset', 'shift'],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxRangeSelector/Configuration/#selectedRangeUpdateMode"
    },
    {
        prefix: "shutter",
        detail: "VM1043:1 Specifies range selector shutter properties.",
        document: {
            type: "Object",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxRangeSelector/Configuration/shutter/"
    },
    {
        prefix: "size",
        detail: "VM1043:1 Specifies the UI component's size in pixels.",
        document: {
            type: "Object",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxRangeSelector/Configuration/size/"
    },
    {
        prefix: "sliderHandle",
        detail: "VM1043:1 Specifies the appearance of the range selector's slider handles.",
        document: {
            type: "Object",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxRangeSelector/Configuration/sliderHandle/"
    },
    {
        prefix: "sliderMarker",
        detail: "VM1043:1 Defines the properties of the range selector slider markers.",
        document: {
            type: "Object",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxRangeSelector/Configuration/sliderMarker/"
    },
    {
        prefix: "theme",
        detail: "VM1043:1 Sets the name of the theme the UI component uses.",
        document: {
            type: "String",
            defaultValue: "'generic.light'",
            description: "",
            note: "",
            acceptValues: ['generic.dark', 'generic.light', 'generic.contrast', 'generic.carmine', 'generic.darkmoon', 'generic.darkviolet', 'generic.greenmist', 'generic.softblue', 'material.blue.light', 'material.lime.light', 'material.orange.light', 'material.purple.light', 'material.teal.light'],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxRangeSelector/Configuration/#theme"
    },
    {
        prefix: "title",
        detail: "VM1043:1 Configures the UI component's title.",
        document: {
            type: "Object | String",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxRangeSelector/Configuration/title/"
    },
    {
        prefix: "value",
        detail: "VM1043:1 The selected range (initial or current). Equals the entire scale when not set.",
        document: {
            type: "Array<Number | String | Date> | Object",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxRangeSelector/Configuration/value/"
    },

]