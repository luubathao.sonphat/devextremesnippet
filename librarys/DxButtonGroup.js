
module.exports = [
    {
        prefix: "accessKey",
        detail: "Specifies the shortcut key that sets focus on the UI component.",
        document: {
            type: "String",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxButtonGroup/Configuration/#accessKey"
    },
    {
        prefix: "activeStateEnabled",
        detail: "Specifies whether the UI component changes its state as a result of user interaction.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxButtonGroup/Configuration/#activeStateEnabled"
    },
    {
        prefix: "buttonComponent",
        detail: "An alias for the buttonTemplate property specified in React. Accepts a custom component. Refer to Using a Custom Component for more information.",
        document: {
            type: "''",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxButtonGroup/Configuration/#buttonComponent"
    },
    {
        prefix: "buttonRender",
        detail: "An alias for the buttonTemplate property specified in React. Accepts a rendering function. Refer to Using a Rendering Function for more information.",
        document: {
            type: "''",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxButtonGroup/Configuration/#buttonRender"
    },
    {
        prefix: "buttonTemplate",
        detail: "Specifies a template for all the buttons in the group.",
        document: {
            type: "template",
            defaultValue: "'content'",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxButtonGroup/Configuration/#buttonTemplate"
    },
    {
        prefix: "disabled",
        detail: "Specifies whether the UI component responds to user interaction.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxButtonGroup/Configuration/#disabled"
    },
    {
        prefix: "elementAttr",
        detail: "Specifies the global attributes to be attached to the UI component's container element.",
        document: {
            type: "Object",
            defaultValue: "{}",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxButtonGroup/Configuration/#elementAttr"
    },
    {
        prefix: "focusStateEnabled",
        detail: "Specifies whether the UI component can be focused using keyboard navigation.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxButtonGroup/Configuration/#focusStateEnabled"
    },
    {
        prefix: "height",
        detail: "Specifies the UI component's height.",
        document: {
            type: "Number | String | Function",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxButtonGroup/Configuration/#height"
    },
    {
        prefix: "hint",
        detail: "Specifies text for a hint that appears when a user pauses on the UI component.",
        document: {
            type: "String",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxButtonGroup/Configuration/#hint"
    },
    {
        prefix: "hoverStateEnabled",
        detail: "Specifies whether the UI component changes its state when a user pauses on it.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxButtonGroup/Configuration/#hoverStateEnabled"
    },
    {
        prefix: "items",
        detail: "Configures buttons in the group.",
        document: {
            type: "Array<dxButtonGroupItem>",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxButtonGroup/Configuration/items/"
    },
    {
        prefix: "keyExpr",
        detail: "Specifies which data field provides keys used to distinguish between the selected buttons.",
        document: {
            type: "String | Function",
            defaultValue: "'text'",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxButtonGroup/Configuration/#keyExpr"
    },
    {
        prefix: "onContentReady",
        detail: "A function that is executed when the UI component's content is ready and each time the content is changed.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxButtonGroup/Configuration/#onContentReady"
    },
    {
        prefix: "onDisposing",
        detail: "A function that is executed before the UI component is disposed of.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxButtonGroup/Configuration/#onDisposing"
    },
    {
        prefix: "onInitialized",
        detail: "A function used in JavaScript frameworks to save the UI component instance.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxButtonGroup/Configuration/#onInitialized"
    },
    {
        prefix: "onItemClick",
        detail: "A function that is executed when a button is clicked or tapped.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxButtonGroup/Configuration/#onItemClick"
    },
    {
        prefix: "onOptionChanged",
        detail: "A function that is executed after a UI component property is changed.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxButtonGroup/Configuration/#onOptionChanged"
    },
    {
        prefix: "onSelectionChanged",
        detail: "A function that is executed when a button is selected or selection is canceled.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxButtonGroup/Configuration/#onSelectionChanged"
    },
    {
        prefix: "rtlEnabled",
        detail: "Switches the UI component to a right-to-left representation.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxButtonGroup/Configuration/#rtlEnabled"
    },
    {
        prefix: "selectedItemKeys",
        detail: "Contains the keys of the selected buttons and allows selecting buttons initially.",
        document: {
            type: "Array<any>",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxButtonGroup/Configuration/#selectedItemKeys"
    },
    {
        prefix: "selectedItems",
        detail: "Contains the data objects that correspond to the selected buttons. The data objects are taken from the items array.",
        document: {
            type: "Array<any>",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxButtonGroup/Configuration/#selectedItems"
    },
    {
        prefix: "selectionMode",
        detail: "Specifies the button selection mode.",
        document: {
            type: "String",
            defaultValue: "'single'",
            description: "",
            note: "",
            acceptValues: ['multiple', 'single', 'none'],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxButtonGroup/Configuration/#selectionMode"
    },
    {
        prefix: "stylingMode",
        detail: "Specifies how buttons in the group are styled.",
        document: {
            type: "String",
            defaultValue: "'contained'",
            description: "",
            note: "",
            acceptValues: ['text', 'outlined', 'contained'],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxButtonGroup/Configuration/#stylingMode"
    },
    {
        prefix: "tabIndex",
        detail: "Specifies the number of the element when the Tab key is used for navigating.",
        document: {
            type: "Number",
            defaultValue: "0",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxButtonGroup/Configuration/#tabIndex"
    },
    {
        prefix: "visible",
        detail: "Specifies whether the UI component is visible.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxButtonGroup/Configuration/#visible"
    },
    {
        prefix: "width",
        detail: "Specifies the UI component's width.",
        document: {
            type: "Number | String | Function",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxButtonGroup/Configuration/#width"
    },

]