
module.exports = [
    {
        prefix: "acceptCustomValue",
        detail: "Specifies whether or not the UI component allows an end-user to enter a custom value.",
        document: {
            type: "Boolean",
            defauleValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDateBox/Configuration/#acceptCustomValue"
    },
    {
        prefix: "accessKey",
        detail: "Specifies the shortcut key that sets focus on the UI component.",
        document: {
            type: "String",
            defauleValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDateBox/Configuration/#accessKey"
    },
    {
        prefix: "activeStateEnabled",
        detail: "Specifies whether or not the UI component changes its state when interacting with a user.",
        document: {
            type: "Boolean",
            defauleValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDateBox/Configuration/#activeStateEnabled"
    },
    {
        prefix: "adaptivityEnabled",
        detail: "Specifies whether or not adaptive UI component rendering is enabled on a small screen.",
        document: {
            type: "Boolean",
            defauleValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDateBox/Configuration/#adaptivityEnabled"
    },
    {
        prefix: "applyButtonText",
        detail: "The text displayed on the Apply button.",
        document: {
            type: "String",
            defauleValue: "'OK'",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDateBox/Configuration/#applyButtonText"
    },
    {
        prefix: "applyValueMode",
        detail: "Specifies the way an end-user applies the selected value.",
        document: {
            type: "String",
            defauleValue: "'instantly'",
            description: "",
            note: "",
            acceptValues: ['instantly', 'useButtons'],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDateBox/Configuration/#applyValueMode"
    },
    {
        prefix: "buttons",
        detail: "Allows you to add custom buttons to the input text field.",
        document: {
            type: "Array<String | Object>",
            defauleValue: "undefined",
            description: "",
            note: "",
            acceptValues: ['clear', 'dropDown'],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDateBox/Configuration/buttons/"
    },
    {
        prefix: "calendarOptions",
        detail: "Configures the calendar's value picker. Applies only if the pickerType is 'calendar'.",
        document: {
            type: "'Calendar Configuration'",
            defauleValue: "{}",
            description: "",
            note: "In Angular and Vue, the nested component that configures the calendarOptions property does not support event bindings and two-way property bindings.",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDateBox/Configuration/#calendarOptions"
    },
    {
        prefix: "cancelButtonText",
        detail: "The text displayed on the Cancel button.",
        document: {
            type: "String",
            defauleValue: "'Cancel'",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDateBox/Configuration/#cancelButtonText"
    },
    {
        prefix: "dateOutOfRangeMessage",
        detail: "Specifies the message displayed if the specified date is later than the max value or earlier than the min value.",
        document: {
            type: "String",
            defauleValue: "'Value is out of range'",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDateBox/Configuration/#dateOutOfRangeMessage"
    },
    {
        prefix: "dateSerializationFormat",
        detail: "Specifies the date-time value serialization format. Use it only if you do not specify the value at design time.",
        document: {
            type: "String",
            defauleValue: "undefined",
            description: "",
            note: "If you are going to change the value using the API, make sure that it has the same format that you specified in this property.",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDateBox/Configuration/#dateSerializationFormat"
    },
    {
        prefix: "deferRendering",
        detail: "Specifies whether to render the drop-down field's content when it is displayed. If false, the content is rendered immediately.",
        document: {
            type: "Boolean",
            defauleValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDateBox/Configuration/#deferRendering"
    },
    {
        prefix: "disabled",
        detail: "Specifies whether the UI component responds to user interaction.",
        document: {
            type: "Boolean",
            defauleValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDateBox/Configuration/#disabled"
    },
    {
        prefix: "disabledDates",
        detail: "Specifies dates that users cannot select. Applies only if pickerType is 'calendar'.",
        document: {
            type: "'Array<Date> | Function'",
            defauleValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDateBox/Configuration/#disabledDates"
    },
    {
        prefix: "displayFormat",
        detail: "Specifies the date display format. Ignored if the pickerType property is 'native'",
        document: {
            type: "Format",
            defauleValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDateBox/Configuration/#displayFormat"
    },
    {
        prefix: "dropDownButtonComponent",
        detail: "An alias for the dropDownButtonTemplate property specified in React. Accepts a custom component. Refer to Using a Custom Component for more information.",
        document: {
            type: "",
            defauleValue: "",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDateBox/Configuration/#dropDownButtonComponent"
    },
    {
        prefix: "dropDownButtonRender",
        detail: "An alias for the dropDownButtonTemplate property specified in React. Accepts a rendering function. Refer to Using a Rendering Function for more information.",
        document: {
            type: "",
            defauleValue: "",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDateBox/Configuration/#dropDownButtonRender"
    },
    {
        prefix: "dropDownButtonTemplate",
        detail: "Specifies a custom template for the drop-down button.",
        document: {
            type: "template",
            defauleValue: "'dropDownButton'",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDateBox/Configuration/#dropDownButtonTemplate"
    },
    {
        prefix: "dropDownOptions",
        detail: "Configures the drop-down field which holds the content.",
        document: {
            type: "'Popup Configuration'",
            defauleValue: "{}",
            description: "",
            note: "In Angular and Vue, the nested component that configures the dropDownOptions property does not support event bindings and two-way property bindings.",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDateBox/Configuration/#dropDownOptions"
    },
    {
        prefix: "elementAttr",
        detail: "Specifies the global attributes to be attached to the UI component's container element.",
        document: {
            type: "Object",
            defauleValue: "{}",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDateBox/Configuration/#elementAttr"
    },
    {
        prefix: "focusStateEnabled",
        detail: "Specifies whether the UI component can be focused using keyboard navigation.",
        document: {
            type: "Boolean",
            defauleValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDateBox/Configuration/#focusStateEnabled"
    },
    {
        prefix: "height",
        detail: "Specifies the UI component's height.",
        document: {
            defauleValue: "undefined",
            type: "'Number | String | Function'",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDateBox/Configuration/#height"
    },
    {
        prefix: "hint",
        detail: "Specifies text for a hint that appears when a user pauses on the UI component.",
        document: {
            type: "String",
            defauleValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDateBox/Configuration/#hint"
    },
    {
        prefix: "hoverStateEnabled",
        detail: "Specifies whether the UI component changes its state when a user pauses on it.",
        document: {
            type: "Boolean",
            defauleValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDateBox/Configuration/#hoverStateEnabled"
    },
    {
        prefix: "inputAttr",
        detail: "Specifies the attributes to be passed on to the underlying HTML element.",
        document: {
            type: "any",
            defauleValue: "{}",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDateBox/Configuration/#inputAttr"
    },
    {
        prefix: "interval",
        detail: "Specifies the interval between neighboring values in the popup list in minutes.",
        document: {
            type: "Number",
            defauleValue: "30",
            description: "",
            note: "This property applies only if the type property is set to 'time' and the pickerType is 'list'.",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDateBox/Configuration/#interval"
    },
    {
        prefix: "invalidDateMessage",
        detail: "Specifies the message displayed if the typed value is not a valid date or time.",
        document: {
            type: "String",
            defauleValue: "'Value must be a date or time'",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDateBox/Configuration/#invalidDateMessage"
    },
    {
        prefix: "isValid",
        detail: "Specifies or indicates whether the editor's value is valid.",
        document: {
            type: "Boolean",
            defauleValue: "true",
            description: "",
            note: "When you use async rules, isValid is true if the status is 'pending' or 'valid'.",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDateBox/Configuration/#isValid"
    },
    {
        prefix: "label",
        detail: "Specifies a text string used to annotate the editor's value.",
        document: {
            type: "String",
            defauleValue: "",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDateBox/Configuration/#label"
    },
    {
        prefix: "labelMode",
        detail: "Specifies the label's display mode.",
        document: {
            type: "String",
            defauleValue: "'static', 'floating' (Material)",
            description: "",
            note: "If autofill is enabled in the browser, we do not recommend that you use 'floating' mode. The autofill values will overlap the label when it is displayed as a placeholder. Use 'static' mode instead.",
            acceptValues: ['static', 'floating', 'hidden'],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDateBox/Configuration/#labelMode"
    },
    {
        prefix: "max",
        detail: "The last date that can be selected within the UI component.",
        document: {
            type: "Date | Number | String",
            defauleValue: "undefined",
            description: "",
            note: "If this property is undefined, pickerType - 'rollers', type - 'date' or 'datetime', the UI component renders values up to 50 years from the current date.",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDateBox/Configuration/#max"
    },
    {
        prefix: "maxLength",
        detail: "Specifies the maximum number of characters you can enter into the textbox.",
        document: {
            type: "String | Number",
            defauleValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDateBox/Configuration/#maxLength"
    },
    {
        prefix: "min",
        detail: "The minimum date that can be selected within the UI component.",
        document: {
            type: "Date | Number | String",
            defauleValue: "undefined",
            description: "",
            note: "If this property is undefined, pickerType - 'rollers', type - 'date' or 'datetime', the UI component renders values from 01/01/1900.",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDateBox/Configuration/#min"
    },
    {
        prefix: "name",
        detail: "The value to be assigned to the name attribute of the underlying HTML element.",
        document: {
            type: "String",
            defauleValue: "",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDateBox/Configuration/#name"
    },
    {
        prefix: "onChange",
        detail: "A function that is executed when the UI component loses focus after the text field's content was changed using the keyboard.",
        document: {
            type: "Function",
            defauleValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDateBox/Configuration/#onChange"
    },
    {
        prefix: "onClosed",
        detail: "A function that is executed once the drop-down editor is closed.",
        document: {
            type: "Function",
            defauleValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDateBox/Configuration/#onClosed"
    },
    {
        prefix: "onContentReady",
        detail: "A function that is executed when the UI component's content is ready and each time the content is changed.",
        document: {
            type: "Function",
            defauleValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDateBox/Configuration/#onContentReady"
    },
    {
        prefix: "onCopy",
        detail: "A function that is executed when the UI component's input has been copied.",
        document: {
            type: "Function",
            defauleValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDateBox/Configuration/#onCopy"
    },
    {
        prefix: "onCut",
        detail: "A function that is executed when the UI component's input has been cut.",
        document: {
            type: "Function",
            defauleValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDateBox/Configuration/#onCut"
    },
    {
        prefix: "onDisposing",
        detail: "A function that is executed before the UI component is disposed of.",
        document: {
            type: "Function",
            defauleValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDateBox/Configuration/#onDisposing"
    },
    {
        prefix: "onEnterKey",
        detail: "A function that is executed when the Enter key has been pressed while the UI component is focused.",
        document: {
            type: "Function",
            defauleValue: "null",
            description: "",
            note: "This function is executed after the onKeyUp and onKeyDown functions.",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDateBox/Configuration/#onEnterKey"
    },
    {
        prefix: "onFocusIn",
        detail: "A function that is executed when the UI component gets focus.",
        document: {
            type: "Function",
            defauleValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDateBox/Configuration/#onFocusIn"
    },
    {
        prefix: "onFocusOut",
        detail: "A function that is executed when the UI component loses focus.",
        document: {
            type: "Function",
            defauleValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDateBox/Configuration/#onFocusOut"
    },
    {
        prefix: "onInitialized",
        detail: "A function used in JavaScript frameworks to save the UI component instance.",
        document: {
            type: "Function",
            defauleValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDateBox/Configuration/#onInitialized"
    },
    {
        prefix: "onInput",
        detail: "A function that is executed each time the UI component's input is changed while the UI component is focused.",
        document: {
            type: "Function",
            defauleValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDateBox/Configuration/#onInput"
    },
    {
        prefix: "onKeyDown",
        detail: "A function that is executed when a user is pressing a key on the keyboard.",
        document: {
            type: "Function",
            defauleValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDateBox/Configuration/#onKeyDown"
    },
    {
        prefix: "onKeyUp",
        detail: "A function that is executed when a user releases a key on the keyboard.",
        document: {
            type: "Function",
            defauleValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDateBox/Configuration/#onKeyUp"
    },
    {
        prefix: "onOpened",
        detail: "A function that is executed once the drop-down editor is opened.",
        document: {
            type: "Function",
            defauleValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDateBox/Configuration/#onOpened"
    },
    {
        prefix: "onOptionChanged",
        detail: "A function that is executed after a UI component property is changed.",
        document: {
            type: "Function",
            defauleValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDateBox/Configuration/#onOptionChanged"
    },
    {
        prefix: "onPaste",
        detail: "A function that is executed when the UI component's input has been pasted.",
        document: {
            type: "Function",
            defauleValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDateBox/Configuration/#onPaste"
    },
    {
        prefix: "onValueChanged",
        detail: "A function that is executed after the UI component's value is changed.",
        document: {
            type: "Function",
            defauleValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDateBox/Configuration/#onValueChanged"
    },
    {
        prefix: "opened",
        detail: "Specifies whether or not the drop-down editor is displayed.",
        document: {
            type: "Boolean",
            defauleValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDateBox/Configuration/#opened"
    },
    {
        prefix: "openOnFieldClick",
        detail: "Specifies whether a user can open the drop-down list by clicking a text field.",
        document: {
            type: "Boolean",
            defauleValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDateBox/Configuration/#openOnFieldClick"
    },
    {
        prefix: "pickerType",
        detail: "Specifies the type of the date/time picker.",
        document: {
            type: "String",
            defauleValue: "'calendar', 'native' (iOS, Android), 'rollers' (Android below version 4.4, mobile devices)",
            description: "",
            note: "",
            acceptValues: ['calendar', 'list', 'native', 'rollers'],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDateBox/Configuration/#pickerType"
    },
    {
        prefix: "placeholder",
        detail: "Specifies a placeholder for the input field.",
        document: {
            type: "String",
            defauleValue: "",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDateBox/Configuration/#placeholder"
    },
    {
        prefix: "readOnly",
        detail: "Specifies whether the editor is read-only.",
        document: {
            type: "Boolean",
            defauleValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDateBox/Configuration/#readOnly"
    },
    {
        prefix: "rtlEnabled",
        detail: "Switches the UI component to a right-to-left representation.",
        document: {
            type: "Boolean",
            defauleValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDateBox/Configuration/#rtlEnabled"
    },
    {
        prefix: "showAnalogClock",
        detail: "Specifies whether to show the analog clock in the value picker. Applies only if type is 'datetime' and pickerType is 'calendar'.",
        document: {
            type: "Boolean",
            defauleValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDateBox/Configuration/#showAnalogClock"
    },
    {
        prefix: "showClearButton",
        detail: "Specifies whether to display the Clear button in the UI component.",
        document: {
            type: "Boolean",
            defauleValue: "false",
            description: "",
            note: "The Clear button can be displayed only if the pickerType is not 'native' or if the native date-time picker includes it.",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDateBox/Configuration/#showClearButton"
    },
    {
        prefix: "showDropDownButton",
        detail: "Specifies whether the drop-down button is visible.",
        document: {
            type: "Boolean",
            defauleValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDateBox/Configuration/#showDropDownButton"
    },
    {
        prefix: "spellcheck",
        detail: "Specifies whether or not the UI component checks the inner text for spelling mistakes.",
        document: {
            type: "Boolean",
            defauleValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDateBox/Configuration/#spellcheck"
    },
    {
        prefix: "stylingMode",
        detail: "Specifies how the UI component's text field is styled.",
        document: {
            type: "String",
            defauleValue: "'outlined', 'filled' (Material)",
            description: "",
            note: "",
            acceptValues: ['outlined', 'underlined', 'filled'],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDateBox/Configuration/#stylingMode"
    },
    {
        prefix: "tabIndex",
        detail: "Specifies the number of the element when the Tab key is used for navigating.",
        document: {
            type: "Number",
            defauleValue: "0",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDateBox/Configuration/#tabIndex"
    },
    {
        prefix: "text",
        detail: "The read-only property that holds the text displayed by the UI component input element.",
        document: {
            type: "String",
            defauleValue: "",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDateBox/Configuration/#text"
    },
    {
        prefix: "type",
        detail: "A format used to display date/time information.",
        document: {
            type: "String",
            defauleValue: "'date'",
            description: "",
            note: "",
            acceptValues: ['date', 'datetime', 'time'],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDateBox/Configuration/#type"
    },
    {
        prefix: "useMaskBehavior",
        detail: "Specifies whether to control user input using a mask created based on the displayFormat.",
        document: {
            type: "Boolean",
            defauleValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDateBox/Configuration/#useMaskBehavior"
    },
    {
        prefix: "validationError",
        detail: "Information on the broken validation rule. Contains the first item from the validationErrors array.",
        document: {
            type: "any",
            defauleValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDateBox/Configuration/#validationError"
    },
    {
        prefix: "validationErrors",
        detail: "An array of the validation rules that failed.",
        document: {
            type: "Array<any>",
            defauleValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDateBox/Configuration/#validationErrors"
    },
    {
        prefix: "validationMessageMode",
        detail: "Specifies how the message about the validation rules that are not satisfied by this editor's value is displayed.",
        document: {
            type: "String",
            defauleValue: "'auto'",
            description: "",
            note: "",
            acceptValues: ['always', 'auto'],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDateBox/Configuration/#validationMessageMode"
    },
    {
        prefix: "validationStatus",
        detail: "Indicates or specifies the current validation status.",
        document: {
            type: "String",
            defauleValue: "'valid'",
            description: "",
            note: "",
            acceptValues: ['valid', 'invalid', 'pending'],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDateBox/Configuration/#validationStatus"
    },
    {
        prefix: "value",
        detail: "An object or a value specifying the date and time currently selected using the date box.",
        document: {
            type: "'Date | Number | String'",
            defauleValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDateBox/Configuration/#value"
    },
    {
        prefix: "valueChangeEvent",
        detail: "Specifies the DOM events after which the UI component's value should be updated.",
        document: {
            type: "String",
            defauleValue: "'change'",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDateBox/Configuration/#valueChangeEvent"
    },
    {
        prefix: "visible",
        detail: "Specifies whether the UI component is visible.",
        document: {
            type: "Boolean",
            defauleValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDateBox/Configuration/#visible"
    },
    {
        prefix: "width",
        detail: "Specifies the UI component's width.",
        document: {
            type: "'Number | String | Function'",
            defauleValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDateBox/Configuration/#width"
    },
]