
module.exports = [
    {
        prefix: "accessKey",
        detail: "Specifies the shortcut key that sets focus on the UI component.",
        document: {
            type: "String",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxMenu/Configuration/#accessKey"
    },
    {
        prefix: "activeStateEnabled",
        detail: "A Boolean value specifying whether or not the UI component changes its state when interacting with a user.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxMenu/Configuration/#activeStateEnabled"
    },
    {
        prefix: "adaptivityEnabled",
        detail: "Specifies whether adaptive UI component rendering is enabled on small screens. Applies only if the orientation is 'horizontal'.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxMenu/Configuration/#adaptivityEnabled"
    },
    {
        prefix: "animation",
        detail: "Configures UI component visibility animations. This object contains two fields: show and hide.",
        document: {
            type: "Object",
            defaultValue: "{ show: { type: 'fade', from: 0, to: 1, duration: 100 }, hide: { type: 'fade', from: 1, to: 0, duration: 100 } }",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxMenu/Configuration/animation/"
    },
    {
        prefix: "cssClass",
        detail: "Specifies the name of the CSS class to be applied to the root menu level and all submenus.",
        document: {
            type: "String",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxMenu/Configuration/#cssClass"
    },
    {
        prefix: "dataSource",
        detail: "Binds the UI component to data.",
        document: {
            type: "String | Array<dxMenuItem> | Store | DataSource | DataSource Configuration",
            defaultValue: "null",
            description: "",
            note: "Review the following notes about data binding",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxMenu/Configuration/#dataSource"
    },
    {
        prefix: "disabled",
        detail: "Specifies whether the UI component responds to user interaction.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxMenu/Configuration/#disabled"
    },
    {
        prefix: "disabledExpr",
        detail: "Specifies the name of the data source item field whose value defines whether or not the corresponding UI component item is disabled.",
        document: {
            type: "String | Function",
            defaultValue: "'disabled'",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxMenu/Configuration/#disabledExpr"
    },
    {
        prefix: "displayExpr",
        detail: "Specifies the data field whose values should be displayed.",
        document: {
            type: "String | Function",
            defaultValue: "'text'",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxMenu/Configuration/#displayExpr"
    },
    {
        prefix: "elementAttr",
        detail: "Specifies the global attributes to be attached to the UI component's container element.",
        document: {
            type: "Object",
            defaultValue: "{}",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxMenu/Configuration/#elementAttr"
    },
    {
        prefix: "focusStateEnabled",
        detail: "Specifies whether the UI component can be focused using keyboard navigation.",
        document: {
            type: "Boolean",
            defaultValue: "true (desktop)",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxMenu/Configuration/#focusStateEnabled"
    },
    {
        prefix: "height",
        detail: "Specifies the UI component's height.",
        document: {
            type: "Number | String | Function",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxMenu/Configuration/#height"
    },
    {
        prefix: "hideSubmenuOnMouseLeave",
        detail: "Specifies whether or not the submenu is hidden when the mouse pointer leaves it.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxMenu/Configuration/#hideSubmenuOnMouseLeave"
    },
    {
        prefix: "hint",
        detail: "Specifies text for a hint that appears when a user pauses on the UI component.",
        document: {
            type: "String",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxMenu/Configuration/#hint"
    },
    {
        prefix: "hoverStateEnabled",
        detail: "Specifies whether the UI component changes its state when a user pauses on it.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxMenu/Configuration/#hoverStateEnabled"
    },
    {
        prefix: "itemComponent",
        detail: "An alias for the itemTemplate property specified in React. Accepts a custom component. Refer to Using a Custom Component for more information.",
        document: {
            type: "''",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxMenu/Configuration/#itemComponent"
    },
    {
        prefix: "itemRender",
        detail: "An alias for the itemTemplate property specified in React. Accepts a rendering function. Refer to Using a Rendering Function for more information.",
        document: {
            type: "''",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxMenu/Configuration/#itemRender"
    },
    {
        prefix: "items",
        detail: "Holds an array of menu items.",
        document: {
            type: "Array<dxMenuItem>",
            defaultValue: "''",
            description: "",
            note: "Do not use the items property if you use dataSource, and vice versa.",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxMenu/Configuration/items/"
    },
    {
        prefix: "itemsExpr",
        detail: "Specifies which data field contains nested items.",
        document: {
            type: "String | Function",
            defaultValue: "'items'",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxMenu/Configuration/#itemsExpr"
    },
    {
        prefix: "itemTemplate",
        detail: "Specifies a custom template for items.",
        document: {
            type: "template",
            defaultValue: "Default Name: 'item'",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxMenu/Configuration/#itemTemplate"
    },
    {
        prefix: "onContentReady",
        detail: "A function that is executed when the UI component's content is ready and each time the content is changed.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxMenu/Configuration/#onContentReady"
    },
    {
        prefix: "onDisposing",
        detail: "A function that is executed before the UI component is disposed of.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxMenu/Configuration/#onDisposing"
    },
    {
        prefix: "onInitialized",
        detail: "A function used in JavaScript frameworks to save the UI component instance.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxMenu/Configuration/#onInitialized"
    },
    {
        prefix: "onItemClick",
        detail: "A function that is executed when a collection item is clicked or tapped.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxMenu/Configuration/#onItemClick"
    },
    {
        prefix: "onItemContextMenu",
        detail: "A function that is executed when a collection item is right-clicked or pressed.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxMenu/Configuration/#onItemContextMenu"
    },
    {
        prefix: "onItemRendered",
        detail: "A function that is executed after a collection item is rendered.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxMenu/Configuration/#onItemRendered"
    },
    {
        prefix: "onOptionChanged",
        detail: "A function that is executed after a UI component property is changed.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxMenu/Configuration/#onOptionChanged"
    },
    {
        prefix: "onSelectionChanged",
        detail: "A function that is executed when a collection item is selected or selection is canceled.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxMenu/Configuration/#onSelectionChanged"
    },
    {
        prefix: "onSubmenuHidden",
        detail: "A function that is executed after a submenu is hidden.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxMenu/Configuration/#onSubmenuHidden"
    },
    {
        prefix: "onSubmenuHiding",
        detail: "A function that is executed before a submenu is hidden.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxMenu/Configuration/#onSubmenuHiding"
    },
    {
        prefix: "onSubmenuShowing",
        detail: "A function that is executed before a submenu is displayed.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxMenu/Configuration/#onSubmenuShowing"
    },
    {
        prefix: "onSubmenuShown",
        detail: "A function that is executed after a submenu is displayed.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxMenu/Configuration/#onSubmenuShown"
    },
    {
        prefix: "orientation",
        detail: "Specifies whether the menu has horizontal or vertical orientation.",
        document: {
            type: "String",
            defaultValue: "'horizontal'",
            description: "",
            note: "",
            acceptValues: ['horizontal', 'vertical'],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxMenu/Configuration/#orientation"
    },
    {
        prefix: "rtlEnabled",
        detail: "Switches the UI component to a right-to-left representation.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxMenu/Configuration/#rtlEnabled"
    },
    {
        prefix: "selectByClick",
        detail: "Specifies whether or not an item becomes selected if a user clicks it.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxMenu/Configuration/#selectByClick"
    },
    {
        prefix: "selectedExpr",
        detail: "Specifies the name of the data source item field whose value defines whether or not the corresponding UI component items is selected.",
        document: {
            type: "String | Function",
            defaultValue: "'selected'",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxMenu/Configuration/#selectedExpr"
    },
    {
        prefix: "selectedItem",
        detail: "The selected item object.",
        document: {
            type: "any",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxMenu/Configuration/#selectedItem"
    },
    {
        prefix: "selectionMode",
        detail: "Specifies the selection mode supported by the menu.",
        document: {
            type: "String",
            defaultValue: "none",
            description: "",
            note: "",
            acceptValues: ['none', 'single'],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxMenu/Configuration/#selectionMode"
    },
    {
        prefix: "showFirstSubmenuMode",
        detail: "Specifies properties for showing and hiding the first level submenu.",
        document: {
            type: "Object | String",
            defaultValue: "{ name: 'onClick', delay: { show: 50, hide: 300 } }",
            description: "",
            note: "",
            acceptValues: ['onClick', 'onHover'],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxMenu/Configuration/showFirstSubmenuMode/"
    },
    {
        prefix: "showSubmenuMode",
        detail: "Specifies properties of submenu showing and hiding.",
        document: {
            type: "Object | String",
            defaultValue: "{ name: 'onHover', delay: { show: 50, hide: 300 } }",
            description: "",
            note: "",
            acceptValues: ['onClick', 'onHover'],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxMenu/Configuration/showSubmenuMode/"
    },
    {
        prefix: "submenuDirection",
        detail: "Specifies the direction at which the submenus are displayed.",
        document: {
            type: "String",
            defaultValue: "'auto'",
            description: "",
            note: "",
            acceptValues: ['auto', 'leftOrTop', 'rightOrBottom'],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxMenu/Configuration/#submenuDirection"
    },
    {
        prefix: "tabIndex",
        detail: "Specifies the number of the element when the Tab key is used for navigating.",
        document: {
            type: "Number",
            defaultValue: "0",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxMenu/Configuration/#tabIndex"
    },
    {
        prefix: "visible",
        detail: "Specifies whether the UI component is visible.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxMenu/Configuration/#visible"
    },
    {
        prefix: "width",
        detail: "Specifies the UI component's width.",
        document: {
            type: "Number | String | Function",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxMenu/Configuration/#width"
    },

]