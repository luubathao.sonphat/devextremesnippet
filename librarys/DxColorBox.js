
module.exports = [
    {
        prefix: "acceptCustomValue",
        detail: "Specifies whether or not the UI component allows an end-user to enter a custom value.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxColorBox/Configuration/#acceptCustomValue"
    },
    {
        prefix: "accessKey",
        detail: "Specifies the shortcut key that sets focus on the UI component.",
        document: {
            type: "String",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxColorBox/Configuration/#accessKey"
    },
    {
        prefix: "activeStateEnabled",
        detail: "Specifies whether the UI component changes its state as a result of user interaction.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxColorBox/Configuration/#activeStateEnabled"
    },
    {
        prefix: "applyButtonText",
        detail: "Specifies the text displayed on the button that applies changes and closes the drop-down editor.",
        document: {
            type: "String",
            defaultValue: "'OK'",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxColorBox/Configuration/#applyButtonText"
    },
    {
        prefix: "applyValueMode",
        detail: "Specifies the way an end-user applies the selected value.",
        document: {
            type: "String",
            defaultValue: "'useButtons'",
            description: "",
            note: "",
            acceptValues: ['instantly', 'useButtons'],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxColorBox/Configuration/#applyValueMode"
    },
    {
        prefix: "buttons",
        detail: "Allows you to add custom buttons to the input text field.",
        document: {
            type: "Array<String | Object>",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: ['clear', 'dropDown'],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxColorBox/Configuration/#buttons"
    },
    {
        prefix: "cancelButtonText",
        detail: "Specifies the text displayed on the button that cancels changes and closes the drop-down editor.",
        document: {
            type: "String",
            defaultValue: "'Cancel'",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxColorBox/Configuration/#cancelButtonText"
    },
    {
        prefix: "deferRendering",
        detail: "Specifies whether to render the drop-down field's content when it is displayed. If false, the content is rendered immediately.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxColorBox/Configuration/#deferRendering"
    },
    {
        prefix: "disabled",
        detail: "Specifies whether the UI component responds to user interaction.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxColorBox/Configuration/#disabled"
    },
    {
        prefix: "dropDownButtonComponent",
        detail: "An alias for the dropDownButtonTemplate property specified in React. Accepts a custom component. Refer to Using a Custom Component for more information.",
        document: {
            type: "''",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxColorBox/Configuration/#dropDownButtonComponent"
    },
    {
        prefix: "dropDownButtonRender",
        detail: "An alias for the dropDownButtonTemplate property specified in React. Accepts a rendering function. Refer to Using a Rendering Function for more information.",
        document: {
            type: "''",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxColorBox/Configuration/#dropDownButtonRender"
    },
    {
        prefix: "dropDownButtonTemplate",
        detail: "Specifies a custom template for the drop-down button.",
        document: {
            type: "template",
            defaultValue: "'dropDownButton'",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxColorBox/Configuration/#dropDownButtonTemplate"
    },
    {
        prefix: "dropDownOptions",
        detail: "Configures the drop-down field which holds the content.",
        document: {
            type: "Popup Configuration",
            defaultValue: "{}",
            description: "",
            note: "In Angular and Vue, the nested component that configures the dropDownOptions property does not support event bindings and two-way property bindings",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxColorBox/Configuration/#dropDownOptions"
    },
    {
        prefix: "editAlphaChannel",
        detail: "Specifies whether or not the UI component value includes the alpha channel component.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxColorBox/Configuration/#editAlphaChannel"
    },
    {
        prefix: "elementAttr",
        detail: "Specifies the global attributes to be attached to the UI component's container element.",
        document: {
            type: "Object",
            defaultValue: "{}",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxColorBox/Configuration/#elementAttr"
    },
    {
        prefix: "fieldComponent",
        detail: "An alias for the fieldTemplate property specified in React. Accepts a custom component. Refer to Using a Custom Component for more information.",
        document: {
            type: "''",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxColorBox/Configuration/#fieldComponent"
    },
    {
        prefix: "fieldRender",
        detail: "An alias for the fieldTemplate property specified in React. Accepts a rendering function. Refer to Using a Rendering Function for more information.",
        document: {
            type: "''",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxColorBox/Configuration/#fieldRender"
    },
    {
        prefix: "fieldTemplate",
        detail: "Specifies a custom template for the input field. Must contain the TextBox UI component.",
        document: {
            type: "template",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxColorBox/Configuration/#fieldTemplate"
    },
    {
        prefix: "focusStateEnabled",
        detail: "Specifies whether the UI component can be focused using keyboard navigation.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxColorBox/Configuration/#focusStateEnabled"
    },
    {
        prefix: "height",
        detail: "Specifies the UI component's height.",
        document: {
            type: "Number | String | Function",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxColorBox/Configuration/#height"
    },
    {
        prefix: "hint",
        detail: "Specifies text for a hint that appears when a user pauses on the UI component.",
        document: {
            type: "String",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxColorBox/Configuration/#hint"
    },
    {
        prefix: "hoverStateEnabled",
        detail: "Specifies whether the UI component changes its state when a user pauses on it.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxColorBox/Configuration/#hoverStateEnabled"
    },
    {
        prefix: "inputAttr",
        detail: "Specifies the attributes to be passed on to the underlying HTML element.",
        document: {
            type: "any",
            defaultValue: "{}",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxColorBox/Configuration/#inputAttr"
    },
    {
        prefix: "isValid",
        detail: "Specifies or indicates whether the editor's value is valid.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "When you use async rules, isValid is true if the status is 'pending' or 'valid'",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxColorBox/Configuration/#isValid"
    },
    {
        prefix: "keyStep",
        detail: "Specifies the size of a step by which a handle is moved using a keyboard shortcut.",
        document: {
            type: "Number",
            defaultValue: "1",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxColorBox/Configuration/#keyStep"
    },
    {
        prefix: "label",
        detail: "Specifies a text string used to annotate the editor's value.",
        document: {
            type: "String",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxColorBox/Configuration/#label"
    },
    {
        prefix: "labelMode",
        detail: "Specifies the label's display mode.",
        document: {
            type: "String",
            defaultValue: "'static', 'floating' (Material)",
            description: "",
            note: "If autofill is enabled in the browser, we do not recommend that you use 'floating' mode. The autofill values will overlap the label when it is displayed as a placeholder. Use 'static' mode instead",
            acceptValues: ['static', 'floating', 'hidden'],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxColorBox/Configuration/#labelMode"
    },
    {
        prefix: "name",
        detail: "The value to be assigned to the name attribute of the underlying HTML element.",
        document: {
            type: "String",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxColorBox/Configuration/#name"
    },
    {
        prefix: "onChange",
        detail: "A function that is executed when the UI component loses focus after the text field's content was changed using the keyboard.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxColorBox/Configuration/#onChange"
    },
    {
        prefix: "onClosed",
        detail: "A function that is executed once the drop-down editor is closed.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxColorBox/Configuration/#onClosed"
    },
    {
        prefix: "onCopy",
        detail: "A function that is executed when the UI component's input has been copied.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxColorBox/Configuration/#onCopy"
    },
    {
        prefix: "onCut",
        detail: "A function that is executed when the UI component's input has been cut.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxColorBox/Configuration/#onCut"
    },
    {
        prefix: "onDisposing",
        detail: "A function that is executed before the UI component is disposed of.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxColorBox/Configuration/#onDisposing"
    },
    {
        prefix: "onEnterKey",
        detail: "A function that is executed when the Enter key has been pressed while the UI component is focused.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "This function is executed after the onKeyUp and onKeyDown functions",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxColorBox/Configuration/#onEnterKey"
    },
    {
        prefix: "onFocusIn",
        detail: "A function that is executed when the UI component gets focus.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxColorBox/Configuration/#onFocusIn"
    },
    {
        prefix: "onFocusOut",
        detail: "A function that is executed when the UI component loses focus.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxColorBox/Configuration/#onFocusOut"
    },
    {
        prefix: "onInitialized",
        detail: "A function used in JavaScript frameworks to save the UI component instance.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxColorBox/Configuration/#onInitialized"
    },
    {
        prefix: "onInput",
        detail: "A function that is executed each time the UI component's input is changed while the UI component is focused.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxColorBox/Configuration/#onInput"
    },
    {
        prefix: "onKeyDown",
        detail: "A function that is executed when a user is pressing a key on the keyboard.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxColorBox/Configuration/#onKeyDown"
    },
    {
        prefix: "onKeyUp",
        detail: "A function that is executed when a user releases a key on the keyboard.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxColorBox/Configuration/#onKeyUp"
    },
    {
        prefix: "onOpened",
        detail: "A function that is executed once the drop-down editor is opened.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxColorBox/Configuration/#onOpened"
    },
    {
        prefix: "onOptionChanged",
        detail: "A function that is executed after a UI component property is changed.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxColorBox/Configuration/#onOptionChanged"
    },
    {
        prefix: "onPaste",
        detail: "A function that is executed when the UI component's input has been pasted.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxColorBox/Configuration/#onPaste"
    },
    {
        prefix: "onValueChanged",
        detail: "A function that is executed after the UI component's value is changed.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxColorBox/Configuration/#onValueChanged"
    },
    {
        prefix: "opened",
        detail: "Specifies whether or not the drop-down editor is displayed.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxColorBox/Configuration/#opened"
    },
    {
        prefix: "openOnFieldClick",
        detail: "Specifies whether a user can open the drop-down list by clicking a text field.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxColorBox/Configuration/#openOnFieldClick"
    },
    {
        prefix: "placeholder",
        detail: "Specifies a text string displayed when the editor's value is empty.",
        document: {
            type: "String",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxColorBox/Configuration/#placeholder"
    },
    {
        prefix: "readOnly",
        detail: "Specifies whether the editor is read-only.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxColorBox/Configuration/#readOnly"
    },
    {
        prefix: "rtlEnabled",
        detail: "Switches the UI component to a right-to-left representation.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxColorBox/Configuration/#rtlEnabled"
    },
    {
        prefix: "showClearButton",
        detail: "Specifies whether to display the Clear button in the UI component.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxColorBox/Configuration/#showClearButton"
    },
    {
        prefix: "showDropDownButton",
        detail: "Specifies whether the drop-down button is visible.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxColorBox/Configuration/#showDropDownButton"
    },
    {
        prefix: "stylingMode",
        detail: "Specifies how the UI component's text field is styled.",
        document: {
            type: "String",
            defaultValue: "'outlined', 'filled' (Material)",
            description: "",
            note: "",
            acceptValues: ['outlined', 'underlined', 'filled'],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxColorBox/Configuration/#stylingMode"
    },
    {
        prefix: "tabIndex",
        detail: "Specifies the number of the element when the Tab key is used for navigating.",
        document: {
            type: "Number",
            defaultValue: "0",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxColorBox/Configuration/#tabIndex"
    },
    {
        prefix: "text",
        detail: "The read-only property that holds the text displayed by the UI component input element.",
        document: {
            type: "String",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxColorBox/Configuration/#text"
    },
    {
        prefix: "validationError",
        detail: "Information on the broken validation rule. Contains the first item from the validationErrors array.",
        document: {
            type: "any",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxColorBox/Configuration/#validationError"
    },
    {
        prefix: "validationErrors",
        detail: "An array of the validation rules that failed.",
        document: {
            type: "Array<any>",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxColorBox/Configuration/#validationErrors"
    },
    {
        prefix: "validationMessageMode",
        detail: "Specifies how the message about the validation rules that are not satisfied by this editor's value is displayed.",
        document: {
            type: "String",
            defaultValue: "'auto'",
            description: "",
            note: "",
            acceptValues: ['always', 'auto'],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxColorBox/Configuration/#validationMessageMode"
    },
    {
        prefix: "validationStatus",
        detail: "Indicates or specifies the current validation status.",
        document: {
            type: "String",
            defaultValue: "'valid'",
            description: "",
            note: "",
            acceptValues: ['valid', 'invalid', 'pending'],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxColorBox/Configuration/#validationStatus"
    },
    {
        prefix: "value",
        detail: "Specifies the currently selected value.",
        document: {
            type: "String",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxColorBox/Configuration/#value"
    },
    {
        prefix: "visible",
        detail: "Specifies whether the UI component is visible.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxColorBox/Configuration/#visible"
    },
    {
        prefix: "width",
        detail: "Specifies the UI component's width.",
        document: {
            type: "Number | String | Function",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxColorBox/Configuration/#width"
    },

]