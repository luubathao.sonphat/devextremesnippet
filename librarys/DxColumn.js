
module.exports = [
    {
        prefix: "alignment",
        detail: "Aligns the content of the column.",
        document: {
            type: "String",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: ['undefined', 'center', 'left', 'right']
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/columns/#alignment"
    },
    {
        prefix: "allowEditing",
        detail: "Specifies whether a user can edit values in the column at runtime. By default, inherits the value of the editing.allowUpdating property.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "If values in the column are calculated customarily using the calculateCellValue property, they cannot be edited at runtime.",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/columns/#allowEditing"
    },
    {
        prefix: "allowExporting",
        detail: "Specifies whether data from this column should be exported. Applies only if the column is visible.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/columns/#allowExporting"
    },
    {
        prefix: "allowFiltering",
        detail: "Specifies whether data can be filtered by this column. Applies only if filterRow.visible is true.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/columns/#allowFiltering"
    },
    {
        prefix: "allowFixing",
        detail: "Specifies whether a user can fix the column at runtime. Applies only if columnFixing.enabled is true.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/columns/#allowFixing"
    },
    {
        prefix: "allowGrouping",
        detail: "Specifies whether the user can group data by values of this column. Applies only when grouping is enabled.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "In a column with calculated values, this property is set to false by default.",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/columns/#allowGrouping"
    },
    {
        prefix: "allowHeaderFiltering",
        detail: "Specifies whether the header filter can be used to filter data by this column. Applies only if headerFilter.visible is true. By default, inherits the value of the allowFiltering property.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/columns/#allowHeaderFiltering"
    },
    {
        prefix: "allowHiding",
        detail: "Specifies whether a user can hide the column using the column chooser at runtime. Applies only if columnChooser.enabled is true.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "Fixed columns ignore the hidingPriority and allowHiding properties.",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/columns/#allowHiding"
    },
    {
        prefix: "allowReordering",
        detail: "Specifies whether this column can be used in column reordering at runtime. Applies only if allowColumnReordering is true.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/columns/#allowReordering"
    },
    {
        prefix: "allowResizing",
        detail: "Specifies whether a user can resize the column at runtime. Applies only if allowColumnResizing is true.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/columns/#allowResizing"
    },
    {
        prefix: "allowSearch",
        detail: "Specifies whether this column can be searched. Applies only if searchPanel.visible is true. Inherits the value of the allowFiltering property by default.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/columns/#allowSearch"
    },
    {
        prefix: "allowSorting",
        detail: "Specifies whether a user can sort rows by this column at runtime. Applies only if sorting.mode differs from 'none'.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "In a column with calculated values, this property is set to false by default.",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/columns/#allowSorting"
    },
    {
        prefix: "autoExpandGroup",
        detail: "Specifies whether groups appear expanded or not when records are grouped by a specific column. Setting this property makes sense only when grouping is allowed for this column.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/columns/#autoExpandGroup"
    },
    {
        prefix: "buttons",
        detail: "Allows you to customize buttons in the edit column or create a custom command column. Applies only if the column's type is 'buttons'.",
        document: {
            type: "Array<String | Column Button>",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: ['cancel', 'delete', 'edit', 'save', 'undelete']
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/columns/buttons/"
    },
    {
        prefix: "calculateCellValue",
        detail: "Calculates custom cell values. Use this function to create an unbound data column.",
        document: {
            type: "Function",
            defaultValue: "",
            description: "",
            note: "The this keyword refers to the column's configuration.This function is called multiple times for every record: when the record is rendered, when sorting or filtering is applied, and when summaries are computed. It is recommend that you keep calculations inside this function as simple as possible to avoid hindering UI component performance.",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/columns/#calculateCellValue"
    },
    {
        prefix: "calculateDisplayValue",
        detail: "Calculates custom display values for column cells. Requires specifying the dataField or calculateCellValue property. Used in lookup optimization.",
        document: {
            type: "String | Function",
            defaultValue: "",
            description: "",
            note: "The this keyword refers to the column's configuration.",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/columns/#calculateDisplayValue"
    },
    {
        prefix: "calculateFilterExpression",
        detail: "Specifies the column's custom rules to filter data.",
        document: {
            type: "Function",
            defaultValue: "",
            description: "",
            note: "The this keyword refers to the column's configuration.If you specify a custom header filter data source, a header filter item's value field can contain a single value (for example, 0) or a filter expression. If it is a filter expression, the calculateFilterExpression function does not apply.",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/columns/#calculateFilterExpression"
    },
    {
        prefix: "calculateGroupValue",
        detail: "Sets custom column values used to group grid records.",
        document: {
            type: "String | Function",
            defaultValue: "",
            description: "",
            note: "The this keyword refers to the column's configuration.",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/columns/#calculateGroupValue"
    },
    {
        prefix: "calculateSortValue",
        detail: "Calculates custom values used to sort this column.",
        document: {
            type: "String | Function",
            defaultValue: "",
            description: "",
            note: "The this keyword refers to the column's configuration.calculateSortValue does not affect group rows. To sort them, implement calculateGroupValue in addition to calculateSortValue. You should also define the groupCellTemplate to apply a custom template for group rows. Refer to the following GitHub repository for an example: DataGrid - How to apply custom sorting to a grouped column.",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/columns/#calculateSortValue"
    },
    {
        prefix: "caption",
        detail: "Specifies a caption for the column.",
        document: {
            type: "String",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/columns/#caption"
    },
    {
        prefix: "cellComponent",
        detail: "An alias for the cellTemplate property specified in React. Accepts a custom component. Refer to Using a Custom Component for more information.",
        document: {
            type: "",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/columns/#cellComponent"
    },
    {
        prefix: "cellRender",
        detail: "An alias for the cellTemplate property specified in React. Accepts a rendering function. Refer to Using a Rendering Function for more information.",
        document: {
            type: "",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/columns/#cellRender"
    },
    {
        prefix: "cellTemplate",
        detail: "Specifies a custom template for data cells.",
        document: {
            type: "template",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/columns/#cellTemplate"
    },
    {
        prefix: "columns",
        detail: "An array of grid columns.",
        document: {
            type: "Array<DataGrid Column | String>",
            defaultValue: "undefined",
            description: "",
            note: "There is an exception though: nested columns cannot be fixed alone, therefore specifying the fixed and fixedPosition properties for them is useless. However, the whole band column can be fixed as usual.",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/columns/columns/"
    },
    {
        prefix: "cssClass",
        detail: "Specifies a CSS class to be applied to the column.",
        document: {
            type: "String",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/columns/#cssClass"
    },
    {
        prefix: "customizeText",
        detail: "Customizes the text displayed in column cells.",
        document: {
            type: "Function",
            defaultValue: "",
            description: "",
            note: "The specified text is not used to sort, filter, and group data or calculate summaries. If it should be, specify the calculateCellValue function instead.The this keyword refers to the column's configuration.",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/columns/#customizeText"
    },
    {
        prefix: "dataField",
        detail: "Binds the column to a field of the dataSource.",
        document: {
            type: "String",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/columns/#dataField"
    },
    {
        prefix: "dataType",
        detail: "Casts column values to a specific data type.",
        document: {
            type: "String",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: ['string', 'number', 'date', 'boolean', 'object', 'datetime']
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/columns/#dataType"
    },
    {
        prefix: "editCellComponent",
        detail: "An alias for the editCellTemplate property specified in React. Accepts a custom component. Refer to Using a Custom Component for more information.",
        document: {
            type: "",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/columns/#editCellComponent"
    },
    {
        prefix: "editCellRender",
        detail: "An alias for the editCellTemplate property specified in React. Accepts a rendering function. Refer to Using a Rendering Function for more information.",
        document: {
            type: "",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/columns/#editCellRender"
    },
    {
        prefix: "editCellTemplate",
        detail: "Specifies a custom template for data cells in editing state.",
        document: {
            type: "template",
            defaultValue: "",
            description: "",
            note: "If you implement two-way data binding in your template, set twoWayBindingEnabled to false to disable this feature's default implementation.If you specify validationRules, the editCellTemplate must contain a DevExtreme editor to which the DataGrid will apply these rules.",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/columns/#editCellTemplate"
    },
    {
        prefix: "editorOptions",
        detail: "Configures the default UI component used for editing and filtering in the filter row.",
        document: {
            type: "any",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/columns/#editorOptions"
    },
    {
        prefix: "encodeHtml",
        detail: "Specifies whether HTML tags are displayed as plain text or applied to the values of the column.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/columns/#encodeHtml"
    },
    {
        prefix: "falseText",
        detail: "In a boolean column, replaces all false items with a specified text. Applies only if showEditorAlways property is false.",
        document: {
            type: "String",
            defaultValue: "'false'",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/columns/#falseText"
    },
    {
        prefix: "filterOperations",
        detail: "Specifies available filter operations. Applies if allowFiltering is true and the filterRow and/or filterPanel are visible.",
        document: {
            type: "Array<String>",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: ['=', '<>', '<', '<=', '>', '>=', 'contains', 'endswith', 'isblank', 'isnotblank', 'notcontains', 'startswith', 'between', 'anyof', 'noneof']
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/columns/#filterOperations"
    },
    {
        prefix: "filterType",
        detail: "Specifies whether a user changes the current filter by including (selecting) or excluding (clearing the selection of) values. Applies only if headerFilter.visible and allowHeaderFiltering are true.",
        document: {
            type: "String",
            defaultValue: "'include'",
            description: "",
            note: "",
            acceptValues: ['exclude', 'include']
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/columns/#filterType"
    },
    {
        prefix: "filterValue",
        detail: "Specifies the column's filter value displayed in the filter row.",
        document: {
            type: "any",
            defaultValue: "undefined",
            description: "",
            note: "The filter row and header filter can work simultaneously. If you specify the filterValue property to set up the filter row, it limits the filter values for the header filter and vice versa: if you specify the filterValues property to set up the header filter, it limits the filter row's filter values.",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/columns/#filterValue"
    },
    {
        prefix: "filterValues",
        detail: "Specifies values selected in the column's header filter.",
        document: {
            type: "Array<any>",
            defaultValue: "undefined",
            description: "",
            note: "The filter row and header filter can work simultaneously. If you specify the filterValue property to set up the filter row, it limits the filter values for the header filter and vice versa: if you specify the filterValues property to set up the header filter, it limits the filter row's filter values.",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/columns/#filterValues"
    },
    {
        prefix: "fixed",
        detail: "Fixes the column.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/columns/#fixed"
    },
    {
        prefix: "fixedPosition",
        detail: "Specifies the UI component's edge to which the column is fixed. Applies only if columns[].fixed is true.",
        document: {
            type: "String",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: ['left', 'right']
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/columns/#fixedPosition"
    },
    {
        prefix: "format",
        detail: "Formats a value before it is displayed in a column cell.",
        document: {
            type: "Format",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/columns/#format"
    },
    {
        prefix: "formItem",
        detail: "Configures the form item that the column produces in the editing state. Applies only if editing.mode is 'form' or 'popup'.",
        document: {
            type: "Simple Form Item",
            defaultValue: "",
            description: "",
            note: "The formItem object does not allow you to specify a template. Use the column's editCellTemplate instead.",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/columns/#formItem"
    },
    {
        prefix: "groupCellComponent",
        detail: "An alias for the groupCellTemplate property specified in React. Accepts a custom component. Refer to Using a Custom Component for more information.",
        document: {
            type: "",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/columns/#groupCellComponent"
    },
    {
        prefix: "groupCellRender",
        detail: "An alias for the groupCellTemplate property specified in React. Accepts a rendering function. Refer to Using a Rendering Function for more information.",
        document: {
            type: "",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/columns/#groupCellRender"
    },
    {
        prefix: "groupCellTemplate",
        detail: "Specifies a custom template for group cells (group rows).",
        document: {
            type: "template",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/columns/#groupCellTemplate"
    },
    {
        prefix: "groupIndex",
        detail: "Specifies the index of a column when grid records are grouped by the values of this column.",
        document: {
            type: "Number",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/columns/#groupIndex"
    },
    {
        prefix: "headerCellComponent",
        detail: "An alias for the headerCellTemplate property specified in React. Accepts a custom component. Refer to Using a Custom Component for more information.",
        document: {
            type: "",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/columns/#headerCellComponent"
    },
    {
        prefix: "headerCellRender",
        detail: "An alias for the headerCellTemplate property specified in React. Accepts a rendering function. Refer to Using a Rendering Function for more information.",
        document: {
            type: "",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/columns/#headerCellRender"
    },
    {
        prefix: "headerCellTemplate",
        detail: "Specifies a custom template for column headers.",
        document: {
            type: "template",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/columns/#headerCellTemplate"
    },
    {
        prefix: "headerFilter",
        detail: "Specifies data settings for the header filter.",
        document: {
            type: "Object",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/columns/headerFilter/"
    },
    {
        prefix: "hidingPriority",
        detail: "Specifies the order in which columns are hidden when the UI component adapts to the screen or container size. Ignored if allowColumnResizing is true and columnResizingMode is 'widget'.",
        document: {
            type: "Number",
            defaultValue: "undefined",
            description: "",
            note: "Specifying hidingPriority for at least one column enables the column hiding feature and cancels the default hiding priorities.Fixed columns ignore the hidingPriority and allowHiding properties.",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/columns/#hidingPriority"
    },
    {
        prefix: "isBand",
        detail: "Specifies whether the column organizes other columns into bands.",
        document: {
            type: "Boolean",
            defaultValue: "undefined",
            description: "",
            note: "Band columns cannot nest command columns.",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/columns/#isBand"
    },
    {
        prefix: "lookup",
        detail: "Specifies properties of a lookup column.",
        document: {
            type: "Object",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/columns/lookup/"
    },
    {
        prefix: "minWidth",
        detail: "Specifies the minimum width of the column.",
        document: {
            type: "Number",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/columns/#minWidth"
    },
    {
        prefix: "name",
        detail: "Specifies the column's unique identifier. If not set in code, this value is inherited from the dataField.",
        document: {
            type: "String",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/columns/#name"
    },
    {
        prefix: "ownerBand",
        detail: "Specifies the band column that owns the current column. Accepts the index of the band column in the columns array.",
        document: {
            type: "Number",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/columns/#ownerBand"
    },
    {
        prefix: "renderAsync",
        detail: "Specifies whether to render the column after other columns and elements. Use if column cells have a complex template. Requires the width property specified.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/columns/#renderAsync"
    },
    {
        prefix: "selectedFilterOperation",
        detail: "Specifies a filter operation that applies when users use the filter row to filter the column.",
        document: {
            type: "String",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: ['<', '<=', '<>', '=', '>', '>=', 'between', 'contains', 'endswith', 'notcontains', 'startswith']
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/columns/#selectedFilterOperation"
    },
    {
        prefix: "setCellValue",
        detail: "Specifies a function to be invoked after the user has edited a cell value, but before it will be saved in the data source.",
        document: {
            type: "Function",
            defaultValue: "",
            description: "",
            note: "The this keyword refers to the column's configuration.",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/columns/#setCellValue"
    },
    {
        prefix: "showEditorAlways",
        detail: "Specifies whether the column displays its values in editors.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "This property has the following specifics:The default value of this property depends on the column's dataType. For Boolean columns, the default value is true; for columns of other types - false.The editCellTemplate has higher priority over the cellTemplate if the showEditorAlways property value is true. Relevant for all data types except Boolean.The cellInfo.setValue function does not work when the showEditorAlways property value is true but you do not switch the component to edit mode.",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/columns/#showEditorAlways"
    },
    {
        prefix: "showInColumnChooser",
        detail: "Specifies whether the column chooser can contain the column header.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/columns/#showInColumnChooser"
    },
    {
        prefix: "showWhenGrouped",
        detail: "Specifies whether or not to display the column when grid records are grouped by it.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/columns/#showWhenGrouped"
    },
    {
        prefix: "sortIndex",
        detail: "Specifies the index according to which columns participate in sorting.",
        document: {
            type: "Number",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/columns/#sortIndex"
    },
    {
        prefix: "sortingMethod",
        detail: "Specifies a custom comparison function for sorting. Applies only when sorting is performed on the client.",
        document: {
            type: "Function",
            defaultValue: "undefined",
            description: "",
            note: "The sortingMethod's value1 and value2 are the values returned from the calculateSortValue function if the latter is specified.The this keyword refers to the column's configuration.",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/columns/#sortingMethod"
    },
    {
        prefix: "sortOrder",
        detail: "Specifies the sort order of column values.",
        document: {
            type: "String",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: ['undefined', 'asc', 'desc']
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/columns/#sortOrder"
    },
    {
        prefix: "trueText",
        detail: "In a boolean column, replaces all true items with a specified text. Applies only if showEditorAlways property is false.",
        document: {
            type: "String",
            defaultValue: "'true'",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/columns/#trueText"
    },
    {
        prefix: "type",
        detail: "Specifies the command column that this object customizes.",
        document: {
            type: "String",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: ['adaptive', 'buttons', 'detailExpand', 'groupExpand', 'selection', 'drag']
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/columns/#type"
    },
    {
        prefix: "validationRules",
        detail: "Specifies validation rules to be checked when cell values are updated.",
        document: {
            type: "Array<RequiredRule | NumericRule | RangeRule | StringLengthRule | CustomRule | CompareRule | PatternRule | EmailRule | AsyncRule>",
            defaultValue: "",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/columns/#validationRules"
    },
    {
        prefix: "visible",
        detail: "Specifies whether the column is visible, that is, occupies space in the table.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/columns/#visible"
    },
    {
        prefix: "visibleIndex",
        detail: "Specifies the position of the column regarding other columns in the resulting UI component.",
        document: {
            type: "Number",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/columns/#visibleIndex"
    },
    {
        prefix: "width",
        detail: "Specifies the column's width in pixels or as a percentage. Ignored if it is less than minWidth.",
        document: {
            type: "Number | String",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: []
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/Configuration/columns/#width"
    },

]