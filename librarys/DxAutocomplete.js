
module.exports = [
    {
        prefix: "accessKey",
        detail: "Specifies the shortcut key that sets focus on the UI component.",
        document: {
            type: "String",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxAutocomplete/Configuration/#accessKey"
    },
    {
        prefix: "activeStateEnabled",
        detail: "Specifies whether the UI component changes its state as a result of user interaction.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxAutocomplete/Configuration/#activeStateEnabled"
    },
    {
        prefix: "buttons",
        detail: "Allows you to add custom buttons to the input text field.",
        document: {
            type: "Array<String | Object>",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: ['clear', 'dropDown'],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxAutocomplete/Configuration/buttons/"
    },
    {
        prefix: "dataSource",
        detail: "Binds the UI component to data.",
        document: {
            type: "Store | DataSource | DataSource Configuration | String | Array<CollectionWidgetItem | any>",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxAutocomplete/Configuration/#dataSource"
    },
    {
        prefix: "deferRendering",
        detail: "Specifies whether to render the drop-down field's content when it is displayed. If false, the content is rendered immediately.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxAutocomplete/Configuration/#deferRendering"
    },
    {
        prefix: "disabled",
        detail: "Specifies whether the UI component responds to user interaction.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxAutocomplete/Configuration/#disabled"
    },
    {
        prefix: "displayValue",
        detail: "Returns the value currently displayed by the UI component.",
        document: {
            type: "String",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxAutocomplete/Configuration/#displayValue"
    },
    {
        prefix: "dropDownButtonComponent",
        detail: "An alias for the dropDownButtonTemplate property specified in React. Accepts a custom component. Refer to Using a Custom Component for more information.",
        document: {
            type: "''",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxAutocomplete/Configuration/#dropDownButtonComponent"
    },
    {
        prefix: "dropDownButtonRender",
        detail: "An alias for the dropDownButtonTemplate property specified in React. Accepts a rendering function. Refer to Using a Rendering Function for more information.",
        document: {
            type: "''",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxAutocomplete/Configuration/#dropDownButtonRender"
    },
    {
        prefix: "dropDownButtonTemplate",
        detail: "Specifies a custom template for the drop-down button.",
        document: {
            type: "template",
            defaultValue: "'dropDownButton'",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxAutocomplete/Configuration/#dropDownButtonTemplate"
    },
    {
        prefix: "dropDownOptions",
        detail: "Configures the drop-down field which holds the content.",
        document: {
            type: "Popup Configuration",
            defaultValue: "{}",
            description: "",
            note: "In Angular and Vue, the nested component that configures the dropDownOptions property does not support event bindings and two-way property bindings.",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxAutocomplete/Configuration/#dropDownOptions"
    },
    {
        prefix: "elementAttr",
        detail: "Specifies the global attributes to be attached to the UI component's container element.",
        document: {
            type: "Object",
            defaultValue: "{}",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxAutocomplete/Configuration/#elementAttr"
    },
    {
        prefix: "focusStateEnabled",
        detail: "Specifies whether the UI component can be focused using keyboard navigation.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxAutocomplete/Configuration/#focusStateEnabled"
    },
    {
        prefix: "groupComponent",
        detail: "An alias for the groupTemplate property specified in React. Accepts a custom component. Refer to Using a Custom Component for more information.",
        document: {
            type: ";",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxAutocomplete/Configuration/#groupComponent"
    },
    {
        prefix: "grouped",
        detail: "Specifies whether data items should be grouped.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "Only one-level grouping is supported",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxAutocomplete/Configuration/#grouped"
    },
    {
        prefix: "groupRender",
        detail: "An alias for the groupTemplate property specified in React. Accepts a rendering function. Refer to Using a Rendering Function for more information.",
        document: {
            type: ";",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxAutocomplete/Configuration/#groupRender"
    },
    {
        prefix: "groupTemplate",
        detail: "Specifies a custom template for group captions.",
        document: {
            type: "template",
            defaultValue: "'group'",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxAutocomplete/Configuration/#groupTemplate"
    },
    {
        prefix: "height",
        detail: "Specifies the UI component's height.",
        document: {
            type: "Number | String | Function",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxAutocomplete/Configuration/#height"
    },
    {
        prefix: "hint",
        detail: "Specifies text for a hint that appears when a user pauses on the UI component.",
        document: {
            type: "String",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxAutocomplete/Configuration/#hint"
    },
    {
        prefix: "hoverStateEnabled",
        detail: "Specifies whether the UI component changes its state when a user pauses on it.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxAutocomplete/Configuration/#hoverStateEnabled"
    },
    {
        prefix: "inputAttr",
        detail: "Specifies the attributes to be passed on to the underlying HTML element.",
        document: {
            type: "any",
            defaultValue: "{}",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxAutocomplete/Configuration/#inputAttr"
    },
    {
        prefix: "isValid",
        detail: "Specifies or indicates whether the editor's value is valid.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "When you use async rules, isValid is true if the status is 'pending' or 'valid'.",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxAutocomplete/Configuration/#isValid"
    },
    {
        prefix: "itemComponent",
        detail: "An alias for the itemTemplate property specified in React. Accepts a custom component. Refer to Using a Custom Component for more information.",
        document: {
            type: "''",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxAutocomplete/Configuration/#itemComponent"
    },
    {
        prefix: "itemRender",
        detail: "An alias for the itemTemplate property specified in React. Accepts a rendering function. Refer to Using a Rendering Function for more information.",
        document: {
            type: "''",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxAutocomplete/Configuration/#itemRender"
    },
    {
        prefix: "items",
        detail: "An array of items displayed by the UI component.",
        document: {
            type: "Array<CollectionWidgetItem | any>",
            defaultValue: "''",
            description: "",
            note: "Do not use the items property if you use dataSource, and vice versa.",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxAutocomplete/Configuration/items/"
    },
    {
        prefix: "itemTemplate",
        detail: "Specifies a custom template for items.",
        document: {
            type: "template",
            defaultValue: "'item'",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxAutocomplete/Configuration/#itemTemplate"
    },
    {
        prefix: "label",
        detail: "Specifies a text string used to annotate the editor's value.",
        document: {
            type: "String",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxAutocomplete/Configuration/#label"
    },
    {
        prefix: "labelMode",
        detail: "Specifies the label's display mode.",
        document: {
            type: "String",
            defaultValue: "'static', 'floating' (Material)",
            description: "",
            note: "If autofill is enabled in the browser, we do not recommend that you use 'floating' mode. The autofill values will overlap the label when it is displayed as a placeholder. Use 'static' mode instead.",
            acceptValues: ['static', 'floating', 'hidden'],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxAutocomplete/Configuration/#labelMode"
    },
    {
        prefix: "maxItemCount",
        detail: "Specifies the maximum count of items displayed by the UI component.",
        document: {
            type: "Number",
            defaultValue: "10",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxAutocomplete/Configuration/#maxItemCount"
    },
    {
        prefix: "maxLength",
        detail: "Specifies the maximum number of characters you can enter into the textbox.",
        document: {
            type: "String | Number",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxAutocomplete/Configuration/#maxLength"
    },
    {
        prefix: "minSearchLength",
        detail: "The minimum number of characters that must be entered into the text box to begin a search.",
        document: {
            type: "Number",
            defaultValue: "1",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxAutocomplete/Configuration/#minSearchLength"
    },
    {
        prefix: "name",
        detail: "The value to be assigned to the name attribute of the underlying HTML element.",
        document: {
            type: "String",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxAutocomplete/Configuration/#name"
    },
    {
        prefix: "onChange",
        detail: "A function that is executed when the UI component loses focus after the text field's content was changed using the keyboard.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxAutocomplete/Configuration/#onChange"
    },
    {
        prefix: "onClosed",
        detail: "A function that is executed once the drop-down editor is closed.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxAutocomplete/Configuration/#onClosed"
    },
    {
        prefix: "onContentReady",
        detail: "A function that is executed when the UI component's content is ready and each time the content is changed.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxAutocomplete/Configuration/#onContentReady"
    },
    {
        prefix: "onCopy",
        detail: "A function that is executed when the UI component's input has been copied.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxAutocomplete/Configuration/#onCopy"
    },
    {
        prefix: "onCut",
        detail: "A function that is executed when the UI component's input has been cut.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxAutocomplete/Configuration/#onCut"
    },
    {
        prefix: "onDisposing",
        detail: "A function that is executed before the UI component is disposed of.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxAutocomplete/Configuration/#onDisposing"
    },
    {
        prefix: "onEnterKey",
        detail: "A function that is executed when the Enter key has been pressed while the UI component is focused.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "This function is executed after the onKeyUp and onKeyDown functions.",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxAutocomplete/Configuration/#onEnterKey"
    },
    {
        prefix: "onFocusIn",
        detail: "A function that is executed when the UI component gets focus.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxAutocomplete/Configuration/#onFocusIn"
    },
    {
        prefix: "onFocusOut",
        detail: "A function that is executed when the UI component loses focus.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxAutocomplete/Configuration/#onFocusOut"
    },
    {
        prefix: "onInitialized",
        detail: "A function used in JavaScript frameworks to save the UI component instance.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxAutocomplete/Configuration/#onInitialized"
    },
    {
        prefix: "onInput",
        detail: "A function that is executed each time the UI component's input is changed while the UI component is focused.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxAutocomplete/Configuration/#onInput"
    },
    {
        prefix: "onItemClick",
        detail: "A function that is executed when a list item is clicked or tapped.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxAutocomplete/Configuration/#onItemClick"
    },
    {
        prefix: "onKeyDown",
        detail: "A function that is executed when a user is pressing a key on the keyboard.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxAutocomplete/Configuration/#onKeyDown"
    },
    {
        prefix: "onKeyUp",
        detail: "A function that is executed when a user releases a key on the keyboard.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxAutocomplete/Configuration/#onKeyUp"
    },
    {
        prefix: "onOpened",
        detail: "A function that is executed once the drop-down editor is opened.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxAutocomplete/Configuration/#onOpened"
    },
    {
        prefix: "onOptionChanged",
        detail: "A function that is executed after a UI component property is changed.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxAutocomplete/Configuration/#onOptionChanged"
    },
    {
        prefix: "onPaste",
        detail: "A function that is executed when the UI component's input has been pasted.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxAutocomplete/Configuration/#onPaste"
    },
    {
        prefix: "onSelectionChanged",
        detail: "A function that is executed when a list item is selected or selection is canceled.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxAutocomplete/Configuration/#onSelectionChanged"
    },
    {
        prefix: "onValueChanged",
        detail: "A function that is executed after the UI component's value is changed.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxAutocomplete/Configuration/#onValueChanged"
    },
    {
        prefix: "opened",
        detail: "Specifies whether or not the drop-down editor is displayed.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxAutocomplete/Configuration/#opened"
    },
    {
        prefix: "openOnFieldClick",
        detail: "Specifies whether a user can open the drop-down list by clicking a text field.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxAutocomplete/Configuration/#openOnFieldClick"
    },
    {
        prefix: "placeholder",
        detail: "Specifies a text string displayed when the editor's value is empty.",
        document: {
            type: "String",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxAutocomplete/Configuration/#placeholder"
    },
    {
        prefix: "readOnly",
        detail: "Specifies whether the editor is read-only.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxAutocomplete/Configuration/#readOnly"
    },
    {
        prefix: "rtlEnabled",
        detail: "Switches the UI component to a right-to-left representation.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxAutocomplete/Configuration/#rtlEnabled"
    },
    {
        prefix: "searchExpr",
        detail: "Specifies the name of a data source item field or an expression whose value is compared to the search criterion.",
        document: {
            type: "getter | Array<getter>",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxAutocomplete/Configuration/#searchExpr"
    },
    {
        prefix: "searchMode",
        detail: "Specifies a comparison operation used to search UI component items.",
        document: {
            type: "String",
            defaultValue: "'contains'",
            description: "",
            note: "",
            acceptValues: ['contains', 'startswith'],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxAutocomplete/Configuration/#searchMode"
    },
    {
        prefix: "searchTimeout",
        detail: "Specifies the time delay, in milliseconds, after the last character has been typed in, before a search is executed.",
        document: {
            type: "Number",
            defaultValue: "500",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxAutocomplete/Configuration/#searchTimeout"
    },
    {
        prefix: "selectedItem",
        detail: "Gets the currently selected item.",
        document: {
            type: "any",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxAutocomplete/Configuration/#selectedItem"
    },
    {
        prefix: "showClearButton",
        detail: "Specifies whether to display the Clear button in the UI component.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxAutocomplete/Configuration/#showClearButton"
    },
    {
        prefix: "showDropDownButton",
        detail: "Specifies whether the drop-down button is visible.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxAutocomplete/Configuration/#showDropDownButton"
    },
    {
        prefix: "spellcheck",
        detail: "Specifies whether or not the UI component checks the inner text for spelling mistakes.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxAutocomplete/Configuration/#spellcheck"
    },
    {
        prefix: "stylingMode",
        detail: "Specifies how the UI component's text field is styled.",
        document: {
            type: "String",
            defaultValue: "'outlined', 'filled' (Material)",
            description: "",
            note: "",
            acceptValues: ['outlined', 'underlined', 'filled'],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxAutocomplete/Configuration/#stylingMode"
    },
    {
        prefix: "tabIndex",
        detail: "Specifies the number of the element when the Tab key is used for navigating.",
        document: {
            type: "Number",
            defaultValue: "0",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxAutocomplete/Configuration/#tabIndex"
    },
    {
        prefix: "text",
        detail: "The read-only property that holds the text displayed by the UI component input element.",
        document: {
            type: "String",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxAutocomplete/Configuration/#text"
    },
    {
        prefix: "useItemTextAsTitle",
        detail: "Specifies whether the Autocomplete uses item's text a title attribute.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxAutocomplete/Configuration/#useItemTextAsTitle"
    },
    {
        prefix: "validationError",
        detail: "Information on the broken validation rule. Contains the first item from the validationErrors array.",
        document: {
            type: "any",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxAutocomplete/Configuration/#validationError"
    },
    {
        prefix: "validationErrors",
        detail: "An array of the validation rules that failed.",
        document: {
            type: "Array<any>",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxAutocomplete/Configuration/#validationErrors"
    },
    {
        prefix: "validationMessageMode",
        detail: "Specifies how the message about the validation rules that are not satisfied by this editor's value is displayed.",
        document: {
            type: "String",
            defaultValue: "'auto'",
            description: "",
            note: "",
            acceptValues: ['always', 'auto'],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxAutocomplete/Configuration/#validationMessageMode"
    },
    {
        prefix: "validationStatus",
        detail: "Indicates or specifies the current validation status.",
        document: {
            type: "String",
            defaultValue: "'valid'",
            description: "",
            note: "",
            acceptValues: ['valid', 'invalid', 'pending'],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxAutocomplete/Configuration/#validationStatus"
    },
    {
        prefix: "value",
        detail: "Specifies the current value displayed by the UI component.",
        document: {
            type: "String",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxAutocomplete/Configuration/#value"
    },
    {
        prefix: "valueChangeEvent",
        detail: "Specifies the DOM events after which the UI component's value should be updated.",
        document: {
            type: "String",
            defaultValue: "'input change keyup'",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxAutocomplete/Configuration/#valueChangeEvent"
    },
    {
        prefix: "valueExpr",
        detail: "Specifies which data field provides unique values to the UI component's value.",
        document: {
            type: "String | Function",
            defaultValue: "'this'",
            description: "",
            note: "You cannot specify valueExpr as a function when the UI component is bound to a remote data source. This is because valueExpr is used in a filter the UI component sends to the server when querying data. Functions with custom logic cannot be serialized for this filter.",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxAutocomplete/Configuration/#valueExpr"
    },
    {
        prefix: "visible",
        detail: "Specifies whether the UI component is visible.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxAutocomplete/Configuration/#visible"
    },
    {
        prefix: "width",
        detail: "Specifies the UI component's width.",
        document: {
            type: "Number | String | Function",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxAutocomplete/Configuration/#width"
    },
    {
        prefix: "wrapItemText",
        detail: "Specifies whether text that exceeds the drop-down list width should be wrapped.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxAutocomplete/Configuration/#wrapItemText"
    },

]