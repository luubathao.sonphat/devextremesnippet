
module.exports = [
    {
        prefix: "accessKey",
        detail: "Specifies the shortcut key that sets focus on the UI component.",
        document: {
            type: "String",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopup/Configuration/#accessKey"
    },
    {
        prefix: "animation",
        detail: "Configures UI component visibility animations. This object contains two fields: show and hide.",
        document: {
            type: "Object",
            defaultValue: "{ show: { type: 'slide', duration: 400, from: { position: { my: 'top', at: 'bottom', of: window } }, to: { position: { my: 'center', at: 'center', of: window } } }, hide: { type: 'slide', duration: 400, from: { position: { my: 'center', at: 'center', of: window } }, to: { position: { my: 'top', at: 'bottom', of: window } } }} (iOS)",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopup/Configuration/animation/"
    },
    {
        prefix: "closeOnOutsideClick",
        detail: "Specifies whether to close the UI component if a user clicks outside it.",
        document: {
            type: "Boolean | Function",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopup/Configuration/#closeOnOutsideClick"
    },
    {
        prefix: "container",
        detail: "Specifies the container in which to render the UI component.",
        document: {
            type: "String | HTMLElement | jQuery",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopup/Configuration/#container"
    },
    {
        prefix: "contentComponent",
        detail: "An alias for the contentTemplate property specified in React. Accepts a custom component. Refer to Using a Custom Component for more information.",
        document: {
            type: "''",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopup/Configuration/#contentComponent"
    },
    {
        prefix: "contentRender",
        detail: "An alias for the contentTemplate property specified in React. Accepts a rendering function. Refer to Using a Rendering Function for more information.",
        document: {
            type: "''",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopup/Configuration/#contentRender"
    },
    {
        prefix: "contentTemplate",
        detail: "Specifies a custom template for the UI component content.",
        document: {
            type: "template",
            defaultValue: "Default Name: 'content'",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopup/Configuration/#contentTemplate"
    },
    {
        prefix: "copyRootClassesToWrapper",
        detail: "Copies your custom CSS classes from the root element to the wrapper element.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopup/Configuration/#copyRootClassesToWrapper"
    },
    {
        prefix: "deferRendering",
        detail: "Specifies whether to render the UI component's content when it is displayed. If false, the content is rendered immediately.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopup/Configuration/#deferRendering"
    },
    {
        prefix: "disabled",
        detail: "Specifies whether the UI component responds to user interaction.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopup/Configuration/#disabled"
    },
    {
        prefix: "dragAndResizeArea",
        detail: "Specifies an element with boundaries within which users can drag and resize the Popup. Ignored if the dragOutsideBoundary property is set to true.",
        document: {
            type: "String | HTMLElement | jQuery",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopup/Configuration/#dragAndResizeArea"
    },
    {
        prefix: "dragEnabled",
        detail: "Specifies whether or not to allow a user to drag the popup window.",
        document: {
            type: "Boolean",
            defaultValue: "false, true (desktop)",
            description: "",
            note: "Dragging is possible only if the 'height: 100 % ' style setting is applied to the html element and 'min- height: 100% ' - to the body element",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopup/Configuration/#dragEnabled"
    },
    {
        prefix: "dragOutsideBoundary",
        detail: "Allows users to drag the Popup within the browser window or beyond the window's borders.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopup/Configuration/#dragOutsideBoundary"
    },
    {
        prefix: "focusStateEnabled",
        detail: "Specifies whether the UI component can be focused using keyboard navigation.",
        document: {
            type: "Boolean",
            defaultValue: "true (desktop)",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopup/Configuration/#focusStateEnabled"
    },
    {
        prefix: "fullScreen",
        detail: "Specifies whether to display the Popup in full-screen mode.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "When fullScreen is true, the UI component always occupies the entire screen and disregards the container value",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopup/Configuration/#fullScreen"
    },
    {
        prefix: "height",
        detail: "Specifies the UI component's height.",
        document: {
            type: "Number | String | Function",
            defaultValue: "'80vh'",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopup/Configuration/#height"
    },
    {
        prefix: "hideOnParentScroll",
        detail: "Specifies whether to hide the Popup when users scroll one of its parent elements.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopup/Configuration/#hideOnParentScroll"
    },
    {
        prefix: "hint",
        detail: "Specifies text for a hint that appears when a user pauses on the UI component.",
        document: {
            type: "String",
            defaultValue: "undefined",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopup/Configuration/#hint"
    },
    {
        prefix: "hoverStateEnabled",
        detail: "Specifies whether the UI component changes its state when a user pauses on it.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopup/Configuration/#hoverStateEnabled"
    },
    {
        prefix: "maxHeight",
        detail: "Specifies the maximum height the UI component can reach while resizing.",
        document: {
            type: "Number | String | Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopup/Configuration/#maxHeight"
    },
    {
        prefix: "maxWidth",
        detail: "Specifies the maximum width the UI component can reach while resizing.",
        document: {
            type: "Number | String | Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopup/Configuration/#maxWidth"
    },
    {
        prefix: "minHeight",
        detail: "Specifies the minimum height the UI component can reach while resizing.",
        document: {
            type: "Number | String | Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopup/Configuration/#minHeight"
    },
    {
        prefix: "minWidth",
        detail: "Specifies the minimum width the UI component can reach while resizing.",
        document: {
            type: "Number | String | Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopup/Configuration/#minWidth"
    },
    {
        prefix: "onContentReady",
        detail: "A function that is executed when the UI component's content is ready and each time the content is changed.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopup/Configuration/#onContentReady"
    },
    {
        prefix: "onDisposing",
        detail: "A function that is executed before the UI component is disposed of.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopup/Configuration/#onDisposing"
    },
    {
        prefix: "onHidden",
        detail: "A function that is executed after the UI component is hidden.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopup/Configuration/#onHidden"
    },
    {
        prefix: "onHiding",
        detail: "A function that is executed before the UI component is hidden.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopup/Configuration/#onHiding"
    },
    {
        prefix: "onInitialized",
        detail: "A function used in JavaScript frameworks to save the UI component instance.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopup/Configuration/#onInitialized"
    },
    {
        prefix: "onOptionChanged",
        detail: "A function that is executed after a UI component property is changed.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopup/Configuration/#onOptionChanged"
    },
    {
        prefix: "onResize",
        detail: "A function that is executed each time the UI component is resized by one pixel.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopup/Configuration/#onResize"
    },
    {
        prefix: "onResizeEnd",
        detail: "A function that is executed when resizing ends.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopup/Configuration/#onResizeEnd"
    },
    {
        prefix: "onResizeStart",
        detail: "A function that is executed when resizing starts.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopup/Configuration/#onResizeStart"
    },
    {
        prefix: "onShowing",
        detail: "A function that is executed before the UI component is displayed.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopup/Configuration/#onShowing"
    },
    {
        prefix: "onShown",
        detail: "A function that is executed after the UI component is displayed.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopup/Configuration/#onShown"
    },
    {
        prefix: "onTitleRendered",
        detail: "A function that is executed when the UI component's title is rendered.",
        document: {
            type: "Function",
            defaultValue: "null",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "function",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopup/Configuration/#onTitleRendered"
    },
    {
        prefix: "position",
        detail: "Positions the UI component.",
        document: {
            type: "String | PositionConfig | Function",
            defaultValue: "{ my: 'center', at: 'center', of: window }",
            description: "",
            note: "",
            acceptValues: ['bottom', 'center', 'left', 'left bottom', 'left top', 'right', 'right bottom', 'right top', 'top'],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopup/Configuration/#position"
    },
    {
        prefix: "resizeEnabled",
        detail: "Specifies whether or not an end user can resize the UI component.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopup/Configuration/#resizeEnabled"
    },
    {
        prefix: "restorePosition",
        detail: "Specifies whether to display the Popup at the initial position when users reopen it.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopup/Configuration/#restorePosition"
    },
    {
        prefix: "rtlEnabled",
        detail: "Switches the UI component to a right-to-left representation.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopup/Configuration/#rtlEnabled"
    },
    {
        prefix: "shading",
        detail: "Specifies whether to shade the background when the UI component is active.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopup/Configuration/#shading"
    },
    {
        prefix: "shadingColor",
        detail: "Specifies the shading color. Applies only if shading is enabled.",
        document: {
            type: "String",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopup/Configuration/#shadingColor"
    },
    {
        prefix: "showCloseButton",
        detail: "Specifies whether or not the UI component displays the Close button.",
        document: {
            type: "Boolean",
            defaultValue: "false, true (desktop), false (Material)",
            description: "",
            note: "The property makes sense only if the showTitle property is set to true",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopup/Configuration/#showCloseButton"
    },
    {
        prefix: "showTitle",
        detail: "A Boolean value specifying whether or not to display the title in the popup window.",
        document: {
            type: "Boolean",
            defaultValue: "true",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopup/Configuration/#showTitle"
    },
    {
        prefix: "tabIndex",
        detail: "Specifies the number of the element when the Tab key is used for navigating.",
        document: {
            type: "Number",
            defaultValue: "0",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopup/Configuration/#tabIndex"
    },
    {
        prefix: "title",
        detail: "The title in the overlay window.",
        document: {
            type: "String",
            defaultValue: "''",
            description: "",
            note: "If the title property is specified, the titleTemplate property value is ignored",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopup/Configuration/#title"
    },
    {
        prefix: "titleComponent",
        detail: "An alias for the titleTemplate property specified in React. Accepts a custom component. Refer to Using a Custom Component for more information.",
        document: {
            type: "''",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopup/Configuration/#titleComponent"
    },
    {
        prefix: "titleRender",
        detail: "An alias for the titleTemplate property specified in React. Accepts a rendering function. Refer to Using a Rendering Function for more information.",
        document: {
            type: "''",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopup/Configuration/#titleRender"
    },
    {
        prefix: "titleTemplate",
        detail: "Specifies a custom template for the UI component title. Does not apply if the title is defined.",
        document: {
            type: "template",
            defaultValue: "Default Name: 'title'",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopup/Configuration/#titleTemplate"
    },
    {
        prefix: "toolbarItems",
        detail: "Configures toolbar items.",
        document: {
            type: "Array<Object>",
            defaultValue: "''",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopup/Configuration/toolbarItems/"
    },
    {
        prefix: "visible",
        detail: "A Boolean value specifying whether or not the UI component is visible.",
        document: {
            type: "Boolean",
            defaultValue: "false",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopup/Configuration/#visible"
    },
    {
        prefix: "width",
        detail: "Specifies the UI component's width.",
        document: {
            type: "Number | String | Function",
            defaultValue: "'80vw'",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopup/Configuration/#width"
    },
    {
        prefix: "wrapperAttr",
        detail: "Specifies the global attributes for the UI component's wrapper element.",
        document: {
            type: "any",
            defaultValue: "{}",
            description: "",
            note: "",
            acceptValues: [],
        },
        property: "field",
        link: "https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxPopup/Configuration/#wrapperAttr"
    },

]