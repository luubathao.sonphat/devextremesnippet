// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
const vscode = require('vscode');
const libraryDev = require('./snippets/library.js');

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed

/**
 * @param {vscode.ExtensionContext} context
 */

function completionProperty(document, position) {
	let items = [];
	let rootPosition = position.line;

	function displayHTML(documentation) {
		let des = documentation.description ? documentation.description + "<br/>" : "";
		let val = documentation.acceptValues.length > 0 ? "<b>Accepted Values: </b>" + documentation.acceptValues.join(`, `) + "<br/>" : "";
		let note = documentation.note ? "<b>NOTE: </b>" + documentation.note + "<br/><br/>" : "";
		let html = `${des}<b>Type: </b> ${documentation.type}<br/><b>Default Value: </b> ${documentation.defaultValue}<br/>${val}<br/>${note}[View more]()`;
		return html;
	}

	let myitem = (prefix, detail, documentation, link, property) => {
		let item = new vscode.CompletionItem(prefix, property == "function" ? vscode.CompletionItemKind.Function : vscode.CompletionItemKind.Property);
		item.preselect = true;
		//insert documention
		let html = displayHTML(documentation);
		const docs = new vscode.MarkdownString();
		docs.supportHtml = true;
		docs.value = html;
		docs.baseUri = vscode.Uri.parse(link);
		docs.appendText = documentation.type;
		item.documentation = docs;
		// item.range = new vscode.Range(position.character - 1, position.character + 1)
		let ss = ""
		if (property == "function") {
			ss = ":" + prefix + "='" + prefix + "$1'"
		} else if (property == "field") {
			if (documentation.acceptValues.length > 0) {// is options
				ss = prefix + "='${1|" + documentation.acceptValues.join(',') + "|}'";
			} else {
				ss = ":" + prefix + "='${1:" + prefix + "}'";
			}
		}
		item.insertText = new vscode.SnippetString(ss);;
		item.detail = detail;
		return item;
	}
	while (rootPosition > -1) {
		let regExrOpen = new RegExp("<", 'g');
		let regExrClose = new RegExp("\\/>", 'g');
		let pos = new vscode.Position(rootPosition, position.character);
		let text = document.lineAt(pos).text;
		if (regExrClose.test(text)) {
			break;
		}
		if (regExrOpen.test(text)) {
			// let prefixSnippet = text.trim().substr(1, text.length).trim();
			let regExrPrefix = (prefix) => new RegExp(`(([\\t]{0,})|([\\s]*))([0-9a-zA-Z!@#$%^&*()_=+?<>\/"';:{}~]*[\\s]*)<${prefix}(([\\s]+[0-9a-zA-Z!@#$%^&*()_=+?<"';:{}~]*[\\s]*>[\\s]*)|([\\s]*))$`, 'g');
			let checkKeySnippet = Object.keys(libraryDev).filter(f => text.search(regExrPrefix(f)) != -1)
			if (!Array.isArray(checkKeySnippet) && checkKeySnippet.length <= 0) {
				break;
			}
			let prefixSnippet = checkKeySnippet[0];
			if (regExrClose.test(text)) {
				break;
			}
			if (regExrPrefix(prefixSnippet).test(text) == false) {
				break;
			}
			let libs = libraryDev[prefixSnippet];
			items = libs.map(m => {
				return myitem(m.prefix, m.detail, m.document, m.link, m.property);
			})
			break
		}
		rootPosition--;
	}
	return items;
}
function autoImportComponent() {
	vscode.workspace.onDidChangeTextDocument(function (e) {
		if (e.contentChanges.length > 0) {
			let text = e.contentChanges[0].text.slice(0, 30).trim();
			let checkPrefixSnippet = Object.keys(libraryDev).filter(f => text.search(new RegExp(`^<${f}(([\\s]{1,}[0-9a-zA-Z!@#$%^&*()_=+?><"';:{}~]{0,})|([\\s]{0,}))$`, 'g')) != -1)
			if (checkPrefixSnippet.length > 0) {
				let prefixSnippet = checkPrefixSnippet[0];
				importComponent(e.document, prefixSnippet)
			}
		}
	})
}
function importComponent(document, prefixSnippet) {
	const editor = vscode.window.activeTextEditor;
	const currentPosition = editor.document.lineAt(editor.selection.active)
	if (!prefixSnippet) {
		return
	}
	if (editor) {
		let posStartScript = 0, posStopScript = 0, posTemplate = 0, posComponent = 0, posExportDefault = 0;
		let arrPrefixSnippetChildren = [];
		function convertComponentKey(prefix) {
			let strPrefix = "";
			let slicePrefix = prefix.slice(2, prefix.length);
			for (let i = 0; i < slicePrefix.length; i++) {
				const e = slicePrefix[i];
				if (i == 0) {
					strPrefix += e;
					continue;
				}
				if (e === e.toUpperCase()) {
					strPrefix += `-${e}`;
				} else {
					strPrefix += e;
				}
			}
			return strPrefix.toLowerCase();
		}
		function existsComponentImport(prefix) {
			for (let i = posStartScript; i <= posStopScript; i++) {
				let pos = new vscode.Position(i, 20);
				let text = document.lineAt(pos).text;
				if (text.search(prefix) != -1) {
					return true;
				}
				// if (i == posExportDefault) {
				// 	return false;
				// }
			}
			return false
		}
		function addTextScript(editBuilder, p, text) {
			let pos = new vscode.Position(p, 20);
			editBuilder.insert(pos, text)
		}
		(function existsTagVue() {
			let regExrScriptOpen = new RegExp(`(([\\t]{0,})|([\\s]*))<script([\\s]*)+([\s]*|lang="ts")+([\\s]*[>][\\s]*)$`, 'g');
			let regExrTemplateClose = new RegExp(`(([\\t]{0,})|([\\s]*))<\\/template([\\s]*)>([\\s]*)$`, 'g');
			let regExrComponent = new RegExp(`(([\\t]{0,})|([\\s]{0,}))component([s]{0,})([\\s]{0,}[:][\\s]{0,}[{][\\s]{0,})$`, 'g');
			let regExrExportDefault = new RegExp(`(([\\t]{0,})|([\\s]{0,}))export([\\s]{1,})default[\\s]{0,}[{][\\s]{0,}$`, 'g');
			let regExrPrefixChildren = new RegExp(`(([\\t]{0,})|([\\s]{0,}))<Dx[0-9a-zA-Z!@#$%^&*()_=+?<"';:{}~]+[\\s]*`, 'g');
			let regExrPrefixClose = new RegExp(`(([\\t]{0,})|([\\s]*))<\\/${prefixSnippet}([\\s]*)>([\\s]*)$`, 'g');
			let isPrefixClose = false;
			for (let i = 1; i < document.lineCount; i++) {
				let pos = new vscode.Position(i, 0);
				let text = document.lineAt(pos).text;
				if (posStartScript > 0 && posStopScript > 0) {
					break;
				}
				if (text.search(regExrScriptOpen) != -1) {
					posStartScript = i; // Vị trí bắt đầu điền import component
				}
				if (text.search(regExrTemplateClose) != -1) {
					posTemplate = i; // Vị trí bắt đầu điền script khi không có script trước đó
				}
				if (text.search(regExrComponent) != -1) {
					posComponent = i; // Vị trí bắt đầu điền components
				}
				if (text.search(regExrExportDefault) != -1) {
					posExportDefault = i; // Dừng tìm kiếm các prefixSnippet
				}
				if (text.search("</script>") != -1) {
					posStopScript = i; // Dừng tìm kiếm
				}
				if (i >= currentPosition.lineNumber && !isPrefixClose) {
					if ((text.search(regExrPrefixClose) != -1 || text.search(regExrTemplateClose) != -1) && !isPrefixClose) {
						isPrefixClose = true; // Dừng tìm kiếm các component con
					}
					if (text.search(regExrPrefixChildren) != -1) {
						text.match(regExrPrefixChildren).forEach(m => arrPrefixSnippetChildren.push(m ? m.trim().slice(1, m.length) : ""));// Lấy danh sách các thành phần con bên trong
					}
				}

			}
		})()
		editor.edit(editBuilder => {
			if (posStartScript == 0 && posStopScript == 0) { // Chưa có script
				addTextScript(editBuilder, posTemplate, `\n<script>\nimport { ${prefixSnippet} } from 'devextreme-vue/${convertComponentKey(prefixSnippet)}';\nexport default{\n\tcomponents:{\n\t\t${prefixSnippet},\n\t}\n}\n</script>`)
			} else { // Đã có script
				if (arrPrefixSnippetChildren.length > 0) {
					let strImport = "\nimport { ";
					let existsPrefix = false;
					arrPrefixSnippetChildren.forEach(e => {
						if (existsComponentImport(e)) {
							existsPrefix = true;
						} else {
							// Lấy import component
							strImport += `\n\t${e},`;
							// Điền các component
							if (posComponent > 0) {
								addTextScript(editBuilder, posComponent, `\n\t\t${e},`);
							}
						}
					});
					strImport += ` } from 'devextreme-vue/${convertComponentKey(prefixSnippet)}';`;
					if (!existsPrefix) {
						addTextScript(editBuilder, posStartScript, strImport);// Điền import component
					}
				}
			}
		});
	}
}
function activate(context) {
	const provider = vscode.languages.registerCompletionItemProvider(
		{ language: 'vue' },
		{
			async provideCompletionItems(document, position) {
				let items = [];
				items = completionProperty(document, position); // Hiển thị property component
				return items;
			}

		},
		':' // triggered whenever a ':' is being typed
	);
	autoImportComponent() // Tự động import component
	context.subscriptions.push(provider);

}

// this method is called when your extension is deactivated
function deactivate() { }

module.exports = {
	activate,
	deactivate
}
